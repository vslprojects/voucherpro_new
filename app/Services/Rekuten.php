<?php

namespace App\Services;

/**
 * Class Rekuten
 *
 * @author Ghulam Mustafa Kasmani <gmk1238@gmail.com>
 * @date   14/02/2020
 */
class Rekuten
{
    public function __construct()
    {
        //
    }

    public function generateAccessToken()
    {
        $data['grant_type']      = config('app.rekuten_grant_type');
        $data['username']        = config('app.rekuten_username');
        $data['password']        = config('app.rekuten_password');
        $data['scope']           = config('app.rekuten_sid');

        // $jsonStrReg              = json_encode($data);
        $httpStrReg              = http_build_query($data);

        // https://developers.rakutenmarketing.com/subscribe/apis/info?name=Coupon&version=1.0&provider=LinkShare&
        $ch = curl_init('https://api.rakutenmarketing.com/token');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $httpStrReg);

        // add token to the authorization header
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded',
            'Authorization: '.config('app.rekuten_authorization')
        ));

        $response = curl_exec($ch);
        $response = json_decode($response);
        return $response;
    }

}
