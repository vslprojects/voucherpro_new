<?php

namespace App\Services;

/**
 * Class SkimLinks
 *
 * @author Ali Nawaz <alinawazbs@gmail.com>
 * @date   20/12/19
 */
class SkimLinks
{
    public function __construct()
    {
        //
    }

    public function generateAccessToken()
    {
        $data['client_id']     = config('app.skim_links_client_id');
        $data['client_secret'] = config('app.skim_links_client_secret');
        $data['grant_type']    = 'client_credentials';
        $jsonStrReg            = json_encode($data);
        $httpStrReg            = http_build_query($data);

        // https://developers.skimlinks.com/
        $ch = curl_init('https://authentication.skimapis.com/access_token');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_POST, true);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $httpStrReg);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonStrReg);

        // add token to the authorization header
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json'
        ));

        $response = curl_exec($ch);
        $response = json_decode($response);

        return $response;
    }

}
