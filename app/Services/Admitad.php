<?php

namespace App\Services;

/**
 * Class Admitad
 *
 * @author Mujtaba Ahmed <connectwithmujtaba@hotmail.com>
 * @date   06/01/2020
 */
class Admitad
{
    public function __construct()
    {
        //
    }

    public function generateAccessToken($code = null)
    {
        $data['client_id']     = config('app.admitad_client_id');
        $data['client_secret'] = config('app.admitad_client_secret');
        $data['grant_type']    = 'authorization_code';
        $data['code']          = $code;
        $data['redirect_uri']  = config('app.admitad_redirect_uri');
        $jsonStrReg            = json_encode($data);
        $httpStrReg            = http_build_query($data);

        $ch = curl_init("https://api.admitad.com/token/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);

        // curl_setopt($ch, CURLOPT_POSTFIELDS, $httpStrReg);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $httpStrReg);

        // add token to the authorization header
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Basic '. config('app.admitad_redirect_uri')
        ));
      
        $response = curl_exec($ch);
     
        $response = json_decode($response);

        return $response;
    }
}
