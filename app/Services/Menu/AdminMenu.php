<?php

namespace App\Services\Menu;

use Spatie\Menu\Laravel\Link;
use Spatie\Menu\Laravel\Menu;

/**
 * Class AdminMenu
 *
 * @author Muzafar Ali Jatoi <muzfr7@gmail.com>
 * @date   23/9/18
 */
class AdminMenu
{
    public function register()
    {
        Menu::macro('admin', function () {
            return Menu::new()
                ->addClass('page-sidebar-menu')
                ->setAttribute('data-keep-expanded', 'false')
                ->setAttribute('data-auto-scroll', 'true')
                ->setAttribute('data-slide-speed', '200')
                ->html('<div class="sidebar-toggler hidden-phone"></div>')

                ->add(Link::toRoute(
                    'admin.dashboard.index',
                    '<i class="fa fa-home"></i> <span class="title">Dashboard</span>'
                )
                ->addParentClass('start'))

                /*
                 * User Management SubMenu
                 */
                /*->submenu(
                    '
                    <a href="javascript:;">
                        <i class="fa fa-user"></i>
                        <span class="title">User Management</span>
                        <span class="arrow open"></span>
                        <span class="selected"></span>
                    </a>
                    ',
                    Menu::new()
                    ->addClass('sub-menu')
                    ->add(Link::toRoute('admin.administrators.index', '<span class="title">Administrators</span>'))
                    ->add(Link::toRoute('admin.companies.index', '<span class="title">Companies</span>'))
                    ->add(Link::toRoute('admin.coaches.index', '<span class="title">Coaches</span>'))
                    ->add(Link::toRoute('admin.coachees.index', '<span class="title">Coachees</span>'))
                )*/
                ->add(Link::toRoute(
                    'admin.affiliate-networks.index',
                    '<i class="fa fa-th"></i> <span class="title">Affiliate Networks</span>'
                ))
                ->add(Link::toRoute(
                    'admin.categories.index',
                    '<i class="fa fa-th"></i> <span class="title">Categories</span>'
                ))
                ->add(Link::toRoute(
                    'admin.stores.index',
                    '<i class="fa fa-th"></i> <span class="title">Stores</span>'
                ))
                ->add(Link::toRoute(
                    'admin.stores.blocked',
                    '<i class="fa fa-th"></i> <span class="title">Blocked Stores</span>'
                ))
                ->add(Link::toRoute(
                    'admin.stores.withoutcategory',
                    '<i class="fa fa-th"></i> <span class="title">Stores Without Category</span>'
                ))
                ->add(Link::toRoute(
                    'admin.offers.index',
                    '<i class="fa fa-th"></i> <span class="title">Offers</span>'
                ))
                ->add(Link::toRoute(
                    'admin.posts.index',
                    '<i class="fa fa-th"></i> <span class="title">Blogs</span>'
                ))
                ->add(Link::toRoute(
                    'admin.side-banners.index',
                    '<i class="fa fa-th"></i> <span class="title">Side Banners</span>'
                ))
                ->add(Link::toRoute(
                    'admin.banners.index',
                    '<i class="fa fa-th"></i> <span class="title">Banners</span>'
                ))
                ->add(Link::toRoute(
                    'admin.events.index',
                    '<i class="fa fa-th"></i> <span class="title">Events</span>'
                ))
                ->add(Link::toRoute(
                    'admin.offer-types.index',
                    '<i class="fa fa-th"></i> <span class="title">Offer Types</span>'
                ))
                ->add(Link::toRoute(
                    'admin.banner-types.index',
                    '<i class="fa fa-th"></i> <span class="title">Banner Types</span>'
                ))
                ->add(Link::toRoute(
                    'admin.pages.index',
                    '<i class="fa fa-th"></i> <span class="title">Pages</span>'
                ))
                ->add(Link::toRoute(
                    'admin.sliders.index',
                    '<i class="fa fa-th"></i> <span class="title">Sliders Management</span>'
                ))
                ->add(Link::toRoute(
                    'admin.media-files.index',
                    '<i class="fa fa-th"></i> <span class="title">Media Files Management</span>'
                ))
                ->add(Link::toRoute(
                    'admin.menus.index',
                    '<i class="fa fa-th"></i> <span class="title">Menus Management</span>'
                ))

                ->add(Link::toRoute(
                    'admin.ratings.index',
                    '<i class="fa fa-th"></i> <span class="title">Ratings</span>'
                ))
                ->add(Link::toRoute(
                    'admin.voucher-requests.index',
                    '<i class="fa fa-th"></i> <span class="title">Voucher Requests</span>'
                ))
                ->add(Link::toRoute(
                    'admin.newsletter-subscribers.index',
                    '<i class="fa fa-users"></i> <span class="title">Newsletter Subscribers</span>'
                ))
                ->add(Link::toRoute(
                    'admin.administrators.index',
                    '<i class="fa fa-user"></i> <span class="title">Administrators</span>'
                ))
                ->add(Link::toRoute(
                    'admin.ip-addresses.index',
                    '<i class="fa fa-user"></i> <span class="title">Ip Addresses</span>'
                ))
                ->add(Link::toRoute(
                    'admin.site-settings.index',
                    '<i class="fa fa-cog"></i> <span class="title">Site Settings</span>'
                ))
                ->add(Link::toRoute(
                    'admin.users.change-password',
                    '<i class="fa fa-lock"></i> <span class="title">Change Password</span>'
                ))
                ->add(Link::toRoute(
                    'admin.auth.logout',
                    '<i class="fa fa-sign-out"></i> <span class="title">Logout</span>'
                )
                    ->setAttribute('id', 'leftnav-logout-link'))
                ->setActiveFromRequest();
        });
    }
}
