<?php

namespace App\Repositories;

use App\Models\Interfaces\PageInterface;
use App\Repositories\Interfaces\PageRepositoryInterface;

/**
 * Class PageRepository
 *
 * @author Ghulam Mustafa <ghulam.mustafa@vservices.com>
 * @date   05/10/18
 */
class PageRepository implements PageRepositoryInterface
{
    private $model;

    public function __construct(PageInterface $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        return $this->model::all();
    }

    public function create(array $data)
    {
        $model = $this->model::create($data);

        return $model;
    }

    public function find($id)
    {
        return $this->model::where('id', $id)->first();
    }

    public function update($id, array $data)
    {
        $this->model::where('id', $id)->update($data);
    }

    public function delete($id)
    {
        $this->model::where('id', $id)->delete();
    }

    /**
     * Return's page data with media file
     * @param  array $where
     * @return Illuminate\Database\Query\Builder
     */
    public function getPage(array $where)
    {
        return $this->model
            ::select(
                'pages.*',
                'media_files.filename as mediafile',
                'media_files.alt_text as mediafile_alt'
            )
            ->leftJoin('media_files', 'pages.media_file_id', 'media_files.id')
            ->where($where)
            ->first();
    }

    public function searchPages($keyword)
    {
        $search = "%" . $keyword . "%";

        return $this->model
            ::select(
                'pages.*',
                'media_files.filename as mediafile',
                'media_files.alt_text as mediafile_alt'
            )
            ->leftJoin('media_files', 'pages.media_file_id', 'media_files.id')
            ->whereRaw(
                "(pages.page_title LIKE ? OR pages.slug LIKE ? OR pages.content LIKE ?)",
                [$search, $search, $search]
            )
            ->get();
    }
}
