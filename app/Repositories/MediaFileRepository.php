<?php

namespace App\Repositories;

use App\Models\Interfaces\MediaFileInterface;
use App\Repositories\Interfaces\MediaFileRepositoryInterface;

/**
 * Class MediaFileRepository
 *
 * @author Ghulam Mustafa <ghulam.mustafa@vservices.com>
 * @date   05/10/18
 */
class MediaFileRepository implements MediaFileRepositoryInterface
{
    private $model;

    public function __construct(MediaFileInterface $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        return $this->model::orderBy('id', 'desc')->get();
    }

    public function create(array $data)
    {
        $model = $this->model::create($data);

        return $model;
    }

    public function find($id)
    {
        return $this->model::where('id', $id)->first();
    }

    public function update($id, array $data)
    {
        $this->model::where('id', $id)->update($data);
    }

    public function delete($id)
    {
        $this->model::where('id', $id)->delete();
    }
}
