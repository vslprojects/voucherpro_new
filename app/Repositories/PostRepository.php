<?php

namespace App\Repositories;

use App\Models\Interfaces\PostInterface;
use App\Repositories\Interfaces\PostRepositoryInterface;

class PostRepository implements PostRepositoryInterface
{
    private $model;

    public function __construct(PostInterface $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        return $this->model
            ::select(
                'posts.*',
                'media_files.filename as mediafile',
                'media_files.alt_text as mediafile_alt'
            )
            ->leftJoin('media_files', 'posts.thumbnail_id', 'media_files.id')
            ->get();
    }

    public function getAllPosts($perPage = 9, $page = 1)
    {
        return $this->model
            ::select(
                'posts.*',
                'media_files.filename as mediafile',
                'media_files.alt_text as mediafile_alt'
            )
            ->leftJoin('media_files', 'posts.thumbnail_id', 'media_files.id')
            ->paginate($perPage, ['*'], 'page', $page);
    }

    public function create(array $data)
    {
        $model = $this->model::create($data);

        return $model;
    }

    public function find($id)
    {
        return $this->model::where('id', $id)->first();
    }

    public function update($id, array $data)
    {
        $this->model::where('id', $id)->update($data);
    }

    public function delete($id)
    {
        $this->model::where('id', $id)->delete();
    }

    public function findBy($where = [])
    {
        return $this->model::where($where)->first();
    }

}
