<?php

namespace App\Repositories;

use App\Models\Interfaces\EventInterface;
use App\Repositories\Interfaces\EventRepositoryInterface;
use DB;

/**
 * Class EventRepository
 *
 * @author Ghulam Mustafa <ghulam.mustafa@vservices.com>
 * @date   05/10/18
 */
class EventRepository implements EventRepositoryInterface
{
    private $model;

    public function __construct(EventInterface $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        return $this->model::all();
    }

    public function create(array $data)
    {
        $model = $this->model::create($data);

        return $model;
    }

    public function find($id)
    {
        return $this->model::where('id', $id)->first();
    }

    public function update($id, array $data)
    {
        $this->model::where('id', $id)->update($data);
    }

    public function delete($id)
    {
        $this->model::where('id', $id)->delete();
    }

    /**
    * Return single event to show event details
    *
    * @param  array $where
    * @return Illuminate\Database\Query\Builder
    */
    public function showEvent($where = [])
    {
        return $this->model::select('events.*', 'admins.first_name', 'admins.last_name')
            //->leftJoin('categories as pcat', 'pcat.id', 'categories.parent_id')
            ->leftJoin('admins', 'admins.id', 'events.admin_id')
            ->where($where)
            ->first();
    }

    /**
    * Return events
    *
    * @param  array $where
    * @return Illuminate\Database\Query\Builder
    */
    public function getEvents($where = [])
    {
        return $this->model::select('events.*')
            ->where($where)
            ->get();
    }

    /**
    * Return offers
    *
    * @param  array $where
    * @return Illuminate\Database\Query\Builder
    */
    public function getOffers($where = [], $take = 0, $skip = 0)
    {
        $date = date('Y-m-d');
        $records = DB::table('offers as o')
            ->join('event_offer as eo', 'eo.offer_id', '=', 'o.id')
            ->join('stores as s', 's.id', '=', 'o.store_id')
            ->join('offer_types as ot', 'ot.id', '=', 'o.offer_type_id')
            ->select([
                'o.id',
                'o.title',
                'o.description',
                'o.voucher_code',
                'o.expiry_date',
                'o.affiliate_url',
                'o.views_count',
                'o.discount_label',
                's.name AS store_name',
                's.website',
                's.affiliate_url AS store_affiliate_url',
                's.logo','s.cover',
                'ot.name AS offer_type'
            ])
            ->where($where)
            ->where('o.start_date', '<=', $date)
            ->where('o.expiry_date', '>=', $date)
            ->orderBy('o.position');

        if($take)
            $records = $records->take($take);
        if($skip)
            $records = $records->skip($skip);

        return $records = $records->get();

    }

    public function getOffersWithPaginate($where = [], $page = 1)
    {
        $date = date('Y-m-d');
        $records = DB::table('offers as o')
            ->join('event_offer as eo', 'eo.offer_id', '=', 'o.id')
            ->join('stores as s', 's.id', '=', 'o.store_id')
            ->join('offer_types as ot', 'ot.id', '=', 'o.offer_type_id')
            ->select([
                'o.id',
                'o.title',
                'o.description',
                'o.voucher_code',
                'o.expiry_date',
                'o.affiliate_url',
                'o.views_count',
                'o.discount_label',
                's.name AS store_name',
                's.website',
                's.affiliate_url AS store_affiliate_url',
                's.logo','s.cover',
                'ot.name AS offer_type'
            ])
            ->where($where)
            ->where('o.start_date', '<=', $date)
            ->where('o.expiry_date', '>=', $date)
            ->orderBy('o.position');

        return $records = $records->paginate(20, ['*'], 'page', $page);

    }
}
