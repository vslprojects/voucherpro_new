<?php

namespace App\Repositories;

use App\Models\Interfaces\IpAddressInterface;
use App\Repositories\Interfaces\IpAddressRepositoryInterface;

/**
 * Class IpAddressRepository
 *
 * @author Muhammad Shahab Khan <muhammad.shahab@vservices.com>
 * @date   2/1/2021
 */
class IpAddressRepository implements IpAddressRepositoryInterface
{
    private $model;

    public function __construct(IpAddressInterface $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        return $this->model::all();
    }

    public function create(array $data)
    {
        $model = $this->model::create($data);

        return $model;
    }

    public function find($id)
    {
        return $this->model::with('admin')->where('id', $id)->first();
    }

    public function findBy($where = [])
    {
        return $this->model::where($where)->first();
    }

    public function update($id, array $data)
    {
        $this->model::where('id', $id)->update($data);
    }

    public function delete($id)
    {
        $this->model::where('id', $id)->delete();
    }

    public function getAllIpAddresses()
    {
        return $this->model::with('admin')->get();
    }
}
