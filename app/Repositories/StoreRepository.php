<?php

namespace App\Repositories;

use App\Models\Interfaces\StoreInterface;
use App\Repositories\Interfaces\StoreRepositoryInterface;
use DB;

/**
 * Class StoreRepository
 *
 * @author Ghulam Mustafa <ghulam.mustafa@vservices.com>
 * @date   05/10/18
 */
class StoreRepository implements StoreRepositoryInterface
{
    private $model;

    public function __construct(StoreInterface $model)
    {
        $this->model = $model;
    }

    public function model()
    {
        return $this->model;
    }

    public function all()
    {
        return $this->model::all();
    }

    public function create(array $data)
    {
        $model = $this->model::create($data);

        return $model;
    }

    public function find($id)
    {
        return $this->model::where('id', $id)->first();
    }

    public function findBy($where = [])
    {
        return $this->model::where($where)->first();
    }

    public function update($id, array $data)
    {
        $this->model::where('id', $id)->update($data);
    }

    public function delete($id)
    {
        $this->model::where('id', $id)->delete();
    }

    /**
    * Insert and update categories of store
    *
    * @param  int $store_id
    * @param  array $data
    * @return boolean
    */
    public function insertUpdateCategoryStore($store_id, $data = [])
    {
        // delete all store categories
        DB::table('category_store')->where('store_id', $store_id)->delete();

        //Bulk Insert Categoires Data
        foreach ($data as $category) {
            $dataInsert[] = array(
                'store_id'    => $store_id,
                'category_id' => $category
            );
        }
        $status = DB::table('category_store')->insert($dataInsert);

        if ($status) {
            return true;
        } else {
            return false;
        }
    }

    public function showStore($where = [])
    {
        return $this->model::select('stores.*', 'admins.first_name', 'admins.last_name','ad.first_name as f_name','ad.last_name as l_name')
            //->leftJoin('categories as pcat', 'pcat.id', 'categories.parent_id')
            ->leftJoin('admins', 'admins.id', 'stores.admin_id')
             ->leftJoin('admins as ad', 'ad.id', 'stores.updated_by')
            ->where($where)
            ->first();
    }

    /**
    * get categories of store
    *
    * @param  array $where
    * @return Illuminate\Database\Query\Builder
    */
    public function getCategoryStore($where = [])
    {
        return DB::table('category_store as cs')
        ->leftJoin('categories as cat', 'cat.id', '=', 'cs.category_id')
        ->select('cat.id', 'cat.name')
        ->where($where)
        ->get();
    }

    /**
    * get stores
    *
    * @param  array $where
    * @return Illuminate\Database\Query\Builder
    */
    public function getStore($where = [], $orderBy = 'ASC', $skip = 0, $take = 0)
    {
        $date = date('Y-m-d');
        $offersQuery = "(SELECT COUNT(o.id) FROM offers o WHERE o.store_id = s.id AND o.is_active = 1 
        AND o.start_date <= '$date' AND o.expiry_date >= '$date') AS total_offers";

        $query = $this->model::select([
            's.id','s.name','s.logo','s.website','s.description','s.description_text',
            's.meta_title','s.meta_description','s.meta_keywords',
            'cs.category_id', 'c.name AS catName', 'c.slug AS catSlug'])
        ->leftJoin('category_store as cs', 's.id', '=', 'cs.store_id')
        ->leftJoin('categories as c', 'c.id', '=', 'cs.category_id')
        ->selectRaw($offersQuery)
        ->from('stores as s')
        ->where($where);
        if ($orderBy) {
            $query = $query->orderBy('s.name', $orderBy);
        }
        if ($skip) {
            $query = $query->skip($skip);
        }

        if ($take) {
            $query = $query->take($take);
        }

        $query = $query->get();
        return $query;
    }

    /**
    * get stores
    *
    * @param  array $where
    * @return Illuminate\Database\Query\Builder
    */
    public function getStoresAndOfferCount($where = [], $orderBy = 'ASC', $skip = 0, $take = 0)
    {
        $date = date('Y-m-d');
        /*return DB::table('stores as s')
        ->select('s.name','s.website',
            DB::raw("(SELECT COUNT(o.id) FROM offers o WHERE o.store_id = s.id AND o.is_active = 1 ) AS total_offers")
        )
        ->where($where)
        ->get()->toArray();*/

        $offersQuery = "(SELECT COUNT(o.id) FROM offers o WHERE o.store_id = s.id AND o.is_active = 1 
        AND o.start_date <= '$date' AND o.expiry_date >= '$date') AS total_offers";

        $query = $this->model::select('s.name', 's.website')
        ->selectRaw($offersQuery)
        ->from('stores as s')
        ->where($where);
        if ($orderBy) {
            $query = $query->orderBy('s.name', $orderBy);
        }
        if ($skip) {
            $query = $query->skip($skip);
        }
        if ($take) {
            $query = $query->take($take);
        }
        $query = $query->get()->toArray();

        return $query;
    }

    /**
    * get similar
    *
    * @param  array $where
    * @return Illuminate\Database\Query\Builder
    */
    public function getSimilarStore($filters = [], $orderBy = 'ASC', $skip = 0, $take = 0)
    {
        $store_id = $filters['store_id'];
        $query = $this->model::selectRaw(
            's.id, 
            s.name,
            s.logo,
            s.website,
            s.description,
            s.cover,
            COUNT(o.id) AS offers_count'
        )
        ->Join('category_store as cs', 's.id', '=', 'cs.store_id')
        ->leftjoin('offers as o', 's.id', '=', 'o.store_id')
        ->from('stores as s')
        ->whereRaw('cs.category_id IN (SELECT category_id FROM category_store WHERE store_id = '.$store_id.')')
        ->where('s.id', '!=', $store_id)
        ->groupBy('s.id')
        ->having('offers_count', '>', '0');
        if ($orderBy) {
            $query = $query->orderBy('s.updated_at', $orderBy);
        }
        if ($skip) {
            $query = $query->skip($skip);
        }
        if ($take) {
            $query = $query->take($take);
        }
        $query = $query->get();
        return $query;
    }

    /**
    * get simple data with out any join
    *
    * @param  array $where
    * @return Illuminate\Database\Query\Builder
    */
    public function simpleData($filters = [], $orderBy = '', $take = 0, $skip = 0)
    {
        $date = date('Y-m-d');
        $offersQuery = "(SELECT COUNT(o.id) FROM offers o WHERE o.store_id = stores.id AND o.is_active = 1 
        AND o.start_date <= '$date' AND o.expiry_date >= '$date') AS total_offers";

        $query = $this->model::select('id', 'name', 'website', 'logo', 'cover', 'description',
            \DB::raw($offersQuery));
        if (isset($filters['q'])) {
            $query = $query->where('stores.name', 'like', '%'.$filters['q'].'%');
        }

        if (isset($filters['on_extension'])) {
            $query = $query->where('stores.enable_on_extension', '=', $filters['on_extension']);
        }

        if ($orderBy) {
            $query = $query->orderBy('stores.name', $orderBy);
        }
        if ($skip) {
            $query = $query->skip($skip);
        }
        if ($take) {
            $query = $query->take($take);
        }

       return $query->get()->toArray();

        // $data['total'] = $this->model::count();


    }


    /**
    * Returns max value
    * @param  array $where
    * @return Illuminate\Database\Query\Builder
    */
    public function getMaxValue($column)
    {
        return $this->model::max($column);
    }

    /**
    * Return offers
    *
    * @param  array $where
    * @return Illuminate\Database\Query\Builder
    */
    public function getStoreByAjax($where = [], $filter = [])
    {
        $query = $this->model::select('stores.*')
            ->where($where)
            ->where(function($query) use ($filter) {

                if (isset($filter['search']) && !empty($filter['search'])) {
                    return $query->where('stores.name', 'like', '%'. $filter['search'] .'%')
                    ->orWhere('stores.website', 'like', '%'. $filter['search'] .'%');
                }
                if ( isset($filter['status']) && $filter['status'] != null ) {
                    $query->where('stores.status', '=',  $filter['status']);   
                }
            })
            ->orderBy('stores.updated_at','DESC');

        $total = $query->count();

        if (isset($filter['skip'])) {
            $query->skip($filter['skip']);
        }

        if (isset($filter['take'])) {
            $query->take($filter['take']);
        }

        return [
            'total'     => $total,
            'records'   => $query->get()
        ];
    }

    /**
    * Return stores information
    *
    * @param  array $where
    * @return Illuminate\Database\Query\Builder
    */
    public function exportStores($where = [])
    {
        return $this->model::select(
            'stores.id AS store_id',
            'stores.name AS store_name',
            'stores.meta_title AS meta_title',
            'stores.meta_description AS meta_description',
            DB::raw("IF(stores.description IS NULL OR stores.description = '', 'Empty', 'Not Empty') AS description"),
            DB::raw("IF(stores.description_text IS NULL OR stores.description_text = '', 'Empty', 'Not Empty') AS description_text"),
            DB::raw("CONCAT((SELECT website FROM site_settings), '/', IF(LOCATE('www', stores.website) = 0, CONCAT('www', '.', stores.website), stores.website)) AS store_url"),
            'stores.affiliate_url AS affiliate_url',
            DB::raw("IF(LOCATE('www', stores.website) = 0, CONCAT('www', '.', stores.website), stores.website) AS brand_url"),
            DB::raw("IF(stores.logo IS NOT NULL, 'Yes', 'No') AS image"),
            DB::raw("IF(stores.cover IS NOT NULL, 'Yes', 'No') AS cover_image"),
            DB::raw("IF(off.cc IS NULL, 0, off.cc) AS number_of_coupon"),
            DB::raw("REPLACE(categories.name, '&amp;', '&') AS category"),
            DB::raw("IF(stores.is_active = 1, 'Enabled', 'Disbaled') AS status"),
            DB::raw("DATE_FORMAT(stores.created_at, '%Y-%m-%d %h:%i:%s') AS date_added")
        )
            ->leftJoin('category_store', 'category_store.store_id', '=', 'stores.id')
            ->leftJoin('categories', 'categories.id', '=', 'category_store.category_id')
            ->leftJoin(DB::raw("(SELECT store_id, COUNT(*) AS cc FROM offers GROUP BY store_id) AS off"), 'off.store_id', '=', 'stores.id')
            ->where($where)
            ->groupBy('stores.id')
            ->get();
    }

    /**
    * get store without category
    *
    * @return Illuminate\Database\Query\Builder
    */
    public function getWithoutCategoryStore()
    {
        return DB::select("SELECT 
                  stores.*
                FROM
                  stores 
                  LEFT JOIN category_store 
                    ON category_store.store_id = stores.id 
                WHERE category_store.store_id IS NULL 
                  AND category_store.category_id IS NULL 
        ");
    }
}
