<?php

namespace App\Repositories;

use App\Models\Interfaces\CategoryInterface;
use App\Repositories\Interfaces\CategoryRepositoryInterface;
use DB;

/**
 * Class CategoryRepository
 *
 * @author Ghulam Mustafa <ghulam.mustafa@vservices.com>
 * @date   05/10/18
 */
class CategoryRepository implements CategoryRepositoryInterface
{
    private $model;

    public function __construct(CategoryInterface $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        return $this->model::all();
    }

    public function create(array $data)
    {
        $model = $this->model::create($data);

        return $model;
    }

    public function find($id)
    {
        return $this->model::where('id', $id)->first();
    }

    public function update($id, array $data)
    {
        $this->model::where('id', $id)->update($data);
    }

    public function delete($id)
    {
        $this->model::where('id', $id)->delete();
    }

    /**
    * Return single category to show category details
    *
    * @param  array $where
    * @return Illuminate\Database\Query\Builder
    */
    public function showCategory($where = [])
    {
        return $this->model::select('categories.*', 'pcat.name AS parent_cat', 'admins.first_name', 'admins.last_name')
            ->leftJoin('categories as pcat', 'pcat.id', 'categories.parent_id')
            ->leftJoin('admins', 'admins.id', 'categories.admin_id')
            ->where($where)
            //->orderBy('menus.position', 'ASC')
            // ->groupBy('menus.id')
            ->first();
    }

    /**
    * Returns categories list with stores
    *
    * @param  array $where
    * @param  array $filter
    * @return Illuminate\Database\Query\Builder
    */
    public function getCategoriesWithStores($where = [], $filter = [])
    {
        $response = [];
        $categories = $this->model::select('categories.*')->where($where);

        if(isset($filter['categories']['orderBy']))
            $categories = $categories->orderBy($filter['categories']['orderBy']);

        $categories = $categories->get()->toArray();

        foreach ($categories as $row) {

            $stores = DB::table('stores as s')
            ->join('category_store as cs', 'cs.store_id', '=', 's.id')
            ->select('s.id','s.name','s.website','s.description','s.logo','s.cover')
            ->where([
                's.is_active'   =>  1,
                'cs.category_id'=>  $row['id']
            ]);

            if(isset($filter['stores']['orderBy']))
                $stores = $stores->orderBy($filter['stores']['orderBy']);
            if(isset($filter['stores']['skip']))
                $stores = $stores->skip($filter['stores']['skip']);
            if(isset($filter['stores']['take']))
                $stores = $stores->take($filter['stores']['take']);

            $stores = $stores->get()->toArray();

            $row['stores'] = $stores;
            array_push($response, $row);
        }

        return $response;
    }

    /**
    * Returns featured categories
    *
    * @param  array $where
    * @return Illuminate\Database\Query\Builder
    */
    public function featuredCategories($where = [], $orderBy, $skip, $take)
    {
        return $this->model::select('*')
            ->where($where)
            ->orderBy('name', $orderBy)
            ->skip($skip)
            ->take($take)
            ->get();
    }

    /**
    * Returns categories for Search
    *
    * @param  array $where
    * @return Illuminate\Database\Query\Builder
    */
    public function searchCategories($filters = [], $orderBy, $take=0, $skip=0)
    {
        $query = $this->model::select('name','slug','image_small');
        if(isset($filters['q'])){
            $query = $query->where('categories.name','like','%'.$filters['q'].'%');
        }
        if(isset($filters['catIds'])){
            $query = $query->whereIn('categories.id', $filters['catIds'], 'or');
        }

        if($orderBy) {
           $query = $query->orderBy('categories.name', $orderBy);
        }
        if($skip) {
           $query = $query->skip($skip);
        }
        if($skip) {
           $query = $query->take($take);
        }
        return $query->get()->toArray();
    }

    /**
    * Returns max value
    *
    * @param  array $where
    * @return Illuminate\Database\Query\Builder
    */
    public function getMaxValue($column)
    {
        return $this->model::max($column);
    }

    /**
    * Return categories
    *
    * @param  array $where
    * @return Illuminate\Database\Query\Builder
    */
    public function getCategories($where = [])
    {
        return $this->model::select('*')
            ->where($where)
            ->get();
    }


      /**
    * Returns categories list with stores
    *
    * @param  array $where
    * @param  array $filter
    * @return Illuminate\Database\Query\Builder
    */
    public function getCategoriesWithStoresCount($where = [], $filter = [])
    {
        $response = [];
        $categories = $this->model::select('categories.*')->where($where);

        if(isset($filter['categories']['orderBy']))
            $categories = $categories->orderBy($filter['categories']['orderBy']);

        $categories = $categories->get()->toArray();

        foreach ($categories as $row) {

            $stores = DB::table('stores as s')
            ->join('category_store as cs', 'cs.store_id', '=', 's.id')
            ->select('s.id','s.name','s.website','s.description','s.logo','s.cover')
            ->where([
                's.is_active'   =>  1,
                'cs.category_id'=>  $row['id']
            ]);

            if(isset($filter['stores']['orderBy']))
                $stores = $stores->orderBy($filter['stores']['orderBy']);
            if(isset($filter['stores']['skip']))
                $stores = $stores->skip($filter['stores']['skip']);
            if(isset($filter['stores']['take']))
                $stores = $stores->take($filter['stores']['take']);

            $stores = $stores->count();

            $row['stores_count'] = $stores;
            array_push($response, $row);
        }

        return $response;
    }


    /**
    * Returns categories list with stores
    *
    * @param  array $where
    * @param  array $filter
    * @return Illuminate\Database\Query\Builder
    */
    public function getCategoriesWithStoresPaginate($where = [], $filter = [], $perPage = 24)
    {
        $response = [];

        $categories = $this->model::select('categories.*')->where($where);

        if(isset($filter['categories']['orderBy']))
            $categories = $categories->orderBy($filter['categories']['orderBy']);

        $categories = $categories->get()->toArray();

        foreach ($categories as $row) {

            $stores = DB::table('stores as s')
            ->join('category_store as cs', 'cs.store_id', '=', 's.id')
            ->select('s.id','s.name','s.website','s.description','s.logo','s.cover')
            ->where([
                's.is_active'   =>  1,
                'cs.category_id'=>  $row['id']
            ]);

            if(isset($filter['stores']['orderBy']))
                $stores = $stores->orderBy($filter['stores']['orderBy']);
            if(isset($filter['stores']['skip']))
                $stores = $stores->skip($filter['stores']['skip']);
            if(isset($filter['stores']['take']))
                $stores = $stores->take($filter['stores']['take']);

            $stores = $stores->paginate($perPage, ['*'], 'page', $filter['stores']['page']);

            $row['stores'] = $stores;
            array_push($response, $row);
        }

        return $response;
    }

}
