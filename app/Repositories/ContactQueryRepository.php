<?php

namespace App\Repositories;

use App\Models\Interfaces\ContactQueryInterface;
use App\Repositories\Interfaces\ContactQueryRepositoryInterface;

/**
 * Class ContactQueryRepository
 *
 * @author Ghulam Mustafa <ghulam.mustafa@vservices.com>
 * @date   05/10/18
 */
class ContactQueryRepository implements ContactQueryRepositoryInterface
{
    private $model;

    public function __construct(ContactQueryInterface $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        return $this->model::all();
    }

    public function create(array $data)
    {
        $model = $this->model::create($data);

        return $model;
    }

    public function find($id)
    {
        return $this->model::where('id', $id)->first();
    }

    public function update($id, array $data)
    {
        $this->model::where('id', $id)->update($data);
    }

    public function delete($id)
    {
        $this->model::where('id', $id)->delete();
    }
}
