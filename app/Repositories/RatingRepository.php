<?php

namespace App\Repositories;

use App\Models\Interfaces\RatingInterface;
use App\Repositories\Interfaces\RatingRepositoryInterface;

/**
 * Class RatingRepository
 *
 * @author Ghulam Mustafa <ghulam.mustafa@vservices.com>
 * @date   05/10/18
 */
class RatingRepository implements RatingRepositoryInterface
{
    private $model;

    public function __construct(RatingInterface $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        return $this->model::all();
    }

    public function create(array $data)
    {
        $model = $this->model::create($data);

        return $model;
    }

    public function find($id)
    {
        return $this->model::where('id', $id)->first();
    }

    public function update($id, array $data)
    {
        $this->model::where('id', $id)->update($data);
    }

    public function delete($id)
    {
        $this->model::where('id', $id)->delete();
    }

    /**
    * Return ratings
    *
    * @param  array $where
    * @return Illuminate\Database\Query\Builder
    */
    public function getRatings($where = [])
    {
        return $this->model::select('ratings.*', 'stores.name AS store_name')
            ->leftJoin('stores', 'stores.id', 'ratings.store_id')
            ->where($where)
            ->get();
    }

    /**
    * Return ratings single record
    *
    * @param  array $where
    * @return Illuminate\Database\Query\Builder
    */
    public function showRatings($where = [])
    {
        return $this->model::select('ratings.*', 'stores.name AS store_name')
            ->leftJoin('stores', 'stores.id', 'ratings.store_id')
            ->where($where)
            ->first();
    }

    public function getAvgRattings($where = [])
    {
        return $this->model::selectRaw("ROUND(AVG(r.rating),2) AS rating")
            ->from('ratings as r')
            ->where($where)
            ->first();
    }
}
