<?php

namespace App\Repositories;

use App\Models\Interfaces\CommentInterface;
use App\Repositories\Interfaces\CommentRepositoryInterface;

class CommentRepository implements CommentRepositoryInterface
{
    private $model;

    public function __construct(CommentInterface $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        return $this->model::all();
    }

    public function create(array $data)
    {
        $model = $this->model::create($data);

        return $model;
    }

    public function find($id)
    {
        return $this->model::where('id', $id)->first();
    }

    public function update($id, array $data)
    {
        $this->model::where('id', $id)->update($data);
    }

    public function delete($id)
    {
        $this->model::where('id', $id)->delete();
    }

    public function findBy($where = [])
    {
        return $this->model::where($where)->first();
    }

    public function allBy($where = [])
    {
        return $this->model::where($where)->get();
    }

}
