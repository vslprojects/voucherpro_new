<?php

namespace App\Repositories;

use App\Models\Interfaces\NewsletterSubscriberInterface;
use App\Repositories\Interfaces\NewsletterSubscriberRepositoryInterface;

/**
 * Class NewsletterSubscriberRepository
 *
 * @author Ghulam Mustafa <ghulam.mustafa@vservices.com>
 * @date   05/10/18
 */
class NewsletterSubscriberRepository implements NewsletterSubscriberRepositoryInterface
{
    private $model;

    public function __construct(NewsletterSubscriberInterface $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        return $this->model::all();
    }

    public function create(array $data)
    {
        $model = $this->model::create($data);

        return $model;
    }

    public function find($id)
    {
        return $this->model::where('id', $id)->first();
    }

    public function update($id, array $data)
    {
        $this->model::where('id', $id)->update($data);
    }

    public function delete($id)
    {
        $this->model::where('id', $id)->delete();
    }

    public function subscribe($data = [])
    {
        $this->model::firstOrCreate(['email' => $data['email']]);
    }

    public function subscribeGetRecord($where = [])
    {
        return $this->model::where($where)->first();
    }
}
