<?php

namespace App\Repositories;

use App\Models\Interfaces\SliderInterface;
use App\Repositories\Interfaces\SliderRepositoryInterface;

/**
 * Class SliderRepository
 *
 * @author Ghulam Mustafa <ghulam.mustafa@vservices.com>
 * @date   05/10/18
 */
class SliderRepository implements SliderRepositoryInterface
{
    private $model;

    public function __construct(SliderInterface $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        return $this->model::all();
    }

    public function create(array $data)
    {
        $model = $this->model::create($data);

        return $model;
    }

    public function find($id)
    {
        return $this->model::where('id', $id)->first();
    }

    public function update($id, array $data)
    {
        $this->model::where('id', $id)->update($data);
    }

    public function delete($id)
    {
        $this->model::where('id', $id)->delete();
    }

    public function getAllSliders()
    {
        return $this->model::whereIsActive(1)->get();
    }

    /**
    * Returns max value
    * 
    * @param  array $where
    * @return Illuminate\Database\Query\Builder
    */
    public function getMaxValue($column)
    {
        return $this->model::max($column);
    }
}
