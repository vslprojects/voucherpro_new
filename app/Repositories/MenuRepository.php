<?php

namespace App\Repositories;

use App\Models\Interfaces\MenuInterface;
use App\Repositories\Interfaces\MenuRepositoryInterface;
use DB;

/**
 * Class MenuRepository
 *
 * @author Ghulam Mustafa <ghulam.mustafa@vservices.com>
 * @date   05/10/18
 */
class MenuRepository implements MenuRepositoryInterface
{
    private $model;

    public function __construct(MenuInterface $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        return $this->model::all();
    }

    public function create(array $data)
    {
        $model = $this->model::create($data);

        return $model;
    }

    public function find($id)
    {
        return $this->model::where('id', $id)->first();
    }

    public function update($id, array $data)
    {
        $this->model::where('id', $id)->update($data);
    }

    public function delete($id)
    {
        $this->model::where('id', $id)->delete();
    }

    public function getAllRecords($where = [])
    {
        /*return $this->model::select(
                'menus.id',
                'menus.parent_id',
                'menus.name',
                'pages.slug',
                'menus.position',
                'menus.is_active'
            )
            ->leftJoin('pages', 'pages.slug', 'menus.slug')
            ->leftJoin('events', 'events.slug', 'menus.slug')
            ->leftJoin('stores', 'stores.website', 'menus.slug')
            ->where($where)
            ->orderBy('menus.position', 'ASC')
            // ->groupBy('menus.id')
            ->toSql();*/

            $query = 'SELECT * FROM (
                        (SELECT
                          `menus`.`id`,
                          `menus`.`parent_id`,
                          `menus`.`name`,
                          `menus`.`enddate`, 
                          `menus`.`sticker`, 
                          `menus`.`style`,
                          `pages`.`slug` ,
                          `menus`.`position`,
                          `menus`.`is_active` 
                        FROM
                          `menus` 
                          INNER JOIN `pages` 
                            ON `pages`.`slug` = `menus`.`slug` 
                        WHERE (`menus`.`is_active` = 1) )
                        UNION ALL
                        (SELECT
                          `menus`.`id`,
                          `menus`.`parent_id`,
                          `menus`.`name`,
                          `menus`.`enddate`, 
                          `menus`.`sticker`, 
                          `menus`.`style`,
                          `stores`.`website` AS "slug",
                          `menus`.`position`,
                          `menus`.`is_active` 
                        FROM
                          `menus` 
                          INNER JOIN `stores` 
                            ON `stores`.`website` = `menus`.`slug` 
                        WHERE (`menus`.`is_active` = 1) )
                        UNION ALL
                        (SELECT 
                          `menus`.`id`,
                          `menus`.`parent_id`,
                          `menus`.`name`,
                          `menus`.`enddate`, 
                          `menus`.`sticker`, 
                          `menus`.`style`,
                          `events`.`slug` ,
                          `menus`.`position`,
                          `menus`.`is_active` 
                        FROM
                          `menus` AS menus 
                          INNER JOIN `events` 
                            ON `events`.`slug` = `menus`.`slug`  
                        WHERE (`menus`.`is_active` = 1) )
                        ) AS i
                        ORDER BY `position` ASC ';

        $results = DB::select( $query );
        return count($results) > 0 ? $results : 0 ;
    }

    public function showMenu($where = [])
    {
        return $this->model::select('menus.*', 'pmenu.name AS parent_menu')
            //->leftJoin('pages', 'pages.id', 'menus.page_id')
            ->leftJoin('menus as pmenu', 'pmenu.id', 'menus.parent_id')
            ->where($where)
            ->orderBy('menus.position', 'ASC')
            // ->groupBy('menus.id')
            ->first();
    }

    /**
    * Returns max value
    * 
    * @param  array $where
    * @return Illuminate\Database\Query\Builder
    */
    public function getMaxValue($column)
    {
        return $this->model::max($column);
    }
}
