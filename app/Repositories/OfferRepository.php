<?php

namespace App\Repositories;

use App\Models\Interfaces\OfferInterface;
use App\Repositories\Interfaces\OfferRepositoryInterface;
use DB;

/**
 * Class OfferRepository
 *
 * @author Ghulam Mustafa <ghulam.mustafa@vservices.com>
 * @date   05/10/18
 */
class OfferRepository implements OfferRepositoryInterface
{
    private $model;

    public function __construct(OfferInterface $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        return $this->model::all();
    }

    public function create(array $data)
    {
        $model = $this->model::create($data);

        return  $model;

    }

    public function insert(array $data){
        DB::enableQueryLog(); // Enable query log
        $model = $this->model::insert($data);
        return $model;
    }

    public function find($id)
    {

        return $this->model::where('id', $id)->first();
        DB::enableQueryLog(); // Enable query log
    }

    public function findBy($where = [])
    {
        DB::enableQueryLog(); // Enable query log
        //dd($where);

        $query = $this->model::where($where)->first();


        return $query;


    }
    public function findBy1($where = [])
    {
        //dd($where);
        DB::enableQueryLog(); // Enable query log
        $query = $this->model::where($where)->count();
        //dd(DB::getQueryLog());
        return $query;


    }

    public function update($id, array $data)
    {
        $this->model::where('id', $id)->update($data);
    }

    public function delete($id)
    {
        $this->model::where('id', $id)->delete();
    }

    /**
    * Return offers
    *
    * @param  array $where
    * @return Illuminate\Database\Query\Builder
    */
    public function getOffers($where = [], $filter = [])
    {
        $date = date('Y-m-d');
        $query = $this->model::select(
            'offers.*',
            'stores.name AS store_name',
            'ot.name AS type_name')
            ->leftJoin('stores', 'stores.id', 'offers.store_id')
            ->leftJoin('offer_types as ot', 'ot.id', 'offers.offer_type_id')
            ->where($where)
            ->where(function($query) use ($filter, $date) {
                if (isset($filter['expired'])) {
                     $query->where('offers.expiry_date', '<', $date);
                }
                if (isset($filter['search']) && !empty($filter['search'])) {
                     $query->where('stores.name', 'like', '%'. $filter['search'] .'%');
                }
                if ( isset($filter['status']) && $filter['status'] != null ) {
                     $query->where('offers.status', '=',  $filter['status']);
                }
            })
            ->orderBy('offers.updated_at','DESC');

        $total = $query->count();

        if (isset($filter['skip'])) {
            $query->skip($filter['skip']);
        }

        if (isset($filter['take'])) {
            $query->take($filter['take']);
        }

        return [
            'total'     => $total,
            'records'   => $query->get()
        ];
    }

    /**
    * Return ratings single record
    *
    * @param  array $where
    * @return Illuminate\Database\Query\Builder
    */
    public function showOffer($where = [])
    {
        return $this->model::select(
            'offers.*',
            'stores.name AS store_name',
            'ot.name AS type_name',
            'e.name AS event_name',
            'admins.first_name',
            'admins.last_name',
            'ad.first_name as updated_fname',
            'ad.last_name as updated_lname'
        )
            ->leftJoin('stores', 'stores.id', 'offers.store_id')
            ->leftJoin('offer_types as ot', 'ot.id', 'offers.offer_type_id')
            ->leftJoin('events as e', 'e.id', 'offers.event_id')
            ->leftJoin('admins', 'admins.id', '=' ,'offers.admin_id')
            ->leftJoin('admins as ad', 'ad.id', '=' ,'offers.updated_by')
            ->where($where)
            ->first();
    }

    /**
    * Return featured vouchers
    *
    * @param  array $where, $orderBy, $skip, $take
    * @return Illuminate\Database\Query\Builder
    */
    public function getFeaturedtopVoucher($where = [], $orderBy, $skip = null, $take = null)
    {
        $date = date('Y-m-d');
        return $this->model::select(
            'o.id',
            'o.title',
            'o.description',
            'o.voucher_code',
            'o.expiry_date',
            'o.affiliate_url AS offer_affiliate_url',
            'o.views_count',
            'o.is_featured AS offer_is_featured',
            'ot.name AS offer_type',
            's.name AS store_name',
            's.affiliate_url AS store_affiliate_url',
            's.logo AS store_logo',
            's.cover AS store_cover',
            's.website'
        )
            ->from('offers As o')
            ->leftJoin('offer_types As ot', 'ot.id', 'o.offer_type_id')
            ->leftJoin('stores as s', 's.id', 'o.store_id')
            ->where($where)
            ->where('o.start_date', '<=', $date)
            ->where('o.expiry_date', '>=', $date)
            ->orderBy('o.views_count', $orderBy)
            ->skip($skip)
            ->take($take)
            ->get();
    }

    /**
    * Return offer count
    *
    * @param  array $where
    * @return Illuminate\Database\Query\Builder
    */
    public function getOffersCount($where = [])
    {
        $date = date('Y-m-d');
        return $this->model::select(
            'ot.name AS type_name',
            DB::raw('count(offers.id) as total')
        )
            ->Join('offer_types as ot', 'ot.id', 'offers.offer_type_id')
            ->Join('stores as s', 's.id', 'offers.store_id')
            ->where($where)
            ->where('offers.start_date', '<=', $date)
            ->where('offers.expiry_date', '>=', $date)
            ->groupBy('offers.offer_type_id')
            ->orderBy('ot.name')
            ->get()->toArray();
    }

    /**
    * Return offers list
    *
    * @param  array $where
    * @return Illuminate\Database\Query\Builder
    */
    public function getOffersList($where = [], $orderBy = 'offers.position', $skip = null, $take = null)
    {
        $date = date('Y-m-d');
        $records = $this->model::select(
            'offers.id',
            'offers.title',
            'offers.description',
            'offers.voucher_code',
            'offers.expiry_date',
            'offers.affiliate_url',
            'offers.views_count',
            'offers.discount_label',
            's.name AS store_name',
            's.website',
            's.affiliate_url AS store_affiliate_url',
            's.logo','s.cover',
            'ot.name AS offer_type'
            )->Join('stores as s', 's.id', 'offers.store_id')
            ->Join('offer_types as ot', 'ot.id', 'offers.offer_type_id')
            ->where($where)
            ->where('offers.start_date', '<=', $date)
            ->where('offers.expiry_date', '>=', $date)
            ->orderBy($orderBy)
            ->orderBy('offers.views_count', 'DESC');

            if($take)
                $records = $records->take($take);
            if($skip)
                $records = $records->skip($skip);

            return $records = $records->get();
    }

    public function getOffersListPaginate($where = [], $orderBy = 'offers.position',  $page =1)
    {
        $date = date('Y-m-d');
        $records = $this->model::select(
            'offers.id',
            'offers.title',
            'offers.description',
            'offers.voucher_code',
            'offers.expiry_date',
            'offers.affiliate_url',
            'offers.views_count',
            'offers.discount_label',
            's.name AS store_name',
            's.website',
            's.affiliate_url AS store_affiliate_url',
            's.logo','s.cover',
            'ot.name AS offer_type'
            )->Join('stores as s', 's.id', 'offers.store_id')
            ->Join('offer_types as ot', 'ot.id', 'offers.offer_type_id')
            ->where($where)
            ->where('offers.start_date', '<=', $date)
            ->where('offers.expiry_date', '>=', $date)
            ->orderBy($orderBy)
            ->orderBy('offers.views_count', 'DESC');

            return $records = $records->paginate(20, ['*'], 'page', $page);
    }

    /**
    * Returns max value
    *
    * @param  array $where
    * @return Illuminate\Database\Query\Builder
    */
    public function getMaxValue($column)
    {
        return $this->model::max($column);
    }

    /**
    * Insert and update offers in event
    *
    * @param  int $offer_id
    * @param  array $data
    * @return boolean
    */
    public function insertUpdateEventOffer($offer_id, $data = [])
    {
        DB::table('event_offer')->where('offer_id', $offer_id)->delete();

        foreach ($data as $event_id) {
            $dataInsert[] = array(
                'event_id'  => $event_id,
                'offer_id'  => $offer_id
            );
        }
        $status = DB::table('event_offer')->insert($dataInsert);

        if ($status) {
            return true;
        } else {
            return false;
        }

    }

    /**
    * delete recored from event_offer
    *
    * @param  array $where
    */
    public function deleteEventOffer($where = [])
    {
        DB::table('event_offer')->where($where)->delete();

    }

    /**
    * get events of offer
    *
    * @param  array $where
    * @return Illuminate\Database\Query\Builder
    */
    public function getEventOffer($where = [])
    {
        return DB::table('event_offer as eo')
        ->Join('events as e', 'e.id', '=', 'eo.event_id')
        ->select('e.id','e.name')
        ->where($where)
        ->get();
    }

    /*public function getStatus($where = [], $status = [])
    {
        return $this->model::select(
            'offers.*',
            'stores.name AS store_name'
            )
            ->leftJoin('stores', 'stores.id', 'offers.store_id')
            ->where($where)
            if (isset($status['offers.status'])
            && $status['offers.status'] != null) {
            $query = $query->where($status);
            }
            $result = $query->get();
            return $result;
    }*/

    /**
    * Return offers of store on store detail page
    *
    * @param  array $where
    * @return Illuminate\Database\Query\Builder
    */
    public function getStoresOffer($where = [])
    {
        return $this->model::select('offers.*')
            ->leftJoin('stores', 'offers.store_id', 'stores.id')
            ->where($where)
            ->get();
    }

}
