<?php

namespace App\Repositories;

use App\Models\Interfaces\SideBannerInterface;
use App\Repositories\Interfaces\SideBannerRepositoryInterface;
use DB;

/**
 * Class SideBannerRepository
 *
 * @author Ghulam Mustafa <ghulam.mustafa@vservices.com>
 * @date   05/10/18
 */
class SideBannerRepository implements SideBannerRepositoryInterface
{
    private $model;

    public function __construct(SideBannerInterface $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        return $this->model::all();
    }

    public function create(array $data)
    {
        $model = $this->model::create($data);

        return $model;
    }

    public function find($id)
    {
        return $this->model::where('id', $id)->first();
    }

    public function update($id, array $data)
    {
        $this->model::where('id', $id)->update($data);
    }

    public function delete($id)
    {
        $this->model::where('id', $id)->delete();
    }

    /**
    * Show side banner
    *
    * @param  array $where
    * @return Illuminate\Database\Query\Builder
    */
    public function showSideBanner($where = [])
    {
        return $this->model::select('side_banners.*', 'banner_types.name', 'admins.first_name', 'admins.last_name', 'cat.name AS category_name')
            ->leftJoin('banner_types', 'banner_types.id', 'side_banners.banner_type_id')
            ->leftJoin('admins', 'admins.id', 'side_banners.admin_id')
            ->leftJoin('categories as cat', 'cat.id', 'side_banners.category_id')
            ->where($where)
            ->first();
    }

    /**
    * Get Side Banner
    *
    * @param  array $where, $orderBy, $skip, $take
    * @return Illuminate\Database\Query\Builder
    */
    public function getSideBanner($where, $orderBy, $skip = null, $take = null)
    {
        return $this->model::select('sb.alt_text', 'sb.click_url', 'sb.image', 'sb.is_featured')
        ->from('side_banners as sb')
        ->leftJoin('banner_types as bt', 'sb.banner_type_id', '=', 'bt.id')
        ->where($where)
        //->orderBy('sb.alt_text', $orderBy)
        ->orderByRaw('RAND()')
        ->skip($skip)
        ->take($take)
        ->get();
    }
}
