<?php

namespace App\Repositories;

use App\Models\Interfaces\BannerInterface;
use App\Repositories\Interfaces\BannerRepositoryInterface;

/**
 * Class BannerRepository
 *
 * @author Ghulam Mustafa <ghulam.mustafa@vservices.com>
 * @date   05/10/18
 */
class BannerRepository implements BannerRepositoryInterface
{
    private $model;

    public function __construct(BannerInterface $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        return $this->model::all();
    }

    public function create(array $data)
    {
        $model = $this->model::create($data);

        return $model;
    }

    public function find($id)
    {
        return $this->model::where('id', $id)->first();
    }

    public function update($id, array $data)
    {
        $this->model::where('id', $id)->update($data);
    }

    public function delete($id)
    {
        $this->model::where('id', $id)->delete();
    }

    public function showBanner($where = [])
    {
        return $this->model::select(
            'banners.*',
            'banner_types.name As banner_type_name',
            'admins.first_name',
            'admins.last_name',
            'stores.name As store_name'
        )
            ->leftJoin('banner_types', 'banner_types.id', 'banners.banner_type_id')
            ->leftJoin('stores', 'stores.id', 'banners.store_id')
            ->leftJoin('admins', 'admins.id', 'banners.admin_id')
            ->where($where)
            ->first();
    }

    /**
    * Get Side Banner
    *
    * @param  array $where, $orderBy, $skip, $take
    * @return Illuminate\Database\Query\Builder
    */
    public function getStoreBanner($where, $orderBy = 'ASC', $skip = null, $take = null)
    {
        $records = $this->model::select('b.alt_text', 'b.click_url', 'b.image')
        ->from('banners as b')
        ->where($where)
        ->orderBy('b.position', $orderBy);
        //->orderByRaw('RAND()');

        if($take)
            $records = $records->take($take);
        if($skip)
            $records = $records->skip($skip);

        return $records = $records->get()->toArray();
    }
}
