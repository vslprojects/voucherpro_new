<?php

namespace App\Repositories\Interfaces;

/**
 * Interface OfferRepositoryInterface
 *
 * @author Ghulam Mustafa <ghulam.mustafa@vservices.com>
 * @date   29/11/18
 */
interface OfferRepositoryInterface extends RepositoryInterface
{

}
