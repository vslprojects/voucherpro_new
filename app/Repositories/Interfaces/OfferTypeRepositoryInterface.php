<?php

namespace App\Repositories\Interfaces;

/**
 * Interface OfferTypeRepositoryInterface
 *
 * @author Ghulam Mustafa <ghulam.mustafa@vservices.com>
 * @date   29/11/18
 */
interface OfferTypeRepositoryInterface extends RepositoryInterface
{

}
