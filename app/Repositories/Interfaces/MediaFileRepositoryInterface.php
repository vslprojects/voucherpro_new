<?php

namespace App\Repositories\Interfaces;

/**
 * Interface MediaFileRepositoryInterface
 *
 * @author Ghulam Mustafa <ghulam.mustafa@vservices.com>
 * @date   29/11/18
 */
interface MediaFileRepositoryInterface extends RepositoryInterface
{

}
