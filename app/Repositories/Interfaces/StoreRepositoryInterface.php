<?php

namespace App\Repositories\Interfaces;

/**
 * Interface StoreRepositoryInterface
 *
 * @author Ghulam Mustafa <ghulam.mustafa@vservices.com>
 * @date   29/11/18
 */
interface StoreRepositoryInterface extends RepositoryInterface
{

}
