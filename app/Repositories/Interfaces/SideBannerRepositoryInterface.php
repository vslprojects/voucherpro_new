<?php

namespace App\Repositories\Interfaces;

/**
 * Interface SideBannerRepositoryInterface
 *
 * @author Ghulam Mustafa <ghulam.mustafa@vservices.com>
 * @date   29/11/18
 */
interface SideBannerRepositoryInterface extends RepositoryInterface
{

}
