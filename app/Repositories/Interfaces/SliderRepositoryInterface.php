<?php

namespace App\Repositories\Interfaces;

/**
 * Interface SliderRepositoryInterface
 *
 * @author Ghulam Mustafa <ghulam.mustafa@vservices.com>
 * @date   29/11/18
 */
interface SliderRepositoryInterface extends RepositoryInterface
{

}
