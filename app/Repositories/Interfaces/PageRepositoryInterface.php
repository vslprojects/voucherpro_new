<?php

namespace App\Repositories\Interfaces;

/**
 * Interface PageRepositoryInterface
 *
 * @author Ghulam Mustafa <ghulam.mustafa@vservices.com>
 * @date   29/11/18
 */
interface PageRepositoryInterface extends RepositoryInterface
{

}
