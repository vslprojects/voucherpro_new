<?php

namespace App\Repositories\Interfaces;

/**
 * Interface ContactQueryRepositoryInterface
 *
 * @author Ghulam Mustafa <ghulam.mustafa@vservices.com>
 * @date   29/11/18
 */
interface ContactQueryRepositoryInterface extends RepositoryInterface
{

}
