<?php

namespace App\Repositories\Interfaces;

/**
 * Interface CategoryRepositoryInterface
 *
 * @author Ghulam Mustafa <ghulam.mustafa@vservices.com>
 * @date   29/11/18
 */
interface CategoryRepositoryInterface extends RepositoryInterface
{

}
