<?php

namespace App\Repositories\Interfaces;

/**
 * Interface IpAddressRepositoryInterface
 *
 * @author Muhammad Shahab Khan <muhammad.shahab@vservices.com>
 * @date   2/1/2021
 */
interface IpAddressRepositoryInterface extends RepositoryInterface
{

}
