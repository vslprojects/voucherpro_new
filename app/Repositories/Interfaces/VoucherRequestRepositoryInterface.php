<?php

namespace App\Repositories\Interfaces;

/**
 * Interface VoucherRequestRepositoryInterface
 *
 * @author Ghulam Mustafa <ghulam.mustafa@vservices.com>
 * @date   29/11/18
 */
interface VoucherRequestRepositoryInterface extends RepositoryInterface
{

}
