<?php

namespace App\Repositories\Interfaces;

/**
 * Interface EventRepositoryInterface
 *
 * @author Ghulam Mustafa <ghulam.mustafa@vservices.com>
 * @date   29/11/18
 */
interface EventRepositoryInterface extends RepositoryInterface
{

}
