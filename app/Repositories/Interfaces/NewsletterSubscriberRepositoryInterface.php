<?php

namespace App\Repositories\Interfaces;

/**
 * Interface NewsletterSubscriberRepositoryInterface
 *
 * @author Ghulam Mustafa <ghulam.mustafa@vservices.com>
 * @date   29/11/18
 */
interface NewsletterSubscriberRepositoryInterface extends RepositoryInterface
{

}
