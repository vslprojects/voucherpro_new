<?php

namespace App\Repositories\Interfaces;

/**
 * Interface BannerTypeRepositoryInterface
 *
 * @author Ghulam Mustafa <ghulam.mustafa@vservices.com>
 * @date   29/11/18
 */
interface BannerTypeRepositoryInterface extends RepositoryInterface
{

}
