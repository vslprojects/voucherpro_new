<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class RepositoryServiceProvider
 *
 * @author Ghulam Mustafa <ghulam.mustafa@vservices.com>
 * @date   29/11/18
 */
class RepositoryServiceProvider extends ServiceProvider
{
    public function boot()
    {
        //
    }

    /**
     * Bind the interface to an implementation repository class
     */
    public function register()
    {
        $this->app->bind('App\Repositories\Interfaces\AdminRepositoryInterface', 'App\Repositories\AdminRepository');
        $this->app->bind(
            'App\Repositories\Interfaces\ContactQueryRepositoryInterface',
            'App\Repositories\ContactQueryRepository'
        );
        $this->app->bind('App\Repositories\Interfaces\EventRepositoryInterface', 'App\Repositories\EventRepository');
        $this->app->bind(
            'App\Repositories\Interfaces\MediaFileRepositoryInterface',
            'App\Repositories\MediaFileRepository'
        );
        $this->app->bind('App\Repositories\Interfaces\MenuRepositoryInterface', 'App\Repositories\MenuRepository');
        $this->app->bind('App\Repositories\Interfaces\PageRepositoryInterface', 'App\Repositories\PageRepository');
        $this->app->bind(
            'App\Repositories\Interfaces\SiteSettingRepositoryInterface',
            'App\Repositories\SiteSettingRepository'
        );
        $this->app->bind('App\Repositories\Interfaces\SliderRepositoryInterface', 'App\Repositories\SliderRepository');
        $this->app->bind('App\Repositories\Interfaces\UserRepositoryInterface', 'App\Repositories\UserRepository');
        $this->app->bind(
            'App\Repositories\Interfaces\NewsletterSubscriberRepositoryInterface',
            'App\Repositories\NewsletterSubscriberRepository'
        );
        $this->app->bind('App\Repositories\Interfaces\BannerRepositoryInterface', 'App\Repositories\BannerRepository');
        $this->app->bind(
            'App\Repositories\Interfaces\BannerTypeRepositoryInterface',
            'App\Repositories\BannerTypeRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\CategoryRepositoryInterface',
            'App\Repositories\CategoryRepository'
        );
        $this->app->bind('App\Repositories\Interfaces\OfferRepositoryInterface', 'App\Repositories\OfferRepository');
        $this->app->bind(
            'App\Repositories\Interfaces\OfferTypeRepositoryInterface',
            'App\Repositories\OfferTypeRepository'
        );
        $this->app->bind('App\Repositories\Interfaces\RatingRepositoryInterface', 'App\Repositories\RatingRepository');
        $this->app->bind(
            'App\Repositories\Interfaces\SideBannerRepositoryInterface',
            'App\Repositories\SideBannerRepository'
        );
        $this->app->bind('App\Repositories\Interfaces\StoreRepositoryInterface', 'App\Repositories\StoreRepository');
        $this->app->bind(
            'App\Repositories\Interfaces\VoucherRequestRepositoryInterface',
            'App\Repositories\VoucherRequestRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\PostRepositoryInterface',
            'App\Repositories\PostRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\CommentRepositoryInterface',
            'App\Repositories\CommentRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\IpAddressRepositoryInterface',
            'App\Repositories\IpAddressRepository'
        );
    }
}
