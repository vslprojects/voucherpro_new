<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class ModelServiceProvider
 *
 * @author Ghulam Mustafa <ghulam.mustafa@vservices.com>
 * @date   29/11/18
 */
class ModelServiceProvider extends ServiceProvider
{
    public function boot()
    {
        //
    }

    /**
     * Bind the interface to an implementation model class
     */
    public function register()
    {
        $this->app->bind('App\Models\Interfaces\AdminInterface', 'App\Models\Admin');
        $this->app->bind('App\Models\Interfaces\ContactQueryInterface', 'App\Models\ContactQuery');
        $this->app->bind('App\Models\Interfaces\EventInterface', 'App\Models\Event');
        $this->app->bind('App\Models\Interfaces\MediaFileInterface', 'App\Models\MediaFile');
        $this->app->bind('App\Models\Interfaces\MenuInterface', 'App\Models\Menu');
        $this->app->bind('App\Models\Interfaces\PageInterface', 'App\Models\Page');
        $this->app->bind('App\Models\Interfaces\SiteSettingInterface', 'App\Models\SiteSetting');
        $this->app->bind('App\Models\Interfaces\SliderInterface', 'App\Models\Slider');
        $this->app->bind('App\Models\Interfaces\UserInterface', 'App\Models\User');
        $this->app->bind('App\Models\Interfaces\BannerInterface', 'App\Models\Banner');
        $this->app->bind('App\Models\Interfaces\BannerTypeInterface', 'App\Models\BannerType');
        $this->app->bind('App\Models\Interfaces\CategoryInterface', 'App\Models\Category');
        $this->app->bind('App\Models\Interfaces\NewsletterSubscriberInterface', 'App\Models\NewsletterSubscriber');
        $this->app->bind('App\Models\Interfaces\OfferInterface', 'App\Models\Offer');
        $this->app->bind('App\Models\Interfaces\OfferTypeInterface', 'App\Models\OfferType');
        $this->app->bind('App\Models\Interfaces\RatingInterface', 'App\Models\Rating');
        $this->app->bind('App\Models\Interfaces\SideBannerInterface', 'App\Models\SideBanner');
        $this->app->bind('App\Models\Interfaces\StoreInterface', 'App\Models\Store');
        $this->app->bind('App\Models\Interfaces\VoucherRequestInterface', 'App\Models\VoucherRequest');
        $this->app->bind('App\Models\Interfaces\PostInterface', 'App\Models\Post');
        $this->app->bind('App\Models\Interfaces\CommentInterface', 'App\Models\Comment');
        $this->app->bind('App\Models\Interfaces\IpAddressInterface', 'App\Models\IpAddress');
    }
}
