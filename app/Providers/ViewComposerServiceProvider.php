<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Store;
use App\Models\SideBanner;
use App\Models\Event;
use App\Models\Offer;

/**
 * Class ViewComposerServiceProvider
 *
 * @author Ghulam Mustafa <ghulam.mustafa@vservices.com>
 * @date   30/11/18
 */
class ViewComposerServiceProvider extends ServiceProvider
{


    public function boot()
    {
        $this->composeAdminPages();
        view()->composer('*', function ($view)
        {
            $date = date('Y-m-d');

            //get featured stores
            $gFeaturedStores = Store::select('s.id','s.name','s.logo','s.website','s.description')
            ->from('stores as s')
            ->where(['s.is_featured' => 1, 's.is_active'=>1])
            ->orderBy('s.position')->skip(0)->take(10)->get();

            //get site banners
            $gSideBanners = SideBanner::select('sb.alt_text', 'sb.click_url', 'sb.image', 'sb.is_featured')
            ->from('side_banners as sb')
            ->where(['sb.is_active' => 1,'sb.banner_type_id' => 1])
            ->orderByRaw('RAND()')->skip(0)->take(2)->get();

            //get events
            $gEvents = Event::select('e.name','e.slug')
            ->from('events as e')
            ->where(['e.is_active'=>1])
            ->orderBy('e.name')->skip(0)->take(20)->get();

            //count expried offers
            $countOffers = Offer::where('offers.expiry_date', '<', $date)->count();

            $total = $countOffers;

            $headerCounts = [
                'expired_offers'    => $countOffers,
                'total'             => $total
            ];

            $view->with(compact([
                'gFeaturedStores',
                'gSideBanners',
                'gEvents',
                'headerCounts'
            ]));
        });
    }

    public function register()
    {
        //
    }

    /**
     * Compose the admin pages
     *
     * e-g: admin page titles etc.
     */
    private function composeAdminPages()
    {
        /*
         * Dashboard
         */
        view()->composer('admin.dashboard.index', function ($view) {
            $view->with(['pageTitle' => 'Dashboard']);
        });

        /*
         * Administrators
         */
        view()->composer('admin.administrators.index', function ($view) {
            $view->with(['pageTitle' => 'Administrator List']);
        });
        view()->composer('admin.administrators.create', function ($view) {
            $view->with(['pageTitle' => 'Add Administrator']);
        });
        view()->composer('admin.administrators.show', function ($view) {
            $view->with(['pageTitle' => 'Show Administrator']);
        });
        view()->composer('admin.administrators.edit', function ($view) {
            $view->with(['pageTitle' => 'Edit Administrator']);
        });

        /*
         * Ip Addresses
         */
        view()->composer('admin.ipaddresses.index', function ($view) {
            $view->with(['pageTitle' => 'Ip Addresses List']);
        });
        view()->composer('admin.ipaddresses.create', function ($view) {
            $view->with(['pageTitle' => 'Add Ip Address']);
        });
        view()->composer('admin.ipaddresses.show', function ($view) {
            $view->with(['pageTitle' => 'Show Ip Address']);
        });
        view()->composer('admin.ipaddresses.edit', function ($view) {
            $view->with(['pageTitle' => 'Edit Ip Address']);
        });

        /*
         * Pages
         */
        view()->composer('admin.pages.index', function ($view) {
            $view->with(['pageTitle' => 'Page List']);
        });
        view()->composer('admin.pages.create', function ($view) {
            $view->with(['pageTitle' => 'Add Page']);
        });
        view()->composer('admin.pages.show', function ($view) {
            $view->with(['pageTitle' => 'Show Page']);
        });
        view()->composer('admin.pages.edit', function ($view) {
            $view->with(['pageTitle' => 'Edit Page']);
        });

        /*
         * Menus
         */
        view()->composer('admin.menus.index', function ($view) {
            $view->with(['pageTitle' => 'Menu List']);
        });
        view()->composer('admin.menus.create', function ($view) {
            $view->with(['pageTitle' => 'Add Menu']);
        });
        view()->composer('admin.menus.show', function ($view) {
            $view->with(['pageTitle' => 'Show Menu']);
        });
        view()->composer('admin.menus.edit', function ($view) {
            $view->with(['pageTitle' => 'Edit Menu']);
        });

        /*
         * Events
         */
        view()->composer('admin.events.index', function ($view) {
            $view->with(['pageTitle' => 'Events List']);
        });
        view()->composer('admin.events.create', function ($view) {
            $view->with(['pageTitle' => 'Add Events']);
        });
        view()->composer('admin.events.show', function ($view) {
            $view->with(['pageTitle' => 'Show Events']);
        });
        view()->composer('admin.events.edit', function ($view) {
            $view->with(['pageTitle' => 'Edit Events']);
        });

        /*
         * Sliders
         */
        view()->composer('admin.sliders.index', function ($view) {
            $view->with(['pageTitle' => 'Slider List']);
        });
        view()->composer('admin.sliders.create', function ($view) {
            $view->with(['pageTitle' => 'Add Slider']);
        });
        view()->composer('admin.sliders.show', function ($view) {
            $view->with(['pageTitle' => 'Show Slider']);
        });
        view()->composer('admin.sliders.edit', function ($view) {
            $view->with(['pageTitle' => 'Edit Slider']);
        });

        /*
         * Galleries
         */
        view()->composer('admin.galleries.index', function ($view) {
            $view->with(['pageTitle' => 'Gallery List']);
        });
        view()->composer('admin.galleries.create', function ($view) {
            $view->with(['pageTitle' => 'Add Gallery']);
        });
        view()->composer('admin.galleries.show', function ($view) {
            $view->with(['pageTitle' => 'Show Gallery']);
        });
        view()->composer('admin.galleries.edit', function ($view) {
            $view->with(['pageTitle' => 'Edit Gallery']);
        });

        /*
         * News
         */
        view()->composer('admin.news.index', function ($view) {
            $view->with(['pageTitle' => 'News List']);
        });
        view()->composer('admin.news.create', function ($view) {
            $view->with(['pageTitle' => 'Add News']);
        });
        view()->composer('admin.news.show', function ($view) {
            $view->with(['pageTitle' => 'Show News']);
        });
        view()->composer('admin.news.edit', function ($view) {
            $view->with(['pageTitle' => 'Edit News']);
        });

        /*
         * FAQs
         */
        view()->composer('admin.faqs.index', function ($view) {
            $view->with(['pageTitle' => 'FAQ List']);
        });
        view()->composer('admin.faqs.create', function ($view) {
            $view->with(['pageTitle' => 'Add FAQ']);
        });
        view()->composer('admin.faqs.show', function ($view) {
            $view->with(['pageTitle' => 'Show FAQ']);
        });
        view()->composer('admin.faqs.edit', function ($view) {
            $view->with(['pageTitle' => 'Edit FAQ']);
        });

        /*
         * Media File
         */
        view()->composer('admin.media_files.index', function ($view) {
            $view->with(['pageTitle' => 'Media File List']);
        });
        view()->composer('admin.media_files.create', function ($view) {
            $view->with(['pageTitle' => 'Add Media File']);
        });
        view()->composer('admin.media_files.show', function ($view) {
            $view->with(['pageTitle' => 'Show Media File']);
        });
        view()->composer('admin.media_files.edit', function ($view) {
            $view->with(['pageTitle' => 'Edit Media File']);
        });

        /*
         * Site Setting
         */
        view()->composer('admin.siteSettings', function ($view) {
            $view->with(['pageTitle' => 'Site Settings']);
        });

        /*
         * Change Password
         */
        view()->composer('admin.users.changePassword', function ($view) {
            $view->with(['pageTitle' => 'Change Password']);
        });

        /*
         * Affiliate Networks
         */
        view()->composer('admin.affiliateNetworks.index', function ($view) {
            $view->with(['pageTitle' => 'Affiliate Networks List']);
        });
        /*view()->composer('admin.categories.create', function ($view) {
            $view->with(['pageTitle' => 'Add Category']);
        });*/
        view()->composer('admin.affiliateNetworks.show', function ($view) {
            $view->with(['pageTitle' => 'Affiliate Network']);
        });
        /*view()->composer('admin.categories.edit', function ($view) {
            $view->with(['pageTitle' => 'Edit Category']);
        });*/

        /*
         * Categories
         */
        view()->composer('admin.categories.index', function ($view) {
            $view->with(['pageTitle' => 'Categories List']);
        });
        view()->composer('admin.categories.create', function ($view) {
            $view->with(['pageTitle' => 'Add Category']);
        });
        view()->composer('admin.categories.show', function ($view) {
            $view->with(['pageTitle' => 'Show Category']);
        });
        view()->composer('admin.categories.edit', function ($view) {
            $view->with(['pageTitle' => 'Edit Category']);
        });

        /*
         * Banner Type
         */
        view()->composer('admin.banner_types.index', function ($view) {
            $view->with(['pageTitle' => 'Banner Type List']);
        });
        view()->composer('admin.banner_types.create', function ($view) {
            $view->with(['pageTitle' => 'Add Banner Type']);
        });
        view()->composer('admin.banner_types.show', function ($view) {
            $view->with(['pageTitle' => 'Show Banner Type']);
        });
        view()->composer('admin.banner_types.edit', function ($view) {
            $view->with(['pageTitle' => 'Edit Banner Type']);
        });

        /*
         * Stores
         */
        view()->composer('admin.stores.index', function ($view) {
            $view->with(['pageTitle' => 'Stores List']);
        });
        view()->composer('admin.stores.create', function ($view) {
            $view->with(['pageTitle' => 'Add Store']);
        });
        view()->composer('admin.stores.show', function ($view) {
            $view->with(['pageTitle' => 'Show Store']);
        });
        view()->composer('admin.stores.edit', function ($view) {
            $view->with(['pageTitle' => 'Edit Store']);
        });view()->composer('admin.stores.show', function ($view) {
            $view->with(['offerpageTitle' => 'Offer List']);
        });

        view()->composer('admin.stores.withoutcategory', function ($view) {
            $view->with(['pageTitle' => 'Stores Without Category']);
        });

        view()->composer('admin.stores.blocked', function ($view) {
            $view->with(['pageTitle' => 'Blocked Stores List']);
        });

        /*
         * Newsletter Subscribers
         */
        view()->composer('admin.newsletter_subscribers.index', function ($view) {
            $view->with(['pageTitle' => 'Newsletter Subscribers List']);
        });

        /*
         * Offer Type
         */
        view()->composer('admin.offer_types.index', function ($view) {
            $view->with(['pageTitle' => 'Offer Type List']);
        });
        view()->composer('admin.offer_types.create', function ($view) {
            $view->with(['pageTitle' => 'Add Offer Type']);
        });
        view()->composer('admin.offer_types.show', function ($view) {
            $view->with(['pageTitle' => 'Show Offer Type']);
        });
        view()->composer('admin.offer_types.edit', function ($view) {
            $view->with(['pageTitle' => 'Edit Offer Type']);
        });

        /*
         * Voucher Requests
         */
        view()->composer('admin.voucher_requests.index', function ($view) {
            $view->with(['pageTitle' => 'Voucher Requests List']);
        });
        view()->composer('admin.voucher_requests.show', function ($view) {
            $view->with(['pageTitle' => 'Show Voucher Request']);
        });

        /*
         * Ratings
         */
        view()->composer('admin.ratings.index', function ($view) {
            $view->with(['pageTitle' => 'Ratings List']);
        });
        view()->composer('admin.ratings.show', function ($view) {
            $view->with(['pageTitle' => 'Show Ratings']);
        });

        /*
         * Side Banner
         */
        view()->composer('admin.side_banners.index', function ($view) {
            $view->with(['pageTitle' => 'Side Banners List']);
        });
        view()->composer('admin.side_banners.create', function ($view) {
            $view->with(['pageTitle' => 'Add Side Banners']);
        });
        view()->composer('admin.side_banners.show', function ($view) {
            $view->with(['pageTitle' => 'Show Side Banners']);
        });
        view()->composer('admin.side_banners.edit', function ($view) {
            $view->with(['pageTitle' => 'Edit Side Banners']);
        });

        /*
         * Offers
         */
        view()->composer('admin.offers.index', function ($view) {
            $view->with(['pageTitle' => 'Offers List']);
        });
        view()->composer('admin.offers.create', function ($view) {
            $view->with(['pageTitle' => 'Add Offer']);
        });
        view()->composer('admin.offers.show', function ($view) {
            $view->with(['pageTitle' => 'Show Offer']);
        });
        view()->composer('admin.offers.edit', function ($view) {
            $view->with(['pageTitle' => 'Edit Offer']);
        });
        view()->composer('admin.offers.expired', function ($view) {
            $view->with(['pageTitle' => 'Expired Offers List']);
        });

        /*
         * Banner
         */
        view()->composer('admin.banners.index', function ($view) {
            $view->with(['pageTitle' => 'Banners List']);
        });
        view()->composer('admin.banners.create', function ($view) {
            $view->with(['pageTitle' => 'Add Banners']);
        });
        view()->composer('admin.banners.show', function ($view) {
            $view->with(['pageTitle' => 'Show Banners']);
        });
        view()->composer('admin.banners.edit', function ($view) {
            $view->with(['pageTitle' => 'Edit Banners']);
        });

        /*
         * Posts
         */
        view()->composer('admin.posts.index', function ($view) {
            $view->with(['pageTitle' => 'Blog List']);
        });
        view()->composer('admin.posts.create', function ($view) {
            $view->with(['pageTitle' => 'Add Blog']);
        });
        view()->composer('admin.posts.show', function ($view) {
            $view->with(['pageTitle' => 'Show Blog']);
        });
        view()->composer('admin.posts.edit', function ($view) {
            $view->with(['pageTitle' => 'Edit Blog']);
        });
    }
}
