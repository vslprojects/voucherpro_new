<?php

namespace App\Http\Controllers\Front;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Repositories\Interfaces\SiteSettingRepositoryInterface;
use App\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Repositories\Interfaces\menuRepositoryInterface;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    private $siteSettingRepository;
    private $categoryRepository;
    private $menuRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        SiteSettingRepositoryInterface $siteSettingRepository,
        CategoryRepositoryInterface $categoryRepository,
        MenuRepositoryInterface $menuRepository
    ) {
        $this->siteSettingRepository = $siteSettingRepository;
        $this->categoryRepository    = $categoryRepository;
        $this->menuRepository    = $menuRepository;

        \View::share('siteSettings', $this->siteSettingRepository->findFirst());
        \View::share('menus', $this->menuRepository->getAllRecords());
        \View::share('featuredCategories', $this->categoryRepository->featuredCategories(
            ['is_featured' => 1, 'is_active' => 1], 'ASC', 0, 8)
        );
    }
}
