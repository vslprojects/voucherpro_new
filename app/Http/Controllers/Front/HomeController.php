<?php

namespace App\Http\Controllers\Front;

use App\Repositories\Interfaces\MediaFileRepositoryInterface;
use Illuminate\Http\Request;
use App\Repositories\Interfaces\SiteSettingRepositoryInterface;
use App\Repositories\Interfaces\PageRepositoryInterface;
use App\Repositories\Interfaces\NewsletterSubscriberRepositoryInterface;
use App\Repositories\Interfaces\StoreRepositoryInterface;
use App\Repositories\Interfaces\SideBannerRepositoryInterface;
use App\Repositories\Interfaces\SliderRepositoryInterface;
use App\Repositories\Interfaces\OfferRepositoryInterface;
use App\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Repositories\Interfaces\MenuRepositoryInterface;
use App\Repositories\Interfaces\EventRepositoryInterface;
use App\Repositories\Interfaces\ContactQueryRepositoryInterface;
use App\Repositories\Interfaces\VoucherRequestRepositoryInterface;
use App\Repositories\Interfaces\BannerRepositoryInterface;
use App\Repositories\Interfaces\RatingRepositoryInterface;
use App\Repositories\Interfaces\PostRepositoryInterface;
use App\Repositories\Interfaces\CommentRepositoryInterface;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;

class HomeController extends Controller
{
    private $siteSettingRepository;
    private $pageRepository;
    private $newsletterRepository;
    private $storeRepository;
    private $sideBannerRepository;
    private $sliderRepository;
    private $offerRepository;
    private $categoryRepository;
    private $menuRepository;
    private $eventRepository;
    private $contactRepository;
    private $voucherRepository;
    private $bannerRepository;
    private $ratingRepository;
    private $postRepository;
    private $commentRepository;
    private $mediaFileRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        SiteSettingRepositoryInterface $siteSettingRepository,
        PageRepositoryInterface $pageRepository,
        NewsletterSubscriberRepositoryInterface $newsletterRepository,
        StoreRepositoryInterface $storeRepository,
        SideBannerRepositoryInterface $sideBannerRepository,
        SliderRepositoryInterface $sliderRepository,
        OfferRepositoryInterface $offerRepository,
        CategoryRepositoryInterface $categoryRepository,
        MenuRepositoryInterface $menuRepository,
        EventRepositoryInterface $eventRepository,
        ContactQueryRepositoryInterface $contactRepository,
        VoucherRequestRepositoryInterface $voucherRepository,
        BannerRepositoryInterface $bannerRepository,
        RatingRepositoryInterface $ratingRepository,
        PostRepositoryInterface $postRepository,
        CommentRepositoryInterface $commentRepository,
        MediaFileRepositoryInterface $mediaFileRepository
    ) {

        // parameter is required for parent controller
       //parent::__construct($siteSettingRepository, $categoryRepository, $menuRepository);


        $this->pageRepository        = $pageRepository;
        $this->categoryRepository    = $categoryRepository;
        $this->newsletterRepository  = $newsletterRepository;
        $this->siteSettingRepository = $siteSettingRepository;
        $this->storeRepository       = $storeRepository;
        $this->sideBannerRepository  = $sideBannerRepository;
        $this->sliderRepository      = $sliderRepository;
        $this->offerRepository       = $offerRepository;
        $this->eventRepository       = $eventRepository;
        $this->contactRepository     = $contactRepository;
        $this->voucherRepository     = $voucherRepository;
        $this->bannerRepository      = $bannerRepository;
        $this->ratingRepository      = $ratingRepository;
        $this->postRepository        = $postRepository;
        $this->commentRepository     = $commentRepository;
        $this->mediaFileRepository     = $mediaFileRepository;
    }

    /**
     * Show home page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $homePage = $this->pageRepository->getPage(['pages.slug' => 'home']);
        $featuredStore = $this->storeRepository->getStore(['s.is_featured' => 1, 's.is_active'=>1], 'ASC', 0, 9);
        $featuredVouchers = $this->offerRepository->getFeaturedtopVoucher([
            'o.is_featured' => 1,
            'o.is_active'   => 1,
            'ot.name'       => 'Codes',
            's.is_active'   => 1], 'DESC', 0, 10);
        $topVouchers = $this->offerRepository->getFeaturedtopVoucher([
            'o.is_featured' => 1,
            'o.is_active'   => 1,
            's.is_active'   => 1], 'DESC', 0, 20);
        $sliders            = $this->sliderRepository->getAllSliders();


        $pageData = $this->pageRepository->getPage(['pages.slug' => 'home']);

        $metaData = [
            'title' => isset($pageData->meta_title) ? $pageData->meta_title : '',
            'description' => isset($pageData->meta_description) ? $pageData->meta_description : '',
            'keywords' => isset($pageData->meta_keywords) ? $pageData->meta_keywords : '',
            'image' => '',
            'url' => route('index'),
        ];

        if ($request->has('copy') || $request->has('shopnow')) {

            $offerDetail = [];
            $params = $request->all();

            $offerId = (isset($params['copy'])) ? $params['copy'] : $params['shopnow'];
            $filter['offers.id'] = $offerId;
            $offerDetail = $this->offerRepository->getOffersList($filter);
            $offerDetail = isset($offerDetail[0]) ? $offerDetail[0] : [];
        } else {
            $offerDetail = [];
        }


        //echo '<pre>'; print_r($homePage); exit;

        return view(
            'front.index'
            ,
            compact(
                'homePage',
                'featuredStore',
                'sliders',
                'featuredVouchers',
                'topVouchers',
                'metaData',
                'offerDetail',
                //'featuredSideBanner'
            )
        );
    }

    /**
     * Show contact page.
     *
     * @return \Illuminate\Http\Response
     */
    public function contact()
    {
        $pageData = $this->pageRepository->getPage(['pages.slug' => 'contact']);
        $metaData = [
            'title' => isset($pageData->meta_title) ? $pageData->meta_title : '',
            'description' => isset($pageData->meta_description) ? $pageData->meta_description : '',
            'keywords' => isset($pageData->meta_keywords) ? $pageData->meta_keywords : '',
            'image' => '',
            'url' => route('contact'),
        ];
        $breadcrumbs = (object)[
            'name' => 'common',
            'title' => 'Contact Us',
            'url' => route('contact')
        ];
        return view('front.contact', compact('metaData', 'breadcrumbs'));
    }

    /**
     * Process Contact Form
     *
     * @return void
     */
    public function processContact(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name'          => 'required',
            'email'         => 'required|email',
            'description'   => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'message'=>$validator->errors()->all()]);
        }
        $data = $request->except([
            '_token',
            '_method',
            'terms'
        ]);

        $this->contactRepository->create($data);
        $siteSettings = $this->siteSettingRepository->findFirst();
        $contact_email = $siteSettings->contact_email;
        /* Send email to user */
        \Mail::to($data['email'])->queue(
            new \App\Mail\ContactQueryEmail($data)
        );
        /* Send email to admin */
        \Mail::to($contact_email)->queue(
            new \App\Mail\ContactQueryAdminEmail($data)
        );

        return response()->json([
            'error' => false,
            'message'=>'Thank you, We have received your query']);
    }

    /**
     * Show search page.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        // $q     = trim($request->get('q'));
        // $pages = $this->pageRepository->searchPages($q);

        return view('front.search');
    }

    /**
     * Show cms page.
     *
     * @param  $slug Slug identifier of the resource/record
     *
     * @return \Illuminate\Http\Response
     */
    public function page(Request $request, $slug = '')
    {
        $page = isset($request->page) ?  $request->page : 1;

        $storePage = $this->storeRepository->getStore(['s.website' => trim($slug)]);

        if (count($storePage) > 0) {
            $filter = [
                'offers.is_active'  => 1,
                'offers.store_id'   => $storePage[0]->id,
                's.is_active'       => 1
            ];

            $offersCount = offersCountSorting($this->offerRepository->getOffersCount($filter));
            $offers = $this->offerRepository->getOffersListPaginate($filter, $orderBy = 'offers.position', $page);
            $popularStores = $this->storeRepository->getStore(
            ['s.is_featured' => 1, 's.is_active'=>1], 'ASC', 0, 10);
            $similarStores = $this->storeRepository->getSimilarStore(['store_id'=>$storePage[0]->id], 'DESC', 0, 10);

            $topOffers = sideBarTopOffers($offers);
            $offerDetail = [];

            $storeBanners = $this->bannerRepository->getStoreBanner([
                'b.is_active'   => 1,
                'b.store_id'    => $storePage[0]->id
            ]);

            $storeSideBanners = $this->sideBannerRepository->getSideBanner([
                'sb.is_active'      => 1,
                'sb.banner_type_id' => 2,
                'sb.category_id'    => $storePage[0]->category_id,
            ], 'ASC', 0, 10);

            $userRating = $this->ratingRepository->getRatings([
                'ratings.ip'=> $request->ip(),
                'ratings.is_approved' => 1,
                'ratings.store_id'=>$storePage[0]->id]
            );
            $avgRating = $this->ratingRepository->getAvgRattings([
                'r.is_approved' => 1,
                'r.store_id'    => $storePage[0]->id]
            );
            $avgRating = (isset($avgRating->rating)) ? $avgRating->rating : 0;
            $params = $request->all();

            if (count($params) > 0) {
                if(isset($params['copy'])) {
                    $offerId = $params['copy'];
                } elseif(isset($params['shopnow'])) {
                    $offerId = $params['shopnow'];
                }else{
                    $offerId = '';
                }
                // $offerId = (isset($params['copy'])) ? $params['copy'] : (isset($params['shopnow'])) ? $params['shopnow'] : '';
                $filter['offers.id'] = $offerId;
                $offerDetail = $this->offerRepository->getOffersList($filter);
                $offerDetail = isset($offerDetail[0]) ? $offerDetail[0] : [];
            }

            $metaData = [
                'title' => $storePage[0]->meta_title,
                'description' => $storePage[0]->meta_description,
                'keywords' => $storePage[0]->meta_keywords,
                'image' => uploadsUrl($storePage[0]->logo),
                'url' => route('page', $storePage[0]->website),
            ];

            $breadcrumbs = (object)[
                'name' => 'store',
                'title' => $storePage[0]->name,
                'url' => route('page', $storePage[0]->website),
                'catName' => $storePage[0]->catName,
                'catUrl' => route('category.details', $storePage[0]->catSlug)
            ];

            //echo '<pre>'; print_r($metaData); exit;

            return view('front.storeDetails', compact(
                'storePage',
                'offersCount',
                'offers',
                'popularStores',
                'similarStores',
                'topOffers',
                'offerDetail',
                'storeBanners',
                'storeSideBanners',
                'userRating',
                'avgRating',
                'metaData',
                'breadcrumbs'
            ));
        }


        $eventPage = $this->eventRepository->getEvents(['slug' => $slug]);
        if (count($eventPage) > 0) {
            $eventOffers  = $this->eventRepository->getOffersWithPaginate([
                'eo.event_id'   => $eventPage[0]->id,
                'o.is_active'   => 1,
                's.is_active'   => 1
            ], $page);

            $metaData = [
                'title' => $eventPage[0]->meta_title,
                'description' => $eventPage[0]->meta_description,
                'keywords' => $eventPage[0]->meta_keywords,
                'image' => (!empty($eventPage[0]->image)) ? uploadsUrl($eventPage[0]->image) : '',
                'url' => route('page', $eventPage[0]->slug),
            ];

            $breadcrumbs = (object)[
                'name' => 'common',
                'title' => $eventPage[0]->name,
                'url' => route('page', $eventPage[0]->slug)
            ];

                if ($request->has('copy') || $request->has('shopnow')) {

                $offerDetail = [];
                $params = $request->all();

                $offerId = (isset($params['copy'])) ? $params['copy'] : $params['shopnow'];
                $filter['offers.id'] = $offerId;
                $offerDetail = $this->offerRepository->getOffersList($filter);
                $offerDetail = isset($offerDetail[0]) ? $offerDetail[0] : [];
                } else {
                    $offerDetail = [];
                }


            return view('front.event', compact(
                'eventPage',
                'eventOffers',
                'metaData',
                'breadcrumbs',
                'offerDetail'
            ));
        }


        $cmsPage = $this->pageRepository->getPage(['pages.slug' => $slug]);

        if ($cmsPage) {
            $metaData = [
                'title'       => $cmsPage->meta_title,
                'description' => $cmsPage->meta_description,
                'keywords'    => $cmsPage->meta_keywords,
                'image'       => '',
                'url'         => route('page', $cmsPage->slug),
            ];

            $breadcrumbs = (object)[
                'name' => 'common',
                'title' => $cmsPage->page_title,
                'url' => route('page', $cmsPage->slug)
            ];

            return view('front.page', compact(
                'cmsPage',
                'metaData',
                'breadcrumbs'
            ));
        }

        // return view('front.errors.404');
        return redirect()->route('error.404');
    }

    /**
     * Show categories Page
     *
     * @return \Illuminate\Http\Response
     */
    public function category()
    {
        $categories = $this->categoryRepository->getCategoriesWithStores(
            ['categories.is_active'=>1],
            ['categories'=> ['orderBy'=>'name'],'stores' => ['take'=>20,'orderBy'=>'name']]
        );

        $pageData = $this->pageRepository->getPage(['pages.slug' => 'categories']);
        $metaData = [
            'title' => isset($pageData->meta_title) ? $pageData->meta_title : '',
            'description' => isset($pageData->meta_description) ? $pageData->meta_description : '',
            'keywords' => isset($pageData->meta_keywords) ? $pageData->meta_keywords : '',
            'image' => '',
            'url' => route('category'),
        ];

        $breadcrumbs = (object)[
            'name' => 'common',
            'title' => 'Categories',
            'url' => route('category')
        ];
        //echo '<pre>'; print_r($pageData); exit;
        return view('front.category', compact(
            'categories',
            'metaData',
            'breadcrumbs'
        ));
    }

    /**
     * Show  details page.
     *
     * @return \Illuminate\Http\Response
     */
    public function categoryDetails($slug = '')
    {

        $record = $this->categoryRepository->getCategoriesWithStoresPaginate(
            ['categories.is_active'=>1, 'slug'=> $slug],
            ['categories'=> ['orderBy'=>'name'],
            'stores' => ['orderBy'=>'name'],
            'stores' => ['page'=> request('page') > 1 ? request('page') : 1]
        ],
         $pePage = 24);

        if ($record) {
            $metaData = [
                'title' => isset($record[0]['meta_title']) ? $record[0]['meta_title'] : '',
                'description' => isset($record[0]['meta_description']) ? $record[0]['meta_description'] : '',
                'keywords' => isset($record[0]['meta_keywords']) ? $record[0]['meta_keywords'] : '',
                'image' => '',
                'url' => route('category.details', isset($record[0]['slug']) ? $record[0]['slug'] : ''),
            ];

            $breadcrumbs = (object)[
                'name' => 'category',
                'title' => isset($record[0]['name']) ? $record[0]['name'] : '',
                'url' => route('category.details', isset($record[0]['slug']) ? $record[0]['slug'] : '')
            ];

            //echo '<pre>'; print_r($record); exit;
            return view('front.categoryDetails', compact(
                'record',
                'metaData',
                'breadcrumbs'
            ));
        }

        // return view('front.errors.404');
        return redirect()->route('error.404');

    }

    /**
     * Show stores Page
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
//        $stores = splitStores($this->storeRepository->getStoresAndOfferCount(['s.is_active'=>1])); // Old Code
        $stores = splitStores($this->storeRepository->getStoresAndOfferCount()); // Edit By Shahab for SEO perspective

        $pageData = $this->pageRepository->getPage(['pages.slug' => 'all-stores']);
        $metaData = [
            'title' => isset($pageData->meta_title) ? $pageData->meta_title : '',
            'description' => isset($pageData->meta_description) ? $pageData->meta_description : '',
            'keywords' => isset($pageData->meta_keywords) ? $pageData->meta_keywords : '',
            'image' => '',
            'url' => route('category'),
        ];

        $breadcrumbs = (object)[
            'name' => 'common',
            'title' => 'All Store',
            'url' => route('store')
        ];

        return view('front.store', compact(
            'stores',
            'metaData',
            'breadcrumbs'
        ));
    }

    /**
     * Show Voucher Request Page
     *
     * @return \Illuminate\Http\Response
     */
    public function voucherRequest()
    {
        $metaData = '';
        $pageData = '';

        if (config('app.site') == 'voucherpro') {
            $pageData = $this->pageRepository->getPage(['pages.slug' => 'voucher-request-voucherpro']);
        } else if (config('app.site') == '247couponcodes') {
            $pageData = $this->pageRepository->getPage(['pages.slug' => 'voucher-request-247couponcodes']);
        }

        if (!empty($pageData)) {
            $metaData = [
                'title' => isset($pageData->meta_title) ? $pageData->meta_title : '',
                'description' => isset($pageData->meta_description) ? $pageData->meta_description : '',
                'keywords' => isset($pageData->meta_keywords) ? $pageData->meta_keywords : '',
                'image' => '',
                'url' => route('voucher.request'),
            ];
        }

        $breadcrumbs = (object)[
            'name' => 'common',
            'title' => 'Submit A Voucher',
            'url' => route('voucher.request')
        ];

        return view('front.voucherRequest', compact('breadcrumbs', 'metaData'));
    }

    /**
     * Process Voucher Request Form
     *
     * @return void
     */
    public function processVoucherRequest(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name'          => 'required',
            'email'         => 'required|email',
            'store'         => 'required',
            'description'   => 'required',
            'voucher'       => 'required',
            'expire_date'   => 'required'
        ]);

        if ($validator->fails())
        {
            return response()->json([
                'error' => true,
                'message'=>$validator->errors()->all()
            ]);
        }
        $data = $request->except([
            '_token',
            '_method',
            'terms'
        ]);
        $this->voucherRepository->create($data);
        return response()->json([
            'error' => false,
            'message'=>'Thank you for submitting voucher']);
    }

    /**
     * Show Amazon Page
     *
     * @return \Illuminate\Http\Response
     */
    public function amazon()
    {
        return view('front.amazon');
    }

    /**
     * Subscribe newsletter
     *
     * @return \Illuminate\Http\Response
     */
    public function newsletterSubscribe(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'email' => 'required'
        ]);

        if ($validator->fails())
        {
            return response()->json([
                'error' => true,
                'message'=>$validator->errors()->all()]);
        }
        $data = $request->except([
            '_token',
            '_method'
        ]);
        $this->newsletterRepository->subscribe($data);
        return response()->json([
            'error' => false,
            'message'=>'You have successfully subscribed for the newsletter']);
    }

    /**
     * Un Subscribe newsletter
     *
     * @return \Illuminate\Http\Response
     */
    function newsletterUnSubscribe()
    {
        $metaData = '';
        $pageData = '';

        if (config('app.site') == 'voucherpro') {
            $pageData = $this->pageRepository->getPage(['pages.slug' => 'newsletter-unsubscribe-voucherpro']);
        } else if (config('app.site') == '247couponcodes') {
            $pageData = $this->pageRepository->getPage(['pages.slug' => 'newsletter-unsubscribe-247couponcodes']);
        }

        if (!empty($pageData)) {
            $metaData = [
                'title' => isset($pageData->meta_title) ? $pageData->meta_title : '',
                'description' => isset($pageData->meta_description) ? $pageData->meta_description : '',
                'keywords' => isset($pageData->meta_keywords) ? $pageData->meta_keywords : '',
                'image' => '',
                'url' => route('newsletter.unsubscribe'),
            ];
        }

        $breadcrumbs = (object)[
            'name' => 'common',
            'title' => 'Unsubscribe Newsletter',
            'url' => route('newsletter.unsubscribe')
        ];

        return view('front.newsletterUnSubscribe', compact('breadcrumbs', 'metaData'));
    }

    /**
     * Un Subscribe newsletter submit
     *
     * @return \Illuminate\Http\Response
     */
    public function newsletterUnSubscribeSubmit(Request $request)
    {
        $data = [];
        $validator = \Validator::make($request->all(), [
            'email' => 'required'
        ]);

        if ($validator->fails()) {
            $data['alert_error'] = true;
        } else {
            $data['alert_success'] = true;
            $email = $request->get('email');
            $record = $this->newsletterRepository->subscribeGetRecord(['email'=>$email]);
            if($record) {
                $unsub_token = md5(time());
                $this->newsletterRepository->update($record->id,['unsub_token'=>$unsub_token]);
                $emailData = [
                    'link'  =>  route('newsletter.unsubscribe.verify',$unsub_token)];
                 \Mail::to($email)->queue(
                    new \App\Mail\UnsubscribeNewsletterEmail($emailData)
                );
            }
        }

        return view('front.newsletterUnSubscribe', $data);
    }

    /**
     * Un Subscribe newsletter
     *
     * @return \Illuminate\Http\Response
     */
    function newsletterUnSubscribeVerify($unsub_token)
    {
        $record = $this->newsletterRepository->subscribeGetRecord(['unsub_token'=>$unsub_token]);
        if($record) {
            $this->newsletterRepository->delete($record->id);
            /*return redirect()
            ->route('thank-you')
            ->with('success', 'You have successfully unsubscribed newsletter.');*/
            $message = 'You have successfully unsubscribed newsletter.';
            return view('front.success', compact('message'));
        }
        // return view('front.errors.404');
        return redirect()->route('error.404');
    }

    /**
     * Un Subscribe newsletter
     *
     * @return \Illuminate\Http\Response
     */
    function searchStores(Request $request)
    {
        $result = [];
        $filters = [
            'q' => $request->input('q')
        ];
        $stores = $this->storeRepository->simpleData($filters, 'ASC', 8, 0);
        if(count($stores) > 0) {
            $sData = searchDataTransformation($stores);
            $filters['catIds'] = $sData['catIds'];
            $result['stores'] = $sData['stores'];
        }
        $categories = $this->categoryRepository->searchCategories($filters, 'ASC', 8, 0);
        if(count($categories) > 0) {
            $result['categories'] = searchCatDataTransformation($categories);
        }
        //return response($record);
        return response()->json($result);
    }

    /**
     * search stores submit
     *
     * @return \Illuminate\Http\Response
     */
    public function searchStoresSubmit(Request $request)
    {
        $q = $request->input('q');
        $stores = $this->storeRepository->simpleData(['q'=>$q], 'ASC');

        $metaData = [
            'title' => 'Search - '.$q
        ];

        $breadcrumbs = (object)[
            'name' => 'common',
            'title' => 'Search',
            'url' => route('search.stores.submit')
        ];

        //echo '<pre>'; print_r($record); exit;
        return view('front.searchStores', compact(
            'q',
            'stores',
            'metaData',
            'breadcrumbs'
        ));
    }

    /**
     * store rating submit
     *
     * @return \Illuminate\Http\Response
     */
    public function storeRatingSubmit(Request $request)
    {
        $ip = $request->ip();
        $storeId = $request->input('store_id');
        $rating = $request->input('rating');

        $userRating = $this->ratingRepository->getRatings(['ratings.ip'=>$ip, 'ratings.store_id'=>$storeId]);
        $data = ['store_id'=>$storeId, 'rating'=>$rating, 'ip'=>$ip];
        if(count($userRating) > 0){
            $id = $userRating[0]->id;
            $this->ratingRepository->update($id, $data);
        } else {
            $this->ratingRepository->create($data);
        }

        return response()->json([
            'error' => false,
            'message'=>'You have successfully ratted on store']);
    }

    /**
     * update offer count
     *
     * @return \Illuminate\Http\Response
     */
    public function offerUpdateCount(Request $request)
    {
        $id = $request->input('id');
        $offer = $this->offerRepository->find($id);
        if($offer){
            $offer->views_count = $offer->views_count+1;
            $offer->timestamps = false;
            $offer->save();
//            $this->offerRepository->update($offer->id, ['views_count'=>$offer->views_count+1, 'updated_at' => false]);
        }
    }

    /**
     * Generate Sitemap.xml
     *
     * @return \Illuminate\Http\Response
     */
    public function runSitemap()
    {
        $sitemap = Sitemap::create();

        $pages = $this->pageRepository->all();
        foreach ($pages as $page) {
            $sitemap->add(Url::create("/{$page->slug}"));
        }

        $categories = $this->categoryRepository->getCategories(['is_active' => 1]);
        foreach ($categories as $category) {
            $sitemap->add(Url::create("/categories/{$category->slug}"));
        }

        $events = $this->eventRepository->getEvents(['is_active' => 1]);
        foreach ($events as $event) {
            $sitemap->add(Url::create("/{$event->slug}"));
        }

//        $stores = $this->storeRepository->getStore(['s.is_active' => 1]); //Old Code
        $stores = $this->storeRepository->getStore(); // Edit By Shahab For SEO perspective
        foreach ($stores as $store) {
            $sitemap->add(Url::create("/{$store->website}"));
        }

        $sitemap->writeToFile(public_path('sitemap.xml'));
    }

    public function extension()
    {
        $sliders       = $this->sliderRepository->getAllSliders();
        $featuredStore = $this->storeRepository->getStore(['s.is_featured' => 1, 's.is_active'=>1], 'ASC', 0, 9);

        $metaData = [
            'title'       => 'Google Extension',
            'description' => 'Voucherpro Google Extension',
            'keywords'    => 'Voucherpro Google Extension',
            'image'       => '',
            'url'         => route('extension'),
        ];

        return view('front.extension', compact('sliders','featuredStore', 'metaData'));
    }

    public function error404()
    {
        $metaData = '';
        $pageData = '';

        $pageData = $this->pageRepository->getPage(['pages.slug' => 'not-found']);

        if (!empty($pageData)) {
            $metaData = [
                'title' => isset($pageData->meta_title) ? $pageData->meta_title : '',
                'description' => isset($pageData->meta_description) ? $pageData->meta_description : '',
                'keywords' => isset($pageData->meta_keywords) ? $pageData->meta_keywords : '',
                'image' => '',
                'url' => route('error.404'),
            ];
        }

        return view('front.errors.404', compact('metaData'));
    }

    public function blogs(Request $request)
    {
        $sliders = $this->sliderRepository->getAllSliders();
        $page = $request->page > 1 ? $request->page : 1;
        $perPage = 9;
        $posts = $this->postRepository->getAllPosts($perPage, $page);
        // $posts   = $this->postRepository->all();

        $metaData = [
            'title'       => 'Blog',
            'description' => 'Voucherpro Blog',
            'keywords'    => 'Blog',
            'image'       => '',
            'url'         => route('blogs')
        ];

        return view('front.blogs', compact('metaData', 'sliders', 'posts'));
    }

    public function blogDetail($slug = '')
    {
        $blog = $this->postRepository->findBy(['slug' => $slug]);

        $getImage = $this->mediaFileRepository->find($blog->thumbnail_id);

        if ($blog) {
            $commnets = $this->commentRepository->allBy(['post_id' => $blog->id]);

            $metaData = [
                'title'       => $blog->meta_title,
                'description' => $blog->meta_description,
                'keywords'    => $blog->meta_keywords,
                'image'       => '',
                'url'         => route('blog-detail', $slug)
            ];

            $blog->thumbnail = ($blog->thumbnail_id != '') ? $getImage->filename : '';

            return view('front.blog_detail', compact('metaData', 'blog', 'commnets'));
        }

        return redirect()->route('error.404');

    }

    public function postComment(Request $request)
    {
        $data = $request->except([
            '_token',
            '_method',
            'slug'
        ]);

        $this->commentRepository->create($data);

        return redirect()
            ->route('blog-detail', $request->slug)
            ->with('success', 'Thank you! Your Comment has been added successfully.');
    }
}
