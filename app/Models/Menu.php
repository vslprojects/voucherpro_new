<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Interfaces\MenuInterface;

class Menu extends Model implements MenuInterface
{
    protected $guarded = [];
}
