<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Interfaces\OfferInterface;

class Offer extends Model implements OfferInterface
{
    protected $guarded = [];
}
