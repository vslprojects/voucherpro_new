<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Interfaces\StoreInterface;

class Store extends Model implements StoreInterface
{
    protected $guarded = [];
}
