<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Interfaces\VoucherRequestInterface;

class VoucherRequest extends Model implements VoucherRequestInterface
{
    protected $guarded = [];
}
