<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Interfaces\BannerInterface;

class Banner extends Model implements BannerInterface
{
    protected $guarded = [];
}
