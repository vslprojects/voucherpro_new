<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Interfaces\OfferTypeInterface;

class OfferType extends Model implements OfferTypeInterface 
{
    protected $guarded = [];
}
