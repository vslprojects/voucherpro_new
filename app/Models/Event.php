<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Interfaces\EventInterface;

class Event extends Model implements EventInterface
{
    protected $guarded = [];
}
