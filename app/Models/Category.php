<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Interfaces\CategoryInterface;

class Category extends Model implements CategoryInterface
{
	protected $guarded = [];
}
