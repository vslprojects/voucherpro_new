<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Interfaces\ContactQueryInterface;

class ContactQuery extends Model implements ContactQueryInterface
{
    protected $guarded = [];
}
