<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Interfaces\CommentInterface;

class Comment extends Model implements CommentInterface
{
    protected $guarded = [];
}
