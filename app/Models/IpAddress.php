<?php

namespace App\Models;

use App\Models\Interfaces\IpAddressInterface;
use Illuminate\Database\Eloquent\Model;

class IpAddress extends Model implements IpAddressInterface
{
    protected $guarded = [];

    /**
     * Get the admin information.
     */
    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id', 'id');
    }
}
