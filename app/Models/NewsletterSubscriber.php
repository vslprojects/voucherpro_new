<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Interfaces\NewsletterSubscriberInterface;

class NewsletterSubscriber extends Model implements NewsletterSubscriberInterface
{
    protected $guarded = [];
}
