<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Interfaces\SliderInterface;

class Slider extends Model implements SliderInterface
{
    protected $guarded = [];
}
