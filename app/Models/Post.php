<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Interfaces\PostInterface;

class Post extends Model implements PostInterface
{
    protected $guarded = [];
}
