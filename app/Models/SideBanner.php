<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Interfaces\SideBannerInterface;

class SideBanner extends Model implements SideBannerInterface
{
    protected $guarded = [];
}
