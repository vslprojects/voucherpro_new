<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Interfaces\BannerTypeInterface;

class BannerType extends Model implements BannerTypeInterface
{
    protected $guarded = [];
}