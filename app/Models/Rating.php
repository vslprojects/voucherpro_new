<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Interfaces\RatingInterface;

class Rating extends Model implements RatingInterface
{
    protected $guarded = [];
}
