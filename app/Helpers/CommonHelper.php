<?php

/**
 * CommonHelper
 *
 * @author Sheeraz Gul <sheeraz.gul@vservices.com>
 * @date   12/02/19
 */

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Return's admin assets directory
 *
 * CALLING PROCEDURE
 *
 * In controller call it like this:
 * $string = queryResultToString($result,$key);
 *
 * In View call it like this:
 * {{ queryResultToString(queryResultToString($result,$key)) }}
 *
 * @param array $result
 * @param string $key
 * @return string
 */

function queryResultToString($result, $key)
{
    $data = [];
    if (is_object($result)) {
        foreach ($result as $value) {
            $data[] = $value->$key;
        }
        $string = implode(', ', $data);
    } else {
        $string = $result;
    }
    return $string;
}

function offerDateFormat($date)
{
    return date('M-d-Y', strtotime($date));
}

function hideChr($string, $no_show = -2)
{
    //return str_pad(substr($string, $no_show), strlen($string), '*', STR_PAD_LEFT);
    return substr($string, $no_show);
}

function limitText($text, $limit)
{
    if (str_word_count($text, 0) > $limit) {
        $words = str_word_count($text, 2);
        $pos = array_keys($words);
        $text = substr($text, 0, $pos[$limit]) . '...';
    }
    return $text;
}

function splitStores($stores)
{
    $result = [];
    foreach ($stores as $store) {
        $chr = strtoupper(substr(trim($store['name']), 0, 1));
        if (is_numeric($chr)) {
            $result['0-9'][] = $store;
        } else {
            $result[$chr][] = $store;
        }
    }
    ksort($result);
    return $result;
}

function offersCountSorting($offers)
{
    $result['total'] = 0 ;
    foreach ($offers as $offer) {
        $key = strtolower($offer['type_name']);
        $key = str_replace(' ', '_', $key);
        $result[$key] = $offer['total'];
        $result['total'] += $offer['total'];
    }
    return $result;
}

function sideBarTopOffers($offers)
{
    $result = [];
    $filterArr = [];
    foreach ($offers as $offer) {
        if (!in_array($offer['offer_type'], $filterArr)) {
            $result[] = $offer;
            array_push($filterArr, $offer['offer_type']);
        }
    }
    return $result;
}

function searchDataTransformation($data)
{
    $result = [];
    foreach ($data as $row) {
        $row['url'] = route('page', $row['website']);
        $row['logoUrl'] = uploadsUrl($row['logo']);
        $result['stores'][] = $row;
        $result['catIds'][] = $row['id'];
    }
    return $result;
}

function searchCatDataTransformation($data)
{
    $result = [];
    foreach ($data as $row) {
        $row['url'] = route('category.details', $row['slug']);
        $row['logoUrl'] = uploadsUrl($row['image_small']);
        $result[] = $row;
    }
    return $result;
}

function setDiscountLabel($label, $offer_type)
{
    //free-shipping, off, sale
    $offer_type = str_replace(' ', '_', $offer_type);
    $label = strtolower($label);
    $result = [];
    if ($offer_type == 'free_shipping') {
        $result['class'] = 'free-shipping';
        $label = str_replace('free', '<span>Free</span>', $label);
    } else {
        $result['class'] = ($offer_type == 'codes') ? 'off' : 'sale';
        preg_match("([\£|\$]\d{1,5}|\d{1,5}\%?)u", $label, $match);
        if (isset($match[0])) {
            $label = str_replace($match[0], '<span>'.$match[0].'</span>', $label);
        }
    }

    if ($label == 'sale') {
        $label = str_replace('sale', '<span>Sale</span>', $label);
    }

    $result['text'] = $label;

    return $result;
}

function textToShare($str)
{
    return $str = str_replace('%', '%25', $str);
}

/**
 * check file exist
 * @param  sting $url   url of the file
 * @return boolen
 */
function remoteFileExists($url)
{
    $curl = curl_init($url);

    //don't fetch the actual page, you only want to check the connection is ok
    curl_setopt($curl, CURLOPT_NOBODY, true);

    //do request
    $result = curl_exec($curl);

    $ret = false;

    //if request did not fail
    if ($result !== false) {
        //if request was ok, check response code
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ($statusCode == 200) {
            $ret = true;
        }
    }

    curl_close($curl);

    return $ret;
}

function urlFilters($url)
{
    $url = str_replace(' ', '%20', $url);
    return $url;
}

/**
 * copied from voucherpro opencart project then modified
 * @return array
 */
function couponValueRange()
{
    $result = [];
    for ($i=1; $i<=99; $i++) {
        $result[] = $i.'%';
        $result[] = '$' .$i;
        $result[] = '£' .$i;
    }

    $result[] = 'Free';
    return $result;
}

/**
 * Converts XML to any of Array|Object|JSON
 *
 * @param string $xml
 * @param string $mode either array, object, or json
 *
 * @author Ali Nawaz Qureshi <alinawazbs@gmail.com>
 *
 * @return mixed
 */
function xmlToArray($xml, $mode = 'object')
{
    // $xml = str_replace(array("\n", "\r", "\t"), '', $xml);
    // $xml = trim(str_replace('"', "'", $xml));
    $simpleXml = simplexml_load_string($xml);
    $json = json_encode($simpleXml);

    if ($mode == 'array') {
        return json_decode($json, true);
    }

    if ($mode == 'object') {
        return json_decode($json);
    }

    return $json;
}

/**
 * Clean a given URL and returns host
 *
 * @param string $url
 *
 * @author Ali Nawaz Qureshi <alinawazbs@gmail.com>
 *
 * @return string
 */
function urlToDomain($url)
{

    if (is_string($url)) {

        $parse = parse_url($url);

        return str_replace('www.', '', $parse['host']);
    }
}

function databaseDate($date)
{
    if (is_string($date)) {
        $d = explode('/', $date);

        return $d[2] . '-' . $d[1] . '-' . $d[0];
    } else {
        return date('Y-m-d');
    }

    return '';
}

function generateExpiryDatePaidOnResult($startDate, $expiryDate)
{
    $tmp = (array) $expiryDate;
    $newDate = '';
    $todate = date("Y-m-d");

    if (empty($tmp)) {
        $start_date = databaseDate($startDate);
        $newDate = date('Y-m-d', strtotime("+3 months", strtotime($todate)));
    } else {
        $newDate = databaseDate($expiryDate);
    }

     return $newDate;
}

function databaseDateForAffiliateFuture($date)
{
    if (is_string($date)) {
        $d = explode('-', $date);
        if ($d[0] != '0000-00-00') {
            return  $d[0] . '-' . $d[1] . '-'   . substr($d[2], 0, 2);
        } else {
            return date('Y-m-d');
        }
    }
}

function generateExpiryDateForPepperJam($startDate, $expiryDate)
{
    $d = explode(' ', $expiryDate);
    if ($d[0] == '0000-00-00') {
        $start_date = databaseDateForAffiliateFuture($startDate);
        $newDate = date('Y-m-d', strtotime("+3 months", strtotime($start_date)));
    } else {
        $newDate = databaseDateForAffiliateFuture($expiryDate);
    }
    return $newDate;
}

function generateExpiryDateForAdmitad($startDate, $expiryDate)
{
    if (empty($expiryDate)) {
        $start_date = databaseDateForAffiliateFuture($startDate);
        $newDate = date('Y-m-d', strtotime("+3 months", strtotime($start_date)));
    } else {
        $newDate = databaseDateForAffiliateFuture($expiryDate);
    }
    return $newDate;
}

function databaseDateBrandrewards($date)
{
    if (is_string($date)) {
        $d = explode(' ', $date);
        if ($d[0] != '0000-00-00') {
            return $d[0];
        } else {
            return date('Y-m-d');
        }
    }
}

function generateExpiryDateBrandRewards($startDate, $expiryDate)
{
    $d = explode(' ', $expiryDate);
    if ($d[0] == '0000-00-00') {
        $start_date = databaseDateBrandrewards($startDate);
        $newDate = date('Y-m-d', strtotime("+3 months", strtotime($start_date)));
    } else {
        $newDate = databaseDateBrandrewards($expiryDate);
    }
    return $newDate;
}

function databaseDateTradeDoubler($date)
{
    if (isset($date) && is_string($date)) {
        $new_date = date('Y-m-d H:i:s', substr($date, 0, -3));
        $d = explode(' ', $new_date);

        return $d[0];
    }
    return '';
}

// Attribute for select drop down
function matchSelected($param1, $param2)
{
    return $param1 == $param2 ? ' selected="selected" ' : '';
}

// Attribute for checkbox and radio button
function matchChecked($param1, $param2)
{
    return $param1 == $param2 ? ' checked="checked" ' : '';
}

// Match selected for DEAL
function matchSelectedDeal($param1)
{
    $selectedDeals = '';
    if($param1 == '' ||
    strtolower(str_replace(' ', '', $param1)) == 'nocodes' ||
    strtolower(str_replace(' ', '', $param1)) == 'n/a' ||
    strtolower(str_replace(' ', '', $param1)) == 'nocodeneeded' ||
    strtolower(str_replace(' ', '', $param1)) == 'notrequired' ||
    strtolower(str_replace(' ', '', $param1)) == 'nocoderequired' ||
    strtolower(str_replace(' ', '', $param1)) == 'nocode') {
        $selectedDeals = "selected='selected'";
    }
    return $selectedDeals !='' ? ' selected="selected" ' : '';
}

//Match selected for voucher Code
function matchSelectedCode($param1)
{
    $selectedCodes = '';
    if($param1 != '' &&
    strtolower(str_replace(' ', '', $param1)) != 'nocodes' &&
    strtolower(str_replace(' ', '', $param1)) != 'n/a' &&
    strtolower(str_replace(' ', '', $param1)) != 'nocodeneeded' &&
    strtolower(str_replace(' ', '', $param1)) != 'notrequired' &&
    strtolower(str_replace(' ', '', $param1)) != 'nocoderequired' &&
    strtolower(str_replace(' ', '', $param1)) != 'nocode') {
        $selectedCodes = "selected='selected'";
    }
    return $selectedCodes !='' ? ' selected="selected" ' : '';
}

//Convert string/domain to Actual Store Name
function stringToStoreName($storeName, $lower = true)
{
    if($lower == false) {
        return preg_replace("/[^a-zA-Z0-9]*( us.*| \(.*|\..*| uk.*| Canada.*| -.*)/i", '', $storeName);
    } else {
        return strtolower(str_replace(' ', '', preg_replace("/[^a-zA-Z0-9]*( us.*| \(.*|\..*| uk.*| ww.*| Canada.*| -.*)/i", '', $storeName)));
    }
}

function arrayToActualStoreName($storesAll)
{
    $regexStoresKeys = str_replace(' ', '', preg_replace("/[^a-zA-Z0-9]*( us.*| \(.*|\..*| uk.*| ww.*| Canada.*| -.*)/i", '', array_keys( $storesAll )));
    $storesAll = array_combine( $regexStoresKeys, array_values( $storesAll ) );
    return array_change_key_case($storesAll);
}

function generateLinkForTradeTracker($url = '')
{
    if ($url) {
        preg_match("/href=\"(.*?)\"/i", $url, $matches);
        $removeHref = str_replace('href=', '',$matches[0]);
        $filteredUrl = str_replace('"', '', $removeHref);
        return $filteredUrl;
    }
    return $url;
}

function getOfferBy($key)
{
    $array[1] = 'Manually';
    $array[2] = 'API';

    return isset($array[$key]) ? $array[$key] : $array;
}

function recordStatus($id = null)
{
    $values = [0 => "All Offers", 1 => "Manual Offers", 2 => "API Offers"];
    return isset($id) && $id >= 1 && $id <= 2 ? $values[$id] : $values;
}

function getRowColor($offerExpiry = null, $offerStatus = null)
{
    $color = '';
    if($offerExpiry != null && $offerExpiry < now()) {
        $color = 'style="background-color: #ed4e2a";';
    } else if ($offerStatus != null && $offerStatus == 1) {
        $color = 'style="background-color: #ffb848";';
    }

    return $color;
}

function getStoreBy($key)
{
    $array[1] = 'Manually';
    $array[2] = 'API';

    return isset($array[$key]) ? $array[$key] : $array;
}

function storeStatus($id = null)
{
    $values = [0 => "All Stores", 1 => "Manual Stores", 2 => "API Stores"];
    return isset($id) && $id >= 1 && $id <= 2 ? $values[$id] : $values;
}

function remove_querystring_var($url, $key) {
    $url = preg_replace('/(.*)(?|&)' . $key . '=[^&]+?(&)(.*)/i', '$1$2$4', $url . '&');
    $url = substr($url, 0, -1);
    return $url;
}

function affiliateNetworksSearch($offers = [], $search)
{
    Schema::table('temp_offers', function (Blueprint $table) {
        $table->dropIfExists();
    });

    Schema::create('temp_offers', function (Blueprint $table) {
        $table->increments('id');
        $table->string('store', 128)->nullable();
        $table->string('offer_type', 128)->nullable();
        $table->string('title', 128)->nullable();
        $table->text('description')->nullable();
        $table->string('voucher_code', 191)->nullable();
        $table->string('discount_label', 24)->nullable();
        $table->date('start_date')->nullable();
        $table->date('expiry_date')->nullable();
        $table->string('affiliate_url', 191)->nullable();
        $table->string('store_name', 128)->nullable();
        $table->string('store_website', 191)->nullable();
        $table->temporary();
    });

    foreach ($offers as $key => $offer) {
        \DB::table('temp_offers')->insert((array) $offer);
    }

    if (isset($search) && !empty($search)) {
        $search = str_replace(' ', '', strtolower(trim(request('search'))));
        $insertedOffers = \DB::table('temp_offers')
            ->select('*')
            ->where('store', 'LIKE', '%' . $search . '%')
            ->get()
            ->toArray();
    } else {
        $insertedOffers = \DB::table('temp_offers')
            ->select('*')
            ->get();
    }
    return $insertedOffers;
}
