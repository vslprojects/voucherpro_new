<?php

/**
 * AssetHelper
 *
 * @author Ghulam Mustafa <ghulam.mustafa@vservices.com>
 * @date   03/12/18
 */

/**
 *  Return's admin assets directory
 *
 * CALLING PROCEDURE
 *
 * In controller call it like this:
 * $adminAssetsDirectory = adminAssetsDir() . $site_settings->site_logo;
 *
 * In View call it like this:
 * {{ asset(adminAssetsDir() . $site_settings->site_logo) }}
 *
 * @param string $role
 *
 * @return bool
 */
function uploadsDir()
{
    return 'uploads/';
}

function uploadsUrl($file = '')
{
    return $file != '' && file_exists('uploads/' . $file) ? url('uploads') . '/' . $file : '';
}

function adminHasAssets($image)
{
    if (!empty($image) && file_exists(uploadsDir() . $image)) {
        return true;
    } else {
        return false;
    }
}

function defaultStoreCoverUrl()
{
	return 'assets/front/images/store-cover.png';
}

function getFileExtension($filename, $offset = 3)
{
    return substr(strtolower($filename), strlen($filename)-$offset, $offset);
}
