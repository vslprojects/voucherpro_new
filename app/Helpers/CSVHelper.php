<?php

/**
 * Parses given file into an array
 *
 * @param string $filename
 * @param string $delimiter
 * @return array|bool
 * @author Ghulam Mustafa Kasmani <gmk1238@gmail.com>
 * @date   31/05/2019
 *
 * CALLING PROCEDURE
 *
 * In Controller call it like this:
 * csvToArray($uploadedfile);
 * csvToArray($uploadedfile, ',');
 * csvToArray($uploadedfile, ';');
 *
 */
function csvToArray($filename = '', $delimiter = ',')
{
    if (!file_exists($filename) || !is_readable($filename)) {
        return false;
    }

    $header = null;
    $data = [];

    if (false !== ($handle = fopen($filename, 'r'))) {
        while (false !== ($row = fgetcsv($handle, 1000, $delimiter))) {
            if (!$header) {
                $header = $row;
            } else {
                $data[] = array_combine($header, $row);
            }
        }
        fclose($handle);
    }

    return $data;
}

/**
 * Creates a CSV file based on given records, columns and fields
 *
 * @author Ghulam Mustafa Kasmani <gmk1238@gmail.com>
 * @date   31/05/2019
 *
 * CALLING PROCEDURE
 *
 * In Controller:
 * $records = $this->userRepository->findByRole($roleId);
 * $columns = ['First Name', 'Last Name', 'Phone #', 'Email Address'];
 * $fields  = ['first_name', 'last_name', 'phone', 'email'];
 * return createCSV($records, $columns, $fields, 'ExportedCompanies.csv');
 *
 * @param array $records
 * @param array $columns
 * @param array $fields
 * @param string $filename
 */
function createCSV($records, array $columns, array $fields, string $filename)
{
    $callback = function () use ($records, $columns, $fields) {

        // Open output stream
        $handle = fopen('php://output', 'w');

        array_unshift($columns, '#');

        // Add CSV headers
        fputcsv($handle, $columns);

        $counter = 1;

        // Get all users
        foreach ($records as $record) {
            $rowFields = [];

            $rowFields[] = $counter;

            foreach ($fields as $field) {
                $rowFields[] = $record->$field;
            }

            // Add a new row with data
            fputcsv($handle, $rowFields);

            $counter++;
        }

        // Close the output stream
        fclose($handle);
    };

    return response()->streamDownload(
        $callback,
        $filename
    );
}

/**
 * Parses given file into an array
 *
 * @param string $filename
 * @param string $delimiter
 * @return array|bool
 * @author Muhammad Shahab Khan <muhammad.shahab@vservices.com>
 * @date   31/05/2019
 *
 * CALLING PROCEDURE
 *
 * In Controller call it like this:
 * csvToArrayWithOutDelimiter($uploadedfile);
 *
 */
function csvToArrayWithOutDelimiter($filename = '', $delimiter = ',')
{
    if (!file_exists($filename) || !is_readable($filename)) {
        return false;
    }

    $header = null;
    $data = [];

    if (false !== ($handle = fopen($filename, 'r'))) {
        while (false !== ($row = fgetcsv($handle))) {
            if (!$header) {
                $header = str_replace(" ", "", $row);
            } else {
                if(!empty($row[0])){
                    $data[] = array_combine($header, $row);
                }
            }
        }
        fclose($handle);
    }

    return $data;
}