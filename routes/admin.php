<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('admin', 'IndexController@index')->name('login'); // for redirection purpose

Route::name('admin.')->middleware('check_ip')->group(
    function () {

        Route::get('/', 'IndexController@index');

        # to show login form
        Route::get('/auth/login', [
            'uses'  => 'Auth\LoginController@showLoginForm',
            'as'    => 'auth.login'
        ]);

        # login form submits to this route
        Route::post('/auth/login', [
            'uses'  => 'Auth\LoginController@login',
            'as'    => 'auth.login'
        ]);

        # logs out admin user
        # it was post method before I recieved MethodNotAllowedHttpException
        Route::any('/auth/logout', [
            'uses'  => 'Auth\LoginController@logout',
            'as'    => 'auth.logout'
        ]);

        # Password reset routes
        Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
        Route::post('/password/reset', 'Auth\ResetPasswordController@reset');
        Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
        Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');

        # shows forgot password form
        /*Route::match(['get', 'post'], '/auth/forgot-password', [
            'uses'  => 'Auth\LoginController@forgotPassword',
            'as'    => 'auth.forgot-password'
        ]);*/

        # shows dashboard
        Route::get('dashboard', [
            'uses'  => 'DashboardController@index',
            'as'    => 'dashboard.index'
        ]);

        # change rating status
        Route::post('ratings/change-rating-status', [
            'uses'  => 'RatingsController@changeRatingStatus',
            'as'    => 'ratings.change-rating-status'
        ]);

        # show expired offers
        Route::get('offers/expired', [
            'uses'  => 'OffersController@expiredOffers',
            'as'    => 'offers.expired'
        ]);

        # bulk delete offers
        Route::post('offers/bulkDelete', [
            'uses'  => 'OffersController@bulkDelete',
            'as'    => 'offers.bulkDelete'
        ]);

        # bulk delete offers
        Route::post('offers/bulkUpdate', [
            'uses'  => 'OffersController@bulkUpdate',
            'as'    => 'offers.bulkUpdate'
        ]);

        # get offers by ajax
        Route::post('offers/ajax-list/{status_id?}', [
            'uses'  => 'OffersController@indexAjax',
            'as'    => 'offers.ajaxList'
        ]);





        # get expired offers list
        Route::post('offers/expired/ajax-list/{status_id?}', [
            'uses'  => 'OffersController@expiredOffersAjax',
            'as'    => 'offers.expired.ajaxList'
        ]);

        # get stores by ajax
        Route::post('stores/ajax-list/{status_id?}', [
            'uses'  => 'StoresController@indexAjax',
            'as'    => 'stores.ajaxList'
        ]);
         Route::get('stores/without-category', [
            'uses'  => 'StoresController@getStoreHasNotCategory',
            'as'    => 'stores.withoutcategory'
        ]);

        Route::get('stores/blocked', [
            'uses'  => 'StoresController@getBlockedStores',
            'as'    => 'stores.blocked'
        ]);
           # get blocked stores by ajax
        Route::post('stores/blocked-ajax-list/{status_id?}', [
            'uses'  => 'StoresController@indexBlockedAjax',
            'as'    => 'stores.blockedAjaxList'
        ]);

        Route::resource('administrators', 'AdministratorsController');
        Route::resource('ip-addresses', 'IpAddressesController');

        Route::post('affiliate-networks/import/{affiliateNetworkName}', [
            'uses' => 'AffiliateNetworksController@importAffiliatedNetworkData',
            'as'   => 'affiliate-networks.import'
        ]);

          Route::get('affiliate-networks/admitad/import/', [
            'uses' => 'AffiliateNetworksController@importAdmitadData',
            'as'   => 'admitad.import'
        ]);

        Route::resource('affiliate-networks', 'AffiliateNetworksController');
        Route::resource('pages', 'PagesController');
        Route::resource('menus', 'MenusController');
        Route::resource('events', 'EventsController');
        Route::resource('sliders', 'SlidersController');
        Route::resource('galleries', 'GalleriesController');
        Route::resource('news', 'NewsController');
        Route::resource('faqs', 'FaqController');
        Route::resource('media-files', 'MediaFilesController');
        Route::resource('banner-types', 'BannerTypeController');
        Route::resource('side-banners', 'SideBannerController');
        Route::resource('banners', 'BannersController');

        Route::resource('categories', 'CategoriesController');

        Route::get('store/export', [
            'uses'  => 'StoresController@downloadExportStoresCSV',
            'as'    => 'stores.export'
        ]);

        Route::resource('stores', 'StoresController');
        //route for offers list on store show page
        Route::get('stores/show/{store_id?}/{status_id?}/', [
            'uses'  => 'StoresController@show',
            'as'    => 'stores.show'
        ]);
        // Route::resource('offers', 'OffersController');
        Route::resource('newsletter-subscribers', 'NewsletterSubscribersController');
        Route::resource('offer-types', 'OfferTypeController');
        Route::resource('voucher-requests', 'VoucherRequestsController');
        Route::resource('ratings', 'RatingsController');
        Route::resource('offers', 'OffersController');
        Route::resource('posts', 'PostsController');

        Route::resource('site-settings', 'SiteSettingsController');

        # To show change password form
        Route::get('/change-password', [
            'uses'  => 'UsersController@changePassword',
            'as'    => 'users.change-password'
        ]);

        # Change password form submits to this route
        Route::post('/change-password', [
            'uses'  => 'UsersController@processChangePassword',
            'as'    => 'users.change-password'
        ]);

        /*
        # Copy data from old system
        Route::get('/old-system-migration', [
            'uses'  => 'OldSystemController@migrateAll',
            'as'    => 'old-system.migrate'
        ]);

        # Copy categories from old system
        Route::get('/old-system-migration-categories', [
            'uses'  => 'OldSystemController@migrateCategories',
            'as'    => 'old-system.migrate.categories'
        ]);

        # Copy stores from old system
        Route::get('/old-system-migration-stores/{store_id?}', [
            'uses'  => 'OldSystemController@migrateStores',
            'as'    => 'old-system.migrate.stores'
        ]);

        # Copy offers from old system
        Route::get('/old-system-migration-offers', [
            'uses'  => 'OldSystemController@migrateOffers',
            'as'    => 'old-system.migrate.offers'
        ]);
        */
    }
);
