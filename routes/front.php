<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

// Auth::routes();

Route::get('/', 'HomeController@index')->name('index');
Route::get('categories', 'HomeController@category')->name('category');
Route::get('all-stores', 'HomeController@store')->name('store');
Route::get('contact', 'HomeController@contact')->name('contact');
Route::get('extension', 'HomeController@extension')->name('extension');
Route::post('contact', 'HomeController@processContact')->name('process-contact');
Route::get('amazon-associate', 'HomeController@amazon')->name('amazon');
Route::get('voucher-request', 'HomeController@voucherRequest')->name('voucher.request');
Route::post('voucher-request', 'HomeController@processVoucherRequest')->name('voucher.request.submit');
Route::get('categories/{slug}', 'HomeController@categoryDetails')->name('category.details');
Route::post('newsletter-subscribre', 'HomeController@newsletterSubscribe')->name('newsletter.subscribe');
Route::get('newsletter-unsubscribre', 'HomeController@newsletterUnSubscribe')->name('newsletter.unsubscribe');
Route::post('newsletter-unsubscribre', 'HomeController@newsletterUnSubscribeSubmit')->name('newsletter.unsubscribeSubmit');
Route::get(
    'newsletter-unsubscribre-verify/{unsub_token}',
    'HomeController@newsletterUnSubscribeVerify'
)
->name('newsletter.unsubscribe.verify');
Route::post('search-stores', 'HomeController@searchStores')->name('search.stores');
Route::get('storesearch', 'HomeController@searchStoresSubmit')->name('search.stores.submit');
Route::post('store-rating', 'HomeController@storeRatingSubmit')->name('store.rating');
Route::post('offer-update-count', 'HomeController@offerUpdateCount')->name('offer.update.count');
Route::get('not-found', 'HomeController@error404')->name('error.404');

/*Route::get('event/{id}', 'HomeController@eventDetails')->name('event.details');
Route::get('gallery', 'HomeController@gallery')->name('gallery');
Route::get('news/{page?}', 'HomeController@news')->name('news')->where('page', '[0-9]+');
Route::get('news/{slug}', 'HomeController@newsDetails')->name('news.details');*/
//Route::get('search', 'HomeController@search')->name('search');

Route::get('home', function () {
    return redirect('/');
});

Route::get('thank-you', function () {
    return view('thankYou');
});

Route::get('run-sitemap', 'HomeController@runSitemap');
Route::get('blogs', 'HomeController@blogs')->name('blogs');
Route::get('blogs/{slug}', 'HomeController@blogDetail')->name('blog-detail');
Route::post('post-comment', 'HomeController@postComment')->name('post-comment');
/*Route::get('/clear-cache', function() {
	Artisan::call('cache:clear');
   	Artisan::call('config:clear');
	Artisan::call('config:cache');
	Artisan::call('view:clear');
	Artisan::call('route:clear');
	return 'Cache cleared!';
});*/

/* This route will always be added at the end of the file due to it's URL pattern */
Route::get('{slug}', 'HomeController@page')->name('page'); // Priority: Store, Event, CMS Page
