<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    |
    */

    'name' => env('APP_NAME', 'Laravel'),

    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |--------------------------------------------------------------------------
    |
    | This value determines the "environment" your application is currently
    | running in. This may determine how you prefer to configure various
    | services the application utilizes. Set this in your ".env" file.
    |
    */

    'env' => env('APP_ENV', 'production'),

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug' => env('APP_DEBUG', false),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */

    'url' => env('APP_URL', 'http://localhost'),

    'asset_url' => env('ASSET_URL', null),

    /*
    |--------------------------------------------------------------------------
    | Api Token
    |--------------------------------------------------------------------------
    |
    | API Token for validation mobile application requests.
    |
    */

    'api_token' => env('API_TOKEN'),

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */

    'timezone' => 'UTC',

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    'locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    'fallback_locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Faker Locale
    |--------------------------------------------------------------------------
    |
    | This locale will be used by the Faker PHP library when generating fake
    | data for your database seeds. For example, this will be used to get
    | localized telephone numbers, street address information and more.
    |
    */

    'faker_locale' => 'en_US',

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    'key'    => env('APP_KEY'),

    'cipher' => 'AES-256-CBC',

    /*
    |--------------------------------------------------------------------------
    | Templates/Themes Configuration
    |--------------------------------------------------------------------------
    |
    | Template name to load on front end is configured here
    | In the initial setup it could be one of followings:
    | template1, template2, template3, template4
    | Default is set to template1, if not configured in .env file then
    | default template "template1" will be loaded on front end website
    |
    */

    'template' => env('TEMPLATE', 'template1'),

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        Illuminate\Bus\BusServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
        Illuminate\Cookie\CookieServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Encryption\EncryptionServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Foundation\Providers\FoundationServiceProvider::class,
        Illuminate\Hashing\HashServiceProvider::class,
        Illuminate\Mail\MailServiceProvider::class,
        Illuminate\Notifications\NotificationServiceProvider::class,
        Illuminate\Pagination\PaginationServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Queue\QueueServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,


        /*
         * Package Service Providers...
         */
        //Appointer\Swaggervel\SwaggervelServiceProvider::class,
        /*
         * Application Service Providers...
         */
        App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
        // App\Providers\BroadcastServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\RouteServiceProvider::class,

        App\Providers\MenuServiceProvider::class,
        App\Providers\ViewComposerServiceProvider::class,
        App\Providers\ModelServiceProvider::class,
        App\Providers\RepositoryServiceProvider::class,

    ],

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases' => [

        'App'          => Illuminate\Support\Facades\App::class,
        'Artisan'      => Illuminate\Support\Facades\Artisan::class,
        'Auth'         => Illuminate\Support\Facades\Auth::class,
        'Blade'        => Illuminate\Support\Facades\Blade::class,
        'Broadcast'    => Illuminate\Support\Facades\Broadcast::class,
        'Bus'          => Illuminate\Support\Facades\Bus::class,
        'Cache'        => Illuminate\Support\Facades\Cache::class,
        'Config'       => Illuminate\Support\Facades\Config::class,
        'Cookie'       => Illuminate\Support\Facades\Cookie::class,
        'Crypt'        => Illuminate\Support\Facades\Crypt::class,
        'DB'           => Illuminate\Support\Facades\DB::class,
        'Eloquent'     => Illuminate\Database\Eloquent\Model::class,
        'Event'        => Illuminate\Support\Facades\Event::class,
        'File'         => Illuminate\Support\Facades\File::class,
        'Gate'         => Illuminate\Support\Facades\Gate::class,
        'Hash'         => Illuminate\Support\Facades\Hash::class,
        'Lang'         => Illuminate\Support\Facades\Lang::class,
        'Log'          => Illuminate\Support\Facades\Log::class,
        'Mail'         => Illuminate\Support\Facades\Mail::class,
        'Notification' => Illuminate\Support\Facades\Notification::class,
        'Password'     => Illuminate\Support\Facades\Password::class,
        'Queue'        => Illuminate\Support\Facades\Queue::class,
        'Redirect'     => Illuminate\Support\Facades\Redirect::class,
        'Redis'        => Illuminate\Support\Facades\Redis::class,
        'Request'      => Illuminate\Support\Facades\Request::class,
        'Response'     => Illuminate\Support\Facades\Response::class,
        'Route'        => Illuminate\Support\Facades\Route::class,
        'Schema'       => Illuminate\Support\Facades\Schema::class,
        'Session'      => Illuminate\Support\Facades\Session::class,
        'Storage'      => Illuminate\Support\Facades\Storage::class,
        'URL'          => Illuminate\Support\Facades\URL::class,
        'Validator'    => Illuminate\Support\Facades\Validator::class,
        'View'         => Illuminate\Support\Facades\View::class,
        'Str'          => Illuminate\Support\Str::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Site short name
    |--------------------------------------------------------------------------
    |
    | This value is the short name of your application. This value is used when
    | the framework needs to place the application's short name in a
    | notification or any other location as required by the application or its
    | packages.
    |
    */
    'site' => env('SITE', '247couponcodes'),

    /*
    |--------------------------------------------------------------------------
    | Affiliate Networks and their API URLs
    |--------------------------------------------------------------------------
    |
    | List of Affiliate Networks and their API URLs.
    |
    */
    'paid_on_results'          => env('PAID_ON_RESULTS'),
    'skim_links'               => env('SKIM_LINKS'),
    'skim_links_client_id'     => env('SKIM_LINKS_CLIENT_ID'),
    'skim_links_client_secret' => env('SKIM_LINKS_CLIENT_SECRET'),
    'skim_links_publisher_id'  => env('SKIM_LINKS_PUBLISHER_ID'),
    'skim_links_user_id'       => env('SKIM_LINKS_USER_ID'),
    'affiliate_future'         => env('AFFILIATE_FUTURE'),
    'brandreward'              => env('BRANDREWARD'),
    'tradedoubler'             => env('TRADEDOUBLER'),
    'pepper_jam_network'       => env('PEPPER_JAM_NETWORK'),
    'pepper_jam_network_advertiser' => env('PEPPER_JAM_NETWORK_ADVERTISER'),
    'admitad'                  => env('ADMITAD'),
    'admitad_client_id'        => env('ADMITAD_CLIENT_ID'),
    'admitad_client_secret'    => env('ADMITAD_CLIENT_SECRET'),
    'admitad_redirect_uri'     => env('ADMITAD_REDIRECT_URI'),
    'admitad_base64_header'    => env('ADMITAD_BASE64_HEADER'),
    'cj_network'               => env('CJ_NETWORK'),
    'cj_network_client_id'     => env('CJ_NETWORK_CLIENT_ID'),
    'cj_network_client_secret' => env('CJ_NETWORK_CLIENT_SECRET'),
    'cj_network_access_token'  => env('CJ_NETWORK_ACCESS_TOKEN'),
    'webgains'                 => env('WEBGAINS'),
    'rekuten'                  => env('REKUTEN'),
    'rekuten_authorization'    => env('REKUTEN_AUTHORIZATION'),
    'rekuten_sid'              => env('REKUTEN_SID'),
    'rekuten_grant_type'       => env('REKUTEN_GRANT_TYPE'),
    'rekuten_username'         => env('REKUTEN_USERNAME'),
    'rekuten_password'         => env('REKUTEN_PASSWORD'),
];
