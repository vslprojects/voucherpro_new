let lastOfferId = ``;

$(document).ready(function () {

  $('.fb-share').click(function () {
    var url = "https://www.facebook.com/sharer/sharer.php?s=100&p[url]=" + $(this).attr('data-value')
      + "&p[title]=" + encodeURIComponent($(this).attr('data-desc')) + "&p[summary]=" + encodeURIComponent($(this).attr
        ('data-desc'))
      + "&p[images][0]=" + encodeURIComponent($(this).attr('data-image'));

    var fbpopup = window.open(url, "sharer", "toolbar=0", "status=0", "width=626", "height=436", "scrollbars=no");
    return false;
  });

  $('.tw-share').click(function () {
    window.open('https://twitter.com/intent/tweet?original_referer=' + $(this).data('value') + '&text=' + $(this).data('desc') + '&tw_p=tweetbutton&url=' + $(this).data('value'), '_blank');

  });

  $('.wa-share').click(function () {
    window.open("https://wa.me/?text=" + $(this).data('desc') + " " + $(this).data('value'), '_blank');

  });

  // remove link of content vglink inserted by ARS for tacking purpose
  // $(".vglnk").removeAttr("href");

});

/* SUCH AUTOCOMPLETE */
//const endpoint = 'https://gist.githubusercontent.com/Miserlou/c5cd8364bf9b2420bb29/raw/2bf258763cdddd704f8ffd3ea9a3e81d25e2c6f6/cities.json';
/*const endpoint = 'http://voucherpro.pk/search-stores';
let dataArray = [];
fetch(endpoint)
    .then(blob => blob.json())
    .then(data => dataArray = data)

function findMatches(wordToMatch, dataArray) {
    return dataArray.filter(place => {
    const regex = new RegExp(wordToMatch, 'gi');
    return place.name.match(regex)
    });
}

function displayMatches() {
const matchArray = findMatches(this.value, dataArray);
const html = matchArray.map(place => {
    if(this.value != '') {
    return `<li><a href="${place.url}">${place.name}</a></li>`
    }
    else {
    return ``
    }
    }).join('');
    suggestions.innerHTML = html;
    $("#searchFieldId .suggestions-wrap").mCustomScrollbar();
}

const searchInput = document.querySelector('#searchBox');
const suggestions = document.querySelector('.suggestions');

searchInput.addEventListener('change', displayMatches);
searchInput.addEventListener('keyup', displayMatches);*/

/* SEARCH FIELD ANIMATION */

/*$(".vProSearch .search-form input").click(function() {
  $(this)
    .parents(".vProSearch")
    .addClass("big");
  $(this)
    .parents(".search-area")
    .addClass("active");
});

$(".vProSearch .search-close").click(function() {
  $(this)
    .parents(".vProSearch")
    .removeClass("big");
  $(this)
    .parents(".search-area")
    .removeClass("active");
  $(this)
    .siblings('#searchBox')
    .val('');
  $(this)
    .siblings('.suggestions-wrap')
    .find('.suggestions')
    .find('li')
    .remove();
});*/

/* CLOSE SEARCHBAR UPON CLICKING ANYWHERE */

/*$(".vProSearch .search-form").click(function(e) {
  e.stopPropagation();
});

$(window).click(function(e) {
  $(".vProSearch")
    .removeClass('big');

  $('.search-area')
    .removeClass('active');

  $(".vProSearch")
    .find('.suggestions-wrap')
    .find('.suggestions')
    .find('li')
    .remove();
});*/

/* SEARCH FIELD ANIMATION */

$(".vProSearch .search-form input").click(function () {
  $(this)
    .parents(".vProSearch")
    .addClass("big");
  $(this)
    .parents(".search-area")
    .addClass("active");
  $(this)
});

$(".vProSearch .search-close").click(function () {
  $(this)
    .parents(".vProSearch")
    .removeClass("big");
  $(this)
    .parents(".search-area")
    .removeClass("active");
  $(this)
    .siblings('#searchBox')
    .val('');
  $(this)
    .siblings('.suggestions-wrap')
    .hide()
});

/* CLOSE SEARCHBAR UPON CLICKING ANYWHERE */

$(".vProSearch .search-form").click(function (e) {
  e.stopPropagation();
});

$(window).click(function (e) {
  $(".vProSearch")
    .removeClass('big');

  $('.search-area')
    .removeClass('active');

  $(".vProSearch")
    .find('.suggestions-wrap')
    .find('.suggestionsList')
    .find('li')
    .remove();

  $('.vProSearch')
    .find('#searchBox')
    .val('');

  $(".vProSearch")
    .find('.suggestions-wrap')
    .hide();
});

$(window).keydown(function (event) {
  if (event.keyCode == 27) {
    $(".vProSearch")
      .removeClass('big');

    $('.search-area')
      .removeClass('active');

    $(".vProSearch")
      .find('.suggestions-wrap')
      .find('.suggestionsList')
      .find('li')
      .remove();

    $('.vProSearch')
      .find('#searchBox')
      .val('');

    $(".vProSearch")
      .find('.suggestions-wrap')
      .hide();

    $('.vProSearch')
      .find('#searchBox')
      .blur();
  }
});

$(document).ready(function () {
  $(".suggestions-wrap").hide();

  $("#searchBox").on("input", function () {
    //$(".suggestions-wrap").show();
    var searchText = $(this).val();
    if (searchText == "") {
      $(".suggestions-wrap").hide();
      $('.morphsearch-content').show();
    } else {
      //$(".suggestions-wrap").show();
      // $(".suggestions-wrap ul").html('');
      //var setPage = $(this).data('search');
      // autoSearchSuggestion(searchText, setPage);
    }
  });
  $('.morphsearch-close').click(function () {
    $('.morphsearch-content').show();
    $(".suggestions-wrap").hide();
  });
  /*$("#searchFieldId").submit(function (event) {
      if (typeof $('#searchFieldId').attr('action') == 'undefined') {
          var actionValue = "storesearch?q=" + $('#searchBox').val();
          window.location = actionValue;
          return false;
      } else {
         var actionValue =  $('#searchFieldId').attr('action');
          window.location = actionValue;
          return false;
      }
  });*/
  $("#searchBox").keydown(function (event) {
    if (event.keyCode == 13) {
      event.preventDefault();
      return false
      //$("#searchBtn").click();
    }
  });

  $('.inner-search #searchBox').click(function (event) {
    $(".suggestions-wrap").show();
  })

  $("#searchBox").keyup(function (event) {
    if (event.keyCode == 13) {
      event.preventDefault();
      return false
      //$("#searchBtn").click();
    }
    var val = $(this).val();

    if (event.keyCode == 38) {
      var listItems = $(".suggestions-wrap ul").children();
      var listLen = listItems.length;
      var hglgItem = -1;
      for (var x = 0; x < listLen; x++) {
        if ($(listItems[x]).hasClass("selected")) {
          hglgItem = x;
          break;
        }
      }
      if (hglgItem == -1) {
        $(listItems[listLen - 1]).addClass("selected");
        // makeActionForSearch(listItems[listLen - 1]);
        $("#searchBox").val($(listItems[listLen - 1]).text());

      } else {
        if (hglgItem == 0) {
          $(listItems[hglgItem]).removeClass("selected");
        } else {
          $(listItems[hglgItem]).removeClass("selected");
          $(listItems[hglgItem - 1]).addClass("selected");
          // makeActionForSearch(listItems[hglgItem - 1]);
          $("#searchBox").val($(listItems[hglgItem - 1]).text());
        }
      }
      return;
    } else if (event.keyCode == 40) {
      var listItems = $(".suggestions-wrap ul").children();
      var listLen = listItems.length;
      var hglgItem = -1;
      for (var x = 0; x < listLen; x++) {
        if ($(listItems[x]).hasClass("selected")) {
          hglgItem = x;
          break;
        }
      }
      if (hglgItem == -1) {
        $(listItems[0]).addClass("selected");
        // makeActionForSearch(listItems[0]);
        $("#searchBox").val($(listItems[0]).text());
      } else {
        if (hglgItem == listLen - 1) {
          $(listItems[hglgItem]).removeClass("selected");
        } else {
          $(listItems[hglgItem]).removeClass("selected");
          $(listItems[hglgItem + 1]).addClass("selected");
          // makeActionForSearch(listItems[hglgItem + 1]);
          $("#searchBox").val($(listItems[hglgItem + 1]).text());
        }
      }
      return;
    } else if (val.length > 1) {
      searchStores(val)
    } else if (val.length == 0) {
      //$('.vProSearch .PopularBrands').show();
    }
  });
});

function searchStores(q) {
  var formData = {
    'q': q
  };
  $.ajax({
    type: "POST",
    url: $('#searchUrl').val(),
    headers: {
      'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content'),
    },
    data: formData,
    success: function (data) {
      searchStoreHtml(data)
    }
  });
}

function searchStoreHtml(data) {
  storeHtml = '';
  catHtml = '';
  if (typeof (data['stores']) != "undefined" && data['stores'].length > 0) {
    var val = data['stores'];
    for (k in val) {
      storeHtml += '<li>';
      storeHtml += '<a href="' + val[k].url + '">';
      storeHtml += '<img src="' + val[k].logoUrl + '" alt="' + val[k].name + '">' + val[k].name + '</a>';
      storeHtml += '</li>';
    }
  } else {
    storeHtml = '';
  }
  $('.suggestions-wrap .store').html(storeHtml);

  if (typeof (data['categories']) != "undefined" && data['categories'].length > 0) {
    var val = data['categories'];
    for (k in val) {
      catHtml += '<li>';
      catHtml += '<a href="' + val[k].url + '">';
      catHtml += '<img src="' + val[k].logoUrl + '" alt="' + val[k].name + '">' + val[k].name + '</a>';
      catHtml += '</li>';
    }

  } else {
    catHtml = '';
  }
  $('.suggestions-wrap .category').html(catHtml);

  if (storeHtml != '' || catHtml != '') {
    $(".suggestions-wrap").show();
    $('.vProSearch .PopularBrands').hide();
  } else {
    $(".suggestions-wrap").hide();
    $('.vProSearch .PopularBrands').show();
  }

  if (storeHtml == '') {
    $('.suggestionsList.store').html('<li><p class="fz-22 fw-600 text-purple mb-0"><i class="far fa-frown-open"></i> No Results Found</p></li>');
  }

  if (catHtml == '') {
    $('.suggestionsList.category').html('<li><p class="fz-22 fw-600 text-purple mb-0"><i class="far fa-frown-open"></i> No Results Found</p></li>');
  }

}

/* MAIN HOME CAROUSEL */

$(".home-carousel").owlCarousel({
  loop: true,
  margin: 10,
  nav: true,
  autoplay: true,
  autoplayTimeout: 5000,
  responsive: {
    0: {
      items: 1
    }
  }
});



/* AMAZON PRODUCTS CAROUSEL */

$(".amazon-product-carousel").owlCarousel({
  loop: true,
  margin: 25,
  nav: true,
  autoplay: true,
  autoplayTimeout: 5000,
  responsive: {
    0: {
      items: 1
    },
    575: {
      items: 2
    },
    768: {
      items: 3
    },
    991: {
      items: 4
    }
  }
});

/* AMAZON MAIN CAROUSEL */

$(".amazon-main-carousel").owlCarousel({
  loop: true,
  margin: 10,
  nav: true,
  autoplay: true,
  autoplayTimeout: 5000,
  responsive: {
    0: {
      items: 1
    }
  }
});

/* FEAUTRED VOUCHER CAROUSEL */

$(document).ready(function () {
  $(".featured-voucher-carousel").slick({
    vertical: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    autoplay: true,
    nextArrow: "<img class='slick-next' src='assets/front/images/next.png'>",
    prevArrow: "<img class='slick-next' src='assets/front/images/prev.png'>",
    responsive: [
      {
        breakpoint: 575,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
});

/* SHOW AND HIDE TEXT COPIED POPOVER */

$('[data-toggle="popover"]').popover();

$(".code-wrap .copy-button").click(function () {
  setTimeout(function () {
    $('[data-toggle="popover"]').popover("hide");
  }, 1000);
});

/* SUBMIT VOUCHER GENERATING BORDER STYLE */

$(".form-fields input, .form-fields textarea")
  .on("focus", function () {
    $(this)
      .parents("span")
      .removeClass("not-focusing")
      .addClass("focusing");
  })
  .on("blur", function () {
    $(this)
      .parents("span")
      .removeClass("focusing")
      .addClass("not-focusing");
  });

/* SUBMIT VOUCHER SWITCH FIELD TOGGLE */

$(".dependency input").on("change", function () {
  if ($(".dependency input#printable_voucher").prop("checked")) {
    $(".dependent").attr({
      placeholder: "Prinatable Voucher",
      //name: "printable_voucher"
    });
  } else if ($(".dependency input#promo_code").prop("checked")) {
    $(".dependent").attr({
      placeholder: "Promo Code",
      //name: "promo_code"
    });
  }
});

/* SUBMIT VOUCHER TOOLTIP */

$('[data-toggle="tooltip"]').tooltip();

/* SITEMAP ALPHABET SLIDER */

$(window).on("load", function () {
  $(".store-categories .stores-list").mCustomScrollbar();
});

/* BLOG CAROUSEL */

$(".blog-carousel").owlCarousel({
  loop: true,
  margin: 10,
  dots: false,
  autoplay: true,
  autoplayTimeout: 5000,
  responsive: {
    0: {
      items: 1
    }
  }
});

/* SETTING THE CONSTANT HEIGHT OF BLOG TITLES */

$(window).on("load resize", function () {
  var blogTitleHeight = $(".blog .posts-wrap .post .post-title")
    .map(function () {
      return $(this).height();
    })
    .get();
  maxHeight = Math.max.apply(null, blogTitleHeight);
  $(".blog .posts-wrap .post .post-title").css("min-height", maxHeight);
});

/* CONTACT FORM CHECKBOX CLASS TOGGLE */

var contactTerms = $('.disclaimer input[type="checkbox"]')
contactTerms.click(function () {
  if ($(this).prop('checked')) {
    $(this).siblings('.box').removeClass('unchecked').addClass('checked')
  }
  else {
    $(this).siblings('.box').removeClass('checked').addClass('unchecked')
  }
})

/* VALIDATE.JS */

$(document).ready(function () {

  $.urlParam = function (name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    return results[1] || 0;
  }

  $.validator.addMethod("letters", function (value, element) {
    return this.optional(element) || value == value.match(/^[a-zA-Z\s]*$/);
  });

  /* newletter validation start */
  var $newsletterForm = $("#newsletterForm");
  $newsletterForm.validate({
    rules: {
      email: {
        required: true,
        email: true
      },
      disclaimer: {
        required: true
      }
    },
    messages: {
      email: "Please specify a valid email address",
      //disclaimer: "Please read the T&C and Privacy Policy"
    },
    submitHandler: function () {
      //$successMsg.show();
    }
  });
  /* newletter subscription validation end */

  /* newletter subscription submission start */
  $newsletterForm.submit(function (e) {
    e.preventDefault();
    if (!$(this).valid())
      return false;

    var formData = $(this).serialize();
    $.ajax({
      type: "POST",
      url: $(this).attr("action"),
      headers: {
        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content'),
      },
      data: formData,
      success: function (data) {
        if (data.error) {
          showAlert('danger', data.message);
        } else {
          $newsletterForm[0].reset();
          $('.disclaimer').find('.box').removeClass('checked');
          showAlert('success', data.message);
        }
      }
    });
  });
  /* newletter subscription submission end */

  /* newsletter unsubscribe validation start */
  var $newsletterUnSub = $("#newsletterUnSub");
  $newsletterUnSub.validate({
    rules: {
      email: {
        required: true,
        email: true
      }
    },
    messages: {
      email: "Please specify a valid email address"
    }
  });
  /* newletter unsubscribe validation end */

  /* contact us form validation start */
  var $contactForm = $("#contactForm");
  $contactForm.validate({
    rules: {
      name: {
        required: true,
        minlength: 3,
        letters: true
      },
      email: {
        required: true,
        email: true
      },
      description: {
        required: true
      },
      terms: {
        required: true
      }
    },
    messages: {
      name: "Please specify your name (only letters and spaces)",
      email: "Please specify a valid email address",
      description: "Don't hesitate to write to us",
      terms: "Please read and confirm the Terms & Conditions"
    },
    submitHandler: function () {
      //$successMsg.show();
    }
  });
  /* contact us form validation end */

  /* contact us form submission start */
  $contactForm.submit(function (e) {
    e.preventDefault();
    if (!$(this).valid())
      return false;

    var formData = $(this).serialize();
    $contactForm[0].reset();
    $.ajax({
      type: "POST",
      url: $(this).attr("action"),
      headers: {
        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content'),
      },
      data: formData,
      success: function (data) {
        if (data.error) {
          showAlert('danger', data.message);
        } else {
          //$contactForm[0].reset();
          $('.disclaimer').find('.box').removeClass('checked');
          $('.disclaimer').find('.box').addClass('unchecked');
          showAlert('success', data.message);
        }
      }
    });

  });
  /* contact us form submission end */

  /* footer contact us form validation start */
  var $footerContactForm = $("#footerContactForm");

  $footerContactForm.validate({
    rules: {
      name: {
        required: true,
        minlength: 3,
        letters: true
      },
      email: {
        required: true,
        email: true
      },
      description: {
        required: true
      },
      terms: {
        required: true
      }
    },
    messages: {
      name: "Please specify your name (only letters and spaces)",
      email: "Please specify a valid email address",
      description: "Don't hesitate to write to us",
      terms: "Please read and confirm the Terms & Conditions"
    },
    submitHandler: function (e) {
      footerContactForm(e);
      //$successMsg.show();
    }
  });
  /* footer contact us form validation end */

  /* contact us form submission start */
  /*$("#footerContactForm").submit(function(e){
    alert('hello worlds');
    e.preventDefault();

    if(!$(this).valid())
      return false;

    var formData = $(this).serialize();
    $.ajax({
        type: "POST",
        url: $('#footerContactFormAction').val(),
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content'),
        },
        data: formData,
        success: function( data ) {
          if(data.error) {
            showAlert('danger',data.message);
          } else {
            $footerContactForm[0].reset();
            $('.disclaimer').find('.box').removeClass('checked');
            $('.disclaimer').find('.box').addClass('unchecked');
            showAlert('success',data.message);
          }
        }
    });

    return false;
  });*/
  /* contact us form submission end */

  /* Submit voucher validation */
  var $submitVoucherForm = $("#submitVoucherForm");
  $submitVoucherForm.validate({
    rules: {
      name: {
        required: true,
        minlength: 6,
        letters: true
      },
      email: {
        required: true,
        email: true
      },
      store: {
        required: true
      },
      description: {
        required: true
      },
      voucher_type: {
        required: true,
        letters: true
      },
      voucher_type: {
        required: true,
        email: true
      },
      voucher: {
        required: true
      },
      expire_date: {
        required: true
      },
      terms: {
        required: true
      }
    },
    messages: {
      name: "Please specify your name (only letters and spaces)",
      email: "Please specify a valid email address",
      store: "Please mention store name",
      description: "Please specify voucher description",
      voucher_type: "Please specify your name (only letters and spaces)",
      voucher: "Please specify code",
      expire_date: "Please Enter Valid Value And Greater then Today Date",
      terms: "Please read the T&C and Privacy Policy"
    },
    submitHandler: function () {
      //$successMsg.show();
    }
  });

  /* contact us form submission start */
  $submitVoucherForm.submit(function (e) {
    e.preventDefault();
    if (!$(this).valid())
      return false;

    var formData = $(this).serialize();
    $submitVoucherForm[0].reset();
    $.ajax({
      type: "POST",
      url: $(this).attr("action"),
      headers: {
        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content'),
      },
      data: formData,
      success: function (data) {
        if (data.error) {
          showAlert('danger', data.message);
        } else {
          $('.disclaimer').find('.box').removeClass('checked');
          $('.disclaimer').find('.box').addClass('unchecked');
          showAlert('success', data.message);
        }
      }
    });
  });
  /* contact us form submission end */

  /* All stores page scripts */
  $('.storeSingle').click(function () {
    var val = $(this).data('value');
    $('.storeSingleList').hide();
    $('.stores-' + val).show();
  });

  $('.storeAll').click(function () {
    $('.storeSingleList').show();
  });

  /* COPY TEXT SNIPPET */
  $('.show-code').on('shown.bs.modal', function (e) {

    $(".code-wrap button").click(function () {
      var voucherCode = $(this).attr("data-code");
      var $temp = $("<input>");
      $("#codeModal").append($temp);
      $temp.val(voucherCode).select();
      document.execCommand("copy");
      $temp.remove();
    });
    updateOfferCount($.urlParam('copy'))
    //modalShowCode(offerArray[lastOfferId]);

  })

  $('.show-deal').on('shown.bs.modal', function (e) {
    //modalShowDeal(offerArray[lastOfferId]);
    updateOfferCount($.urlParam('shopnow'))
  })

  $('.coupon-button').click(function () {
    url = $(this).data('url');
    window.location = url;
  });

  $('.give-star-rating input[type="radio"]').on('click', function () {
    var formData = {
      'store_id': $('#storeId').val(),
      'rating': $(this).val()
    };

    $.ajax({
      type: "POST",
      url: $('#ratingUrl').val(),
      headers: {
        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content'),
      },
      data: formData,
      success: function (data) {

      }
    });
  })

});

/* To show success messages */
function showAlert($type, $msg) {
  $sel = $('.alert-' + $type);
  $sel.find('p').text($msg);
  $sel.addClass('alert-show');
  setTimeout(function () {
    $sel.removeClass("alert-show");
  }, 8000);
}

function hideAlert() {
  setTimeout(function () {
    $('.alert').removeClass("alert-show");
  }, 8000);
}

/* Modal popups */
function modalShowCode(data) {
  $sel = $('#codeModal');
  $sel.find('.store-image img').attr('src', data.store_logo);
  $sel.find('.about-deal').text(data.title);
  $sel.find('.code').text(data.code);
  $sel.find('.code-wrap button').attr('data-code', data.code);
  $sel.find('.expire_date').text(data.expire_date);
}

function modalShowDeal(data) {
  $sel = $('#dealModal');
  $sel.find('.store-image img').attr('src', data.store_logo);
  $sel.find('.about-deal').text(data.title);
  $sel.find('.store-name').text(data.store_name);
  $sel.find('.expire_date').text(data.expire_date);
}

function updateOfferCount(id) {

  if (id != 0) {
    var formData = {
      'id': id
    };

    $.ajax({
      type: "POST",
      url: offerCountUrl,
      headers: {
        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content'),
      },
      data: formData,
      success: function (data) {

      }
    });
  }
}

/* PRELOADER */

$(window).on('load', function () {
  $('.preloader').hide();
  $('.main-wrapper').show();
});

/* Footer Contact Form Submit */
function footerContactForm($this) {

  var formData = $($this).serialize();
  $("#footerContactForm")[0].reset();

  $.ajax({
    type: "POST",
    url: $this.action,
    headers: {
      'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content'),
    },
    data: formData,
    success: function (data) {
      if (data.error) {
        showAlert('danger', data.message);
      } else {
        //$footerContactForm[0].reset();
        $('.disclaimer').find('.box').removeClass('checked');
        $('.disclaimer').find('.box').addClass('unchecked');
        showAlert('success', data.message);
      }
    }
  });

  return false;
}

$(document).ready(function () {
  $(".description-card").append('<a href="javascript:void(0)" class="descr-toggler">Read more</a>');
  $('.description-card').each(function () {
    // if there are more than just one P    
    if ($(this).children('p').length > 1) {
      // wrap them in a container & also have class .hidden (hides content)
      $('p:not(:first-child)').wrapAll('<div class="descr-extended d-none"></div>');
    }
  });
  $('.descr-toggler').on('click', function () {
    $('.descr-extended').toggleClass('d-block');  // toggling visibility w .hidden
  });


  $('.descr-toggler').on('click', function () {
    $(this).toggle(function () {
      $(".description-card").append('<a href="javascript:void(0)" class="descr-toggler">Show Less</a>');
    }, function () {
      $(this).text("Read more").stop();
    })
  });
});