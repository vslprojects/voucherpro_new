@extends('front.layouts.app')

@include('front.partials._head')

@section('css')
    <!-- <link rel="stylesheet" href="{!! asset('assets/front/css/trending.css') !!}"> -->
@endsection

@section('content')

@include('front.partials._innerSearch')

    <section class="category-listing">
        <div class="container">
            @include('front.partials._sideBanners')
            @if($record)
            <div class="row">
                <div class="col-12 mb-20">
                    <h1 class="mb-10 text-center text-lg-left fw-600 mb-0"><img src="{!! asset(uploadsDir(). $record[0]['image_large']) !!}" alt="{!! $record[0]['name'] !!}"><span style="color: {{ $record[0]['color'] }}">{!! $record[0]['name'] !!}</span></h1>
                </div>
                <div class="col-12">
                    <div class="row">

                        @foreach($record[0]['stores'] as $store)
                        <div class="col-12 col-sm-6 col-xl-3">
                            <div class="company-card">
                                <div class="banner"><img src="{!! ($store->cover) ? asset(uploadsDir(). $store->cover) : asset(defaultStoreCoverUrl()) !!}" class="img-fluid" alt="{!! $store->name !!}"></div>
                                <div class="about-offer">
                                    <div class="brand-logo"><img src="{!! asset(uploadsDir(). $store->logo) !!}" width="81" height="72" alt="{!! $store->name !!}"></div>
                                    <h4 class="store-name">{!! $store->name !!}</h4>
                                    <p class="description">{!! Str::words(strip_tags(html_entity_decode($store->description, ENT_QUOTES, 'UTF-8')), 10) !!}</p>
                                    <div class="meta">
                                        <div class="separator"></div>
                                        <a href="{!! route('page', trim($store->website)) !!}" class="v-btn yellow p-2 mx-auto">Visit Store</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                 @if(count($record[0]['stores']) > 0)
                <span style="margin: 0 auto;">{{ $record[0]['stores']->links() }}</span>
                @endif
            </div>
            @endif
        </div>
    </section>

@include('front.partials._popularCategories')
@endsection

@section('js')
@endsection
