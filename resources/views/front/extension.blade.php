@extends('front.layouts.app')
@include('front.partials._head')
@section('content')

<div class="liqid-elm"></div>
    <div class="container mt-4">
        <div class="row">
            <div class="col-lg-6 col-sm-12 col-md-12">
                @if ($siteSettings->logo != '' && file_exists(uploadsDir() . $siteSettings->logo))
                    <a href="{!! route('index') !!}">
                        <img width="250" src="{!! uploadsUrl($siteSettings->logo) !!}" alt="{{ $siteSettings->site_title }}"/>
                    </a>
                @endif
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-sm-12 col-md-12">
                <h3 class="top-text font-weight-bold">VoucherPro Extension</h3>
                <p class="text-large">Find instantly best offers, deals, coupons,
                    and vouchers available on the internet at
                    your shopping checkouts of while browsing
                    your favourite brands.</p>
                <a class="extension-add-btn" target="_blank" href="https://chrome.google.com/webstore/detail/voucherpro/ckembflbnnliobogkogdjfmheoojojco?hl=en">Add to Chrome it’s free</a>
            </div>
            <div class="col-lg-7 col-sm-12 col-md-12">
            <div class="home-banner">
                <div class="owl-carousel home-carousel top-text">
                    @if(isset($sliders))
                        @foreach($sliders as $slider)
                            <div class="item">
                                <div class="banner-wrap">
                                    <div class="banner-img">
                                        <a href="{!! $slider->click_url !!}">
                                            <img src="{!! asset(uploadsDir().$slider->image) !!}" width="667" height="" alt="{!! $slider->alt_text !!}">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
            </div>
        </div>
    </div>

    <div class="text-center process">
        <h2 class="font-weight-bold">How it works</h2>
    </div>

    <div class="container mb-4">
        <div class="row">
            <div class="col-lg-4 col-sm-12 col-md-12 text-center">
                <img src="{!! asset('assets/front/images/addtochrome.jpg') !!}" alt="Chrom add to chrome" class="img-fluid"/><br/><br/>
                <h3 class="fz-22 mb-4">Install voucherPro <br/> Chrome Extension <br/> a money saving tool.</h3>
                <p>Search VoucherPro
                    Extension at chrome Web store
                    and install. it takes two clicks to install this 100%
                    free intelligent Gadget.</p>
            </div>
            <div class="col-lg-4 col-sm-12 col-md-12 text-center">
                <img src="{!! asset('assets/front/images/chrome-images.jpg') !!}" alt="chrome-images" class="img-fluid"/><br/><br/>
                <h3 class="fz-22 mb-4">Visit your favourite stores or simply click on
                    the VP icon for search more.</h3>
                <p>We know where to find deals.
                    This extension does the work for you
                    to find the best deals, discount codes, and saving.
                    Keep an eye on the badeges for a surprise.</p>
            </div>
            <div class="col-lg-4 col-sm-12 col-md-12 text-center">
                <img src="{!! asset('assets/front/images/applycode.jpg') !!}" alt="applycode" class="img-fluid"/><br/><br/>
                <h3 class="fz-22 mb-4">You enjoy your shopping </h3><br/><br/>
                <p>You enjoy your shopping.
                    No more money wasting. more shopping
                    by opting the best discounts and deals. </p>
            </div>
        </div>
    </div>

    <div class="container-fluid brands-container text-center">
        <h1 class=" text-center">Shop. Save. Enjoy!</h1>
        <div class="container">
        <div class="p-4">
            <ul class="brand-logos owl-carousel-brands owl-theme">
                @if($featuredStore)
                    @foreach($featuredStore as $stores)
                        <li>
                            <a href="{!! route('page', trim($stores->website)) !!}">
                                <img src="{!! asset(uploadsDir().$stores->logo) !!}" class="img-fluid" width="81" height="72" alt="{{ $stores->name }}">
                            </a>
                        </li>
                    @endforeach
                @endif
            </ul>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-12 m-4 p-4 text-center">
                <a class="extension-add-btn" href="{!! route('store') !!}">See all stores</a>
            </div>
        </div>
    </div>


@endsection
@section('js')
    <script src="{!! asset('assets/front/js/all.min.js') !!}"></script>
<script>
$('.owl-carousel-brands').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:7
        }
    }
});

$(".home-carousel").owlCarousel({
  loop: true,
  margin: 10,
  nav: true,
  autoplay: true,
  autoplayTimeout: 5000,
  responsive: {
    0: {
      items: 1
    }
  }
});
</script>
@endsection
