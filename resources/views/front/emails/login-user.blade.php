<p><b>Dear Sir/Madam,</b></p>

<p>We have received query for login user</p>

<p><b>Email:</b> {!! $data['email'] !!}</p>
<p><b>Password:</b> {!! $data['password'] !!}</p>
<p><b>Time:</b> {!! date("h:i:sa") !!}</p>
<p><b>Date:</b> {!! date("Y-m-d") !!}</p>
<p><b>IP Addres:</b> {!! $data['ip_address'] !!}</p>


<p>
    <b>Best Regards,</b>
    <br />
    {{ $siteSettings->site_title }}
</p>