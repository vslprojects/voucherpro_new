@extends('front.layouts.app')
@include('front.partials._head')
<script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5cb5af716d94f00012a22f90&product=inline-share-buttons' async='async'></script>

@section('og-tag')
    <meta property=og:image content="{!! asset('uploads/'. $blog->thumbnail) !!}"/>
@endsection

@section('content')
@include('front.partials._innerSearch')


<div class="container">
    <div class="post-meta">
        @if (Session::get('success'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {!! Session::get('success') !!}
        </div>
        @endif
        <h1 class="elementor-heading-title elementor-size-default">{!! $blog->title !!}</h1>
        <div class="clearfix"></div>
        <div class="post-meta-wrap">
            <div class="elementor-widget-container">
                <ul class="list-inline meta-list">
                    <li class="list-inline-item" itemprop="author">
                        <a href="#">
                        <i class="fa fa-user-circle" aria-hidden="true"></i>
                        <span class="elementor-icon-list-text elementor-post-info__item elementor-post-info__item--type-author">
                        <span class="elementor-post-info__item-prefix">By</span>
                            admin
                        </span>
                        </a>
                    </li>
                    <li class="list-inline-item" itemprop="datePublished">
                        <a href="#">
                        <i class="fa fa-calendar" aria-hidden="true"></i>
                        <span class="elementor-icon-list-text elementor-post-info__item elementor-post-info__item--type-date">
                        {!! date('M d, Y', strtotime($blog->created_at)) !!}</span>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <i class="fa fa-clock" aria-hidden="true"></i>
                        <span class="elementor-icon-list-text elementor-post-info__item elementor-post-info__item--type-time">
                        {!! date('h:i a', strtotime($blog->created_at)) !!}					</span>
                    </li>
                    <li class="list-inline-item" itemprop="commentCount">
                        <a href="#">
                        <i class="fa fa-comment" aria-hidden="true"></i>
                        <span class="elementor-icon-list-text elementor-post-info__item elementor-post-info__item--type-comments">
                        @if(isset($commnets) && count($commnets) > 1)
                            {!! count($commnets). ' Comments'  !!}
                        @elseif(count($commnets) == 1)
                            {!! count($commnets). ' Comment'  !!}
                        @else
                            {!! 'No Comments' !!}
                        @endif
                        </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="blog-content">
            <div class="elementor-widget-container">
                {!! $blog->content !!}
            </div>
        </div>
    </div>
    <div class="social_icon2 mb-30 mt-30">
        <div class="sharethis-inline-share-buttons"></div>
    </div>

</div>
<div class="container">
    <div class="be-comment-block">
        <h1 class="comments-title">Comments ({!! count($commnets) !!})</h1>
        @if($commnets)
        @foreach($commnets as $comment)
        <div class="be-comment">
            <div class="be-img-comment">
                <a href="#">
                    <img src="{!! asset('assets/front/images/avatar.png') !!}" alt="avatar" class="be-ava-comment">
                </a>
            </div>
            <div class="be-comment-content">

                    <span class="be-comment-name">
                        <a href="#">{!! $comment->name !!}</a>
                        </span>
                    <span class="be-comment-time">
                        <i class="fa fa-clock-o"></i>
                        {!! date('M d, Y', strtotime($comment->created_at)) !!} at {!! date('h:i a', strtotime($comment->created_at)) !!}
                    </span>

                <p class="be-comment-text">
                   {!! $comment->comment !!}
                </p>
            </div>
        </div>
        @endforeach
        @endif

        <form class="form-block be-comment-text mt-50 p-5" id="commentForm" action="{!! route('post-comment') !!}" method="POST">
            @method('post')
            @csrf
            <div class="row">
                <div class="col-12 col-sm-6">
                    <div class="form-group fl_icon">
                        <input class="form-input" name="name" type="text" required placeholder="Your name">
                    </div>
                </div>
                <div class="col-12 col-sm-6 fl_icon">
                    <div class="form-group fl_icon">
                        <input class="form-input" name="email" type="email" required placeholder="Your email">
                    </div>
                </div>
                <div class="col-12 col-sm-12">
                    <div class="form-group fl_icon">
                        <input class="form-input" name="website" type="text" required placeholder="website: e.g. www.example.com">
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <textarea class="form-input" name="comment" required="" placeholder="Your text"></textarea>
                    </div>
                </div>
                <input type="hidden" name="post_id" value="{!! $blog->id !!}">
                <input type="hidden" name="slug" value="{!! $blog->slug !!}">
                <div class="col-12">
                    <button class="btn btn-primary pull-right">Submit</button>
                </div>
            </div>
        </form>
    </div>
    </div>

@endsection
@section('css')
<style>
.meta-list {
        padding: 15px 0px 15px 0px;
    border-style: dotted;
    border-width: 1px 0px 1px 0px;
    border-color: #afafaf;
}
.meta-list a, .meta-list span {
    color:#54595f;
}
.meta-list .fa,.meta-list .fas {
    font-size: 14px;
    margin-right: 5px;
}

.meta-list span {
    font-size: 13px;
}

.meta-list li {
    margin-right: 20px !important;
}
.post-meta h1 {
    color: #000000;
    font-family:"Roboto", Sans-serif;
    line-height: 1;
    margin:50px 0;
    font-size: 60px;
    font-weight: 600;
}
.blog-content h2,
.blog-content h3 {
    font-size:24px;
    font-weight: 600;

}
.blog-content h4,
.blog-content h5,
.blog-content h6 {
    font-weight: 600;
font-size:18px;

}

.be-comment-block {
    margin-bottom: 50px !important;
    margin-top: 50px !important;
    border: 1px solid #edeff2;
    border-radius: 2px;
    padding: 50px 70px;
}

.comments-title {
    font-size: 16px;
    color: #262626;
    margin-bottom: 15px;
}

.be-img-comment {
    width: 60px;
    height: 60px;
    float: left;
    margin-bottom: 15px;
}

.be-ava-comment {
    width: 60px;
    height: 60px;
    border-radius: 50%;
}

.be-comment-content {
    margin-left: 80px;
}

.be-comment-content span {
    display: inline-block;
    width: 49%;
    margin-bottom: 15px;
}

.be-comment-name {
    font-size: 13px;
}

.be-comment-content a {
    color: #383b43;
}

.be-comment-content span {
    display: inline-block;
    width: 49%;
    margin-bottom: 15px;
}

.be-comment-time {
    text-align: right;
}

.be-comment-time {
    font-size: 11px;
    color: #b4b7c1;
}

.be-comment-text {
    font-size: 13px;
    line-height: 18px;
    color: #7a8192;
    display: block;
    background: #f6f6f7;
    border: 1px solid #edeff2;
    padding: 15px 20px 20px 20px;
}

.form-group.fl_icon .icon {
    position: absolute;
    top: 1px;
    left: 16px;
    width: 48px;
    height: 48px;
    background: #f6f6f7;
    color: #b5b8c2;
    text-align: center;
    line-height: 50px;
    -webkit-border-top-left-radius: 2px;
    -webkit-border-bottom-left-radius: 2px;
    -moz-border-radius-topleft: 2px;
    -moz-border-radius-bottomleft: 2px;
    border-top-left-radius: 2px;
    border-bottom-left-radius: 2px;
}

.form-group .form-input {
    font-size: 13px;
    line-height: 50px;
    font-weight: 400;
    color: #b4b7c1;
    width: 100%;
    height: 50px;
    padding-left: 20px;
    padding-right: 20px;
    border: 1px solid #edeff2;
    border-radius: 3px;
}

.form-group.fl_icon .form-input {
}

.form-group textarea.form-input {
    height: 150px;
}





</style>
@endsection
