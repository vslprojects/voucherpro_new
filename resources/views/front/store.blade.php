@extends('front.layouts.app')

@include('front.partials._head')

@section('css')
    <!-- <link rel="stylesheet" href="{!! asset('assets/front/css/sitemap.css') !!}"> -->
@endsection

@section('content')

@include('front.partials._innerSearch')

<section class="sitemap">
    <div class="container">
        @include('front.partials._sideBanners')
        
        <div class="row">
            <div class="col-12">
                <div class="heading py-0">
                    <h2>All Stores</h2>
                    <p>Find your store and grab a voucher</p>
                </div>

                <ul class="sort-filter-wrap">
                    <li><a href="javascript:void(0);" class="storeAll">All</a></li>
                    @for($i=65; $i<=90; $i++)
                        <li><a href="javascript:void(0);" class="storeSingle" data-value="{{ chr($i) }}">{{ chr($i) }}</a></li>
                    @endfor
                    <li><a href="javascript:void(0);" class="storeSingle" data-value="0-9">0 - 9</a></li>
                </ul>

                <div class="store-categories">
                    @foreach($stores as $key => $records)
                    <div class="category-wrap storeSingleList stores-{{ $key }}">
                        <div class="alphabet">
                            <a href="javascript:void(0);" class="alphabet storeSingle" data-value="{{ $key }}">{{ $key }}</a>
                        </div>
                        <div class="stores-list">
                            <ul class="list-unstyled">
                                @foreach($records as $store)
                                    <li><a href="{{ route('page', trim($store['website'])) }}">{{ $store['name'] }} ({{ $store['total_offers'] }})</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>

@include('front.partials._popularCategories')
@endsection