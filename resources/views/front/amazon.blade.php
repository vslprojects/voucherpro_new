@extends('front.layouts.app')

@include('front.partials._head')

@section('css')
    <!-- <link rel="stylesheet" href="{!! asset('assets/front/css/amazon.css') !!}"> -->
@endsection

@section('content')
<section class="amazon-associates">
    <section class="amazon-search">
        <div class="container">
            <div class="row">
                <div class="col-10 col-md-8 mx-md-auto">
                    <div class="top">
                        <h1>Search for Any Amazon Products</h1>
                        <img src="{!! asset('assets/front/images/amazon-big.png') !!}" class="img-fluid" alt="Amazon Associate">
                    </div>
                    <div class="bottom">
                        <form>
                            <div class="form-row">
                                <div class="col-12 col-lg-3 mb-20">
                                    <select id="category" name="category" class="form-control">
                                        <option value="All">All Departments</option>
                                        <option value="Apparel">Clothing &amp; Accessories</option>
                                        <option value="Appliances">Appliances</option>
                                        <option value="Automotive">Car &amp; Motorbike</option>
                                        <option value="DVD">Movies &amp; TV Shows</option>
                                        <option value="Electronics">Electronics</option>
                                        <option value="Furniture">Furniture</option>
                                        <option value="GiftCards">Gift Cards</option>
                                        <option value="Beauty">Beauty</option>
                                        <option value="Books">Books</option>
                                        <option value="Grocery">Gourmet &amp; Specialty Foods</option>
                                        <option value="HealthPersonalCare">Health &amp; Personal Care</option>
                                        <option value="Industrial">Industrial &amp; Scientific</option>
                                        <option value="Software">Software</option>
                                        <option value="Shoes">Shoes &amp; Handbags</option>
                                        <option value="Jewelry">Jewellery</option>
                                        <option value="KindleStore">Kindle Store</option>
                                        <option value="LawnAndGarden">Lawn &amp; Garden</option>
                                        <option value="Luggage">Luggage &amp; Bags</option>
                                        <option value="LuxuryBeauty">Luxury Beauty</option>
                                        <option value="MusicalInstruments">Musical Instruments</option>
                                        <option value="PetSupplies">Pet Supplies</option>
                                        <option value="PCHardware">Computers &amp; Accessories</option>
                                        <option value="Pantry">Amazon Pantry</option>
                                        <option value="SportingGoods">Sports, Fitness &amp; Outdoors</option>
                                        <option value="Music">Music</option>
                                        <option value="Office Products">Office Products</option>
                                        <option value="Toys">Toys &amp; Games</option>
                                        <option value="VideoGames">Video Games</option>
                                        <option value="Watches">Watches</option>
                                    </select>
                                </div>
                                <div class="col-12 col-lg-9 mb-20">
                                    <input type="text" class="form-control" id="search" name="search" placeholder="Keywords, ASIN or ISBN">
                                    <button class="v-btn yellow">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <div class="container">
        <div class="owl-carousel amazon-main-carousel">
            <div class="item">
                <div class="product-wrap">
                    <a href="javascript:void(0);">
                        <img src="{!! asset('assets/front/images/amazon/banner-1.jpg') !!}" class="img-fluid" alt="Product">
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="product-wrap">
                    <a href="javascript:void(0);">
                        <img src="{!! asset('assets/front/images/amazon/banner-2.jpg') !!}" class="img-fluid" alt="Product">
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="product-wrap">
                    <a href="javascript:void(0);">
                        <img src="{!! asset('assets/front/images/amazon/banner-3.jpg') !!}" class="img-fluid" alt="Product">
                    </a>
                </div>
            </div>
        </div>

        <section class="best-selling">
            <h2 class="section-heading"><span>Best Selling</span> Amazon Products</h2>
            <div class="row">
                <div class="col-12">
                    <div class="owl-carousel amazon-product-carousel">
                        <div class="item">
                            <div class="product-wrap">
                                <a href="javascript:void(0);">
                                    <img src="{!! asset('assets/front/images/products/1.jpg') !!}" class="img-fluid" alt="Product">
                                    <p class="product-description">Finish Dishwasher Tablets, All in 1 Max Lemon, 90-Count</p>
                                    <p class="price"><span class="old">£25.00</span> £12.41</p>
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-wrap">
                                <a href="javascript:void(0);">
                                    <img src="{!! asset('assets/front/images/products/2.jpg') !!}" class="img-fluid" alt="Product">
                                    <p class="product-description">Finish Dishwasher Tablets, All in 1 Max Lemon, 90-Count</p>
                                    <p class="price"><span class="old">£25.00</span> £12.41</p>
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-wrap">
                                <a href="javascript:void(0);">
                                    <img src="{!! asset('assets/front/images/products/3.jpg') !!}" class="img-fluid" alt="Product">
                                    <p class="product-description">Finish Dishwasher Tablets, All in 1 Max Lemon, 90-Count</p>
                                    <p class="price"><span class="old">£25.00</span> £12.41</p>
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-wrap">
                                <a href="javascript:void(0);">
                                    <img src="{!! asset('assets/front/images/products/4.jpg') !!}" class="img-fluid" alt="Product">
                                    <p class="product-description">Finish Dishwasher Tablets, All in 1 Max Lemon, 90-Count</p>
                                    <p class="price"><span class="old">£25.00</span> £12.41</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</section>

@include('front.partials._popularCategories')
@endsection