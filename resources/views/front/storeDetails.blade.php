@extends('front.layouts.app')

@include('front.partials._head')

@section('css')
<!-- <link rel="stylesheet" href="{!! asset('assets/front/css/store.css') !!}"> -->
@endsection

@section('og-tag')
    <meta property=og:image content="{!! asset('uploads/'. $storePage[0]->logo) !!}"/>
@endsection

@section('content')

@include('front.partials._innerSearch')

<section class="store">
    <section class="container">
        @include('front.partials._sideBanners')
        <section class="row">
            <section class="col-12">
                <div class="store-overview">
                    <div class="left">
                        <img src="{!! asset('uploads/'. $storePage[0]->logo) !!}"
                            alt="{!! $storePage[0]->name !!} Discount Code">
                        <div class="content">

                            @php
                            $promoWord = 'Promo';

                            if (config('app.site') == 'voucherpro') {
                            $promoWord = 'Voucher';
                            }
                            @endphp
                            <h1>{!! $storePage[0]->name !!} {!! (config('app.site') == 'voucherpro') ? 'Discount Code' : 'Coupon Code' !!} & {!! $promoWord !!} {!! date('F Y') !!}</h1>
                            <input type="hidden" id="storeId" value="{{ $storePage[0]->id }}">
                            <input type="hidden" id="ratingUrl" value="{!! route('store.rating') !!}">
                            <fieldset class="give-star-rating">
                                @for($i=10; $i>0; $i--)
                                @php
                                $ratingVal = ($i/2);
                                $userrating = 5;
                                $ratingCheck = ( $userrating == $ratingVal) ?
                                'checked' : '';
                                @endphp

                                @if($i % 2 == 0)
                                <input type="radio" id="rating{{ $i }}" name="rating" value="{{ ($ratingVal) }}"
                                    {{ $ratingCheck }} /><label for="rating{{ $i }}"
                                    title="{{ $ratingVal }} stars"></label>
                                @else
                                <input type="radio" id="rating{{ $i }}" name="rating" value="{{ $ratingVal }}"
                                    {{ $ratingCheck }} /><label class="half" for="rating{{ $i }}"
                                    title="{{ (floor($ratingVal) == 0)? '1/2' : (floor($ratingVal).' 1/2')  }} stars"></label>
                                @endif
                                @endfor
                            </fieldset>
                            <p class="ratings mb-0">Average rating: {{ $avgRating }}/5 stars</p>
                        </div>
                    </div>

                    <div class="right">
                        <div class="list-details">
                            <div class="detail all">
                                <p>All <span class="d-block">({{ $offersCount['total'] }})</span></p>
                            </div>
                            <div class="detail codes">
                                <p>Codes <span
                                        class="d-block">({{ (isset($offersCount['codes'])) ? $offersCount['codes'] : 0 }})</span>
                                </p>
                            </div>
                            <div class="detail deals">
                                <p>Deals <span
                                        class="d-block">({{ (isset($offersCount['deals'])) ? $offersCount['deals'] : 0 }})</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </section>

        <section class="row mb-30">
        <div class="col-12">
            @if(!isset($offers[0]))
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <strong>Apologies, </strong>we don't have any offers on {!! $storePage[0]->name !!}. You may have similar stores.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
        </div>

            <section class="col-12 col-lg-8">
                @foreach($offers as $offer)
                @php
                $offer_type = strtolower($offer->offer_type);
                $codeUrl = $offer->website;
                $codeUrl .= ($offer_type == 'codes') ? '?copy=' : '?shopnow=';
                $codeUrl .= $offer->id;
                $affiliate_url = (!empty($offer['affiliate_url'])) ? $offer['affiliate_url'] :
                $offer['store_affiliate_url'];
                @endphp
                <div class="voucher-card">
                    <div class="top">
                        <div class="left">
                            @php
                            $discLabel = setDiscountLabel($offer->discount_label, $offer_type);
                            @endphp
                            <p class="deal-type {{ $discLabel['class'] }}">{!! $discLabel['text'] !!}</p>
                            {{-- @php
                            @if($offer_type == 'codes')
                                <p class="deal-type off">{{ $offer->discount_label }}</p>
                            @elseif($offer_type == 'deals')
                            <p class="deal-type sale">{{ $offer->discount_label }}</p>
                            @else
                            <p class="deal-type free-shipping">{{ $offer->discount_label }}</p>
                            @endif

                            @endphp --}}
                            <div class="content">
                                {{--<p class="store-name">{{ $offer->store_name }}</p>--}}
                                <p class="store-name">{{ $offer->title }}</p>
                                {{--<p class="text-purple fw-600 mb-5">{{ $offer->title }}</p>--}}
                                <p>{!! $offer->description !!}</p>
                            </div>
                        </div>
                        <div class="right">
                            <p class="views">Views <span>{{ $offer->views_count }}</span></p>
                            <!-- <a href="javascript:void(0);" class="view-more">View More</a> -->
                        </div>
                    </div>
                    <div class="bottom">
                        <div class="meta">
                            <div class="separator"></div>
                            <div class="left">
                                <div class="date"><i class="fa fa-clock-o"></i>
                                    {{ offerDateFormat($offer->expiry_date) }}</div>
                                <ul class="share">
                                    <li>
                                        <a href="javascript:void(0);" class="fb fb-share"
                                            data-value="{!! route('page', $codeUrl) !!}"
                                            data-desc="{!! textToShare($offer->title) !!}"
                                            data-image="{!! asset('uploads/'.$offer->logo) !!}"><i
                                                class="fab fa-facebook-f"></i></a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);" class="tw t6w-share"
                                            data-value="{!! route('page', $codeUrl) !!}"
                                            data-desc="{!! textToShare($offer->title) !!}"><i
                                                class="fab fa-twitter"></i></a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);" class="wa wa-share"
                                            data-value="{!! route('page', $codeUrl) !!}"
                                            data-desc="{!! textToShare($offer->title) !!}"><i
                                                class="fab fa-whatsapp"></i></a>
                                    </li>
                                    <li>
                                        <a href="mailto:?subject={!! $offer->title !!}&body=Please Check this Link {!! route('page', $codeUrl) !!}"
                                            class="ib"><i class="fa fa-inbox"></i></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="right">
                                <a data-type="{!! $offer_type !!}" data-id="{!! $offer->id !!}"
                                    class="coupon-button @php echo $offer_type == 'codes' ? '' : 'full'; @endphp"
                                    href="javascript:;" data-url="{!! $affiliate_url !!}">Show @php echo $offer_type ==
                                    'codes' ? 'Code' : 'Deal'; @endphp
                                    <span>{{ hideChr($offer->voucher_code, -2) }}</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>


                @if(count($storeBanners) > 0 &&
                ( (count($offers) < 3 && $loop->last) || $loop->iteration % 3 == 0) )
                    @foreach($storeBanners as $key => $banner)
                    <div class="store-ad">
                        <a href="{!! $banner['click_url'] !!}"><img src="{!! asset('uploads/'. $banner['image']) !!}"
                                class="img-fluid" alt="{!! $banner['alt_text'] !!}"></a>
                    </div>
                    @php
                    unset($storeBanners[$key])
                    @endphp

                    @break
                    @endforeach
                    @endif

                    @endforeach
                    @if(count($offers) > 0)
                    <span style="margin: 0 auto;">{{ $offers->links() }}</span>
                    @endif
                    <!-- <div class="voucher-card">
                    <div class="top">
                        <div class="left">
                            <img src="{!! asset('assets/front/images/featured-vouchers/1.jpg') !!}" alt="">
                            <div class="content">
                                <p class="store-name">Rakuten TV</p>
                                <p class="text-purple fw-600 mb-5">Jewelry up tp 60% off, with code extra $4 off $39 for any order</p>
                                <p>Receive Delivery For Free Off Any Order At Campus Gifts</p>
                            </div>
                        </div>
                        <div class="right">
                            <p class="views">Views <span>198</span></p>
                            <a href="javascript:void(0);" class="view-more">View More</a>
                        </div>
                    </div>
                    <div class="bottom">
                        <div class="meta">
                            <div class="separator"></div>
                            <div class="left">
                                <div class="date"><i class="fa fa-clock-o"></i> Sep-1-2018</div>
                                <ul class="share">
                                    <li><a href="javascript:void(0);" class="fb"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="javascript:void(0);" class="tw"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="javascript:void(0);" class="wa"><i class="fab fa-whatsapp"></i></a></li>
                                    <li><a href="javascript:void(0);" class="ib"><i class="fa fa-inbox"></i></a></li>
                                </ul>
                            </div>
                            <div class="right">
                                <a href="javascript:void(0);" class="coupon-button"  data-toggle="modal" data-target="#codeModal">Show Code <span>9X</span></a>
                            </div>
                        </div>
                    </div>
                </div>-->
                    <!-- Carousel !-->
                    <style>
                    .top-offer .offer-card {
                        margin: 0 5px;
                    }
                    /* #storeCarousel .brand-logo{
                        min-height:110px;
                        width:75%;
                        margin:10px auto !important;
                        padding:0;
                        box-shadow:none;
                        border:none;
                    } */
                    .banner {
                        height: 200px;
                        width: 100%;
                        /* display:none; */

                    }
                    #storeCarousel .owl-dots{
                        text-align:center;
                    }
                    #storeCarousel .owl-dots .owl-dot{
                        padding: 0;
                        width: 14px;
                        height: 14px;
                        border-radius: 50px;
                        border: 1px solid #d0d0d0;
                        background-color: #fff;
                        box-shadow: 0px 8px 37.24px 0.76px rgba(205,205,205,0.24);
                        transition: 0.3s ease all;
                        margin:3px;
                    }
                    #storeCarousel .description{
                        min-height:100px;
                    }
                    #storeCarousel .owl-dots{
                        margin-top:20px;
                    }
                    #storeCarousel .owl-dots .active{
                        background-color: #2196f3;
                    }
                    #storeCarousel .store-name{
                        padding:0;
                        font-size:14px;
                    }
                    .alert.show{
                        opacity:1  !important;
                        visibility:visible !important;
                        position:static !important;
                        margin:10px auto;
                        width:100%;
                        box-sizing:border-box;
                    }
                    </style>
                    <div id="storeCarousel"class="owl-carousel owl-theme top-offer bg-transparent">
                    @foreach($similarStores as $simStore)
                        <div class="item">
                            <div class="offer-card">
                                <div class="banner"
                                    style="background:url('{!! ($simStore->cover) ? asset(uploadsDir(). $simStore->cover) : asset(defaultStoreCoverUrl()) !!}')no-repeat; background-position: center center;background-size:cover">
                                </div>
                                <div class="about-offer">
                                    <div class="brand-logo">
                                        <a href="{!! route('page', trim($simStore->website)) !!}">
                                            <img src="{!! asset(uploadsDir(). $simStore->logo) !!}" width="81"
                                                height="72" alt="{!! $simStore->name !!}"></a>
                                    </div>
                                    <h4 class="store-name">{!! $simStore->name !!}</h4>
                                    <p class="description m-0">{!! Str::words(strip_tags(html_entity_decode($simStore->description, ENT_QUOTES, 'UTF-8')), 10) !!}
                                    </p>
                                    <div class="text-center font-weight-bold" style="font-size:14px;">
                                        {!! $simStore->offers_count !!} Offers
                                    </div>
                                    <div class="meta">
                                        <div class="separator"></div>
                                        <div class="bottom pb-2">
                                        <a href="{!! route('page', trim($simStore->website)) !!}" class="v-btn yellow p-2 mx-auto">Visit Store</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                    <!-- /Carousel !-->

            </section>

            <aside class="col-12 col-lg-4">
                <div class="sidebar-card">
                    <h3 class="sidebar-heading">Popular Store’s</h3>

                    <ul class="sidebar-list list-unstyled mb-0">
                        @foreach($popularStores as $pStore)
                        <li><a href="{!! route('page', $pStore->website) !!}">{{ $pStore->name }}</a></li>
                        @endforeach
                    </ul>
                </div>

                @if(count($storeSideBanners) > 0)
                <div class="ad ad-carousel">
                    @foreach($storeSideBanners as $row)
                    <div class="item">
                        <a href="{!! $row->click_url !!}"><img src="{!! asset('uploads/'. $row->image) !!}" alt="{!! $row->alt_text !!}" class="img-fluid"></a>
                    </div>
                    @endforeach
                </div>
                @endif

                <div class="sidebar-card">
                    <h3 class="sidebar-heading">More from category</h3>
                    <ul class="sidebar-list list-unstyled mb-0">
                        @foreach($similarStores as $simStore)
                        <li><a href="{!! route('page', trim($simStore->website)) !!}">{{ $simStore->name }}</a></li>
                        @endforeach
                    </ul>
                </div>

                <div class="sidebar-card mb-0">
                    <h3 class="sidebar-heading">Today’s {{ $storePage[0]->name }} top offers</h3>

                    <ul class="sidebar-list-styled list-unstyled">
                        @foreach($topOffers as $tOffer)

                        @php
                        $clickUrl = (!empty($tOffer->affiliate_url)) ? $tOffer->affiliate_url :
                        $tOffer->store_affiliate_url;
                        @endphp
                        <li><a href="{!! $clickUrl !!}"><i class="arrow"></i>{{ $tOffer->store_name }}
                                {{ $tOffer->offer_type }}: {{ $tOffer->title }}</a></li>
                        @endforeach
                    </ul>

                    <table class="table sidebar-table">
                        <tbody>
                            <tr>
                                <td>Total Deals</td>
                                <td>{{ (isset($offersCount['deals'])) ? $offersCount['deals'] : 0 }}</td>
                            </tr>
                            @if( count($offers) > 0 )
                            <tr>
                                <td>Best Discount</td>
                                <td>{{ str_singular($offers[0]->offer_type) }}</td>
                            </tr>
                            @endif
                            <tr>
                                <td>Coupon Codes</td>
                                <td>{{ (isset($offersCount['codes'])) ? $offersCount['codes'] : 0 }}</td>
                            </tr>
                            <tr>
                                <td>Free Shipping Deals</td>
                                <td>{{ (isset($offersCount['free_shipping'])) ? $offersCount['free_shipping'] : 0 }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </aside>
        </section>

        @if($storePage[0]->description)
        <section class="row">
            <div class="col-12">
                <div class="store-overview">
                    <p class="mb-0">{!! html_entity_decode($storePage[0]->description, ENT_QUOTES, 'UTF-8') !!}</p>
                </div>
            </div>
        </section>
        @endif

        @if($storePage[0]->description_text)
        <section class="row">
            <div class="col-12">
                <div class="store-overview">
                    <p class="mb-0">{!! html_entity_decode($storePage[0]->description_text, ENT_QUOTES, 'UTF-8') !!}</p>
                </div>
            </div>
        </section>
        @endif

        @if($offers[0] != NULL)
        <section class="row">
            <div class="col-12">
                <div class="store-overview">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Discount</th>
                                <th>Description</th>
                                <th>Expires</th>
                            </tr>
                        </thead>
                        <tbody>
                        @php
                            $i = 0;
                        @endphp
                        @foreach($offers as $offer)
                            @php

                                $offer_type = strtolower($offer->offer_type);
                                $codeUrl = $offer->website;
                                $codeUrl .= ($offer_type == 'codes') ? '?copy=' : '?shopnow=';
                                $codeUrl .= $offer->id;
                                $affiliate_url = (!empty($offer['affiliate_url'])) ? $offer['affiliate_url'] : $offer['store_affiliate_url'];
                            @endphp
                            @if($i <= 5)
                            <tr>
                                @php
                                $discLabel = setDiscountLabel($offer->discount_label, $offer_type);
                                @endphp
                                <td>{{strip_tags(strtoupper($discLabel['text']))}}</td>
                                <td>{{ $offer->title }}</td>

                                <td>{{ offerDateFormat($offer->expiry_date) }}</td>
                            </tr>
                            @endif
                            @php
                            $i++;
                            @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        @endif


        @if($storePage[0]->website)
         <section class="row">
            <div class="col-12">
                <div class="store-overview">
                    <p class="mb-0">Official website - <a href="https://www.{!! $storePage[0]->website !!}" rel="nofollow" target="_blank">{!! $storePage[0]->name !!}</a></p>
                </div>
            </div>
        </section>
        @endif

    </section>
</section>

@include('front.partials._modal')
@include('front.partials._popularCategories')
@endsection

@section('js')
<script type="text/javascript">

$(".coupon-button").click(function() {
    $varName = $(this).data("id"),
        $store = $(this).data("url"),
        $type = $(this).data("type"),
        $url = window.location.href.split("?")[0],
        $typeNew = "codes" == $type ? "copy" : "shopnow", window.open($url + "?" + $typeNew + "=" + $varName,
            '_blank'), window.location = "?r=" + store
});

@if(isset($offerDetail['offer_type']) && strtolower($offerDetail['offer_type']) == 'codes')
$('#codeModal').modal('show');
@elseif(isset($offerDetail['offer_type']) && strtolower($offerDetail['offer_type']) == 'deals')
$('#dealModal').modal('show');
@endif

const offerCountUrl = "{!! route('offer.update.count') !!}";

$('.ad-carousel').owlCarousel({
    loop: true,
    dots: true,
    nav: false,
    autoplay: true,
    responsive: {
        0: {
            items: 1
        }
    }
});

$('#storeCarousel').owlCarousel({
    loop: true,
    dots: true,
    nav: false,
    autoplay:true,
    autoplayTimeout:3000,
    dots: true,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 3
        },
        1000: {
            items: 3
        }
    }
});

</script>
@endsection
