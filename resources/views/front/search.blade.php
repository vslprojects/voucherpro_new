@extends('front.layouts.app')

@include('front.partials._head')

@section('css')
    <!-- <link rel="stylesheet" href="{!! asset('assets/front/css/trending.css') !!}"> -->
@endsection

@section('content')

@include('front.partials._innerSearch')

<section class="trending">
    <div class="container">
        @include('front.partials._sideBanners')
        
        <h2><span>Result For</span> Levis</h2>

        <div class="row">
            <div class="col-12 col-md-6">
                <div class="voucher-card">
                    <div class="top">
                        <div class="left">
                            <img src="{!! asset('assets/front/images/featured-vouchers/1.jpg') !!}" alt="featured-vouchers">
                            <div class="content">
                                <h3>Levis: Bet £5 and Get £20 Free Bets at Ladbrokes</h3>
                                <p>Receive Delivery For Free Off Any Order At Campus Gifts</p>
                            </div>
                        </div>
                        <div class="right">
                            <p class="views">Views <span>198</span></p>
                            <a href="javascript:void(0);" class="view-more">View More</a>
                        </div>
                    </div>
                    <div class="bottom">
                        <div class="meta">
                            <div class="separator"></div>
                            <div class="left">
                                <div class="date"><i class="fa fa-clock-o"></i> Sep-1-2018</div>
                                <ul class="share">
                                    <li><a href="javascript:void(0);" class="fb"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="javascript:void(0);" class="tw"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="javascript:void(0);" class="wa"><i class="fab fa-whatsapp"></i></a></li>
                                    <li><a href="javascript:void(0);" class="ib"><i class="fa fa-inbox"></i></a></li>
                                </ul>
                            </div>
                            <div class="right">
                                <a href="javascript:void(0);" class="coupon-button">Show Code <span>9X</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-12 col-md-6">
                <div class="voucher-card">
                    <div class="top">
                        <div class="left">
                            <img src="{!! asset('assets/front/images/featured-vouchers/1.jpg') !!}" alt="featured-vouchers">
                            <div class="content">
                                <h3>Levis</h3>
                                <p>Receive Delivery For Free Off Any Order At Campus Gifts</p>
                            </div>
                        </div>
                        <div class="right">
                            <p class="views">Views <span>198</span></p>
                            <a href="javascript:void(0);" class="view-more">View More</a>
                        </div>
                    </div>
                    <div class="bottom">
                        <div class="meta">
                            <div class="separator"></div>
                            <div class="left">
                                <div class="date"><i class="fa fa-clock-o"></i> Sep-1-2018</div>
                                <ul class="share">
                                    <li><a href="javascript:void(0);" class="fb"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="javascript:void(0);" class="tw"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="javascript:void(0);" class="wa"><i class="fab fa-whatsapp"></i></a></li>
                                    <li><a href="javascript:void(0);" class="ib"><i class="fa fa-inbox"></i></a></li>
                                </ul>
                            </div>
                            <div class="right">
                                <a href="javascript:void(0);" class="coupon-button">Show Code <span>9X</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6">
                <div class="voucher-card">
                    <div class="top">
                        <div class="left">
                            <img src="{!! asset('assets/front/images/featured-vouchers/1.jpg') !!}" alt="featured-vouchers">
                            <div class="content">
                                <h3>Levis</h3>
                                <p>Receive Delivery For Free Off Any Order At Campus Gifts</p>
                            </div>
                        </div>
                        <div class="right">
                            <p class="views">Views <span>198</span></p>
                            <a href="javascript:void(0);" class="view-more">View More</a>
                        </div>
                    </div>
                    <div class="bottom">
                        <div class="meta">
                            <div class="separator"></div>
                            <div class="left">
                                <div class="date"><i class="fa fa-clock-o"></i> Sep-1-2018</div>
                                <ul class="share">
                                    <li><a href="javascript:void(0);" class="fb"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="javascript:void(0);" class="tw"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="javascript:void(0);" class="wa"><i class="fab fa-whatsapp"></i></a></li>
                                    <li><a href="javascript:void(0);" class="ib"><i class="fa fa-inbox"></i></a></li>
                                </ul>
                            </div>
                            <div class="right">
                                <a href="javascript:void(0);" class="coupon-button">Show Code <span>9X</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6">
                <div class="voucher-card">
                    <div class="top">
                        <div class="left">
                            <img src="{!! asset('assets/front/images/featured-vouchers/1.jpg') !!}" alt="featured-vouchers">
                            <div class="content">
                                <h3>Levis</h3>
                                <p>Receive Delivery For Free Off Any Order At Campus Gifts</p>
                            </div>
                        </div>
                        <div class="right">
                            <p class="views">Views <span>198</span></p>
                            <a href="javascript:void(0);" class="view-more">View More</a>
                        </div>
                    </div>
                    <div class="bottom">
                        <div class="meta">
                            <div class="separator"></div>
                            <div class="left">
                                <div class="date"><i class="fa fa-clock-o"></i> Sep-1-2018</div>
                                <ul class="share">
                                    <li><a href="javascript:void(0);" class="fb"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="javascript:void(0);" class="tw"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="javascript:void(0);" class="wa"><i class="fab fa-whatsapp"></i></a></li>
                                    <li><a href="javascript:void(0);" class="ib"><i class="fa fa-inbox"></i></a></li>
                                </ul>
                            </div>
                            <div class="right">
                                <a href="javascript:void(0);" class="coupon-button">Show Code <span>9X</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6">
                <div class="voucher-card">
                    <div class="top">
                        <div class="left">
                            <img src="{!! asset('assets/front/images/featured-vouchers/1.jpg') !!}" alt="featured-vouchers">
                            <div class="content">
                                <h3>Levis</h3>
                                <p>Receive Delivery For Free Off Any Order At Campus Gifts</p>
                            </div>
                        </div>
                        <div class="right">
                            <p class="views">Views <span>198</span></p>
                            <a href="javascript:void(0);" class="view-more">View More</a>
                        </div>
                    </div>
                    <div class="bottom">
                        <div class="meta">
                            <div class="separator"></div>
                            <div class="left">
                                <div class="date"><i class="fa fa-clock-o"></i> Sep-1-2018</div>
                                <ul class="share">
                                    <li><a href="javascript:void(0);" class="fb"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="javascript:void(0);" class="tw"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="javascript:void(0);" class="wa"><i class="fab fa-whatsapp"></i></a></li>
                                    <li><a href="javascript:void(0);" class="ib"><i class="fa fa-inbox"></i></a></li>
                                </ul>
                            </div>
                            <div class="right">
                                <a href="javascript:void(0);" class="coupon-button">Show Code <span>9X</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6">
                <div class="voucher-card">
                    <div class="top">
                        <div class="left">
                            <img src="{!! asset('assets/front/images/featured-vouchers/1.jpg') !!}" alt="featured-vouchers">
                            <div class="content">
                                <h3>Levis</h3>
                                <p>Receive Delivery For Free Off Any Order At Campus Gifts</p>
                            </div>
                        </div>
                        <div class="right">
                            <p class="views">Views <span>198</span></p>
                            <a href="javascript:void(0);" class="view-more">View More</a>
                        </div>
                    </div>
                    <div class="bottom">
                        <div class="meta">
                            <div class="separator"></div>
                            <div class="left">
                                <div class="date"><i class="fa fa-clock-o"></i> Sep-1-2018</div>
                                <ul class="share">
                                    <li><a href="javascript:void(0);" class="fb"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="javascript:void(0);" class="tw"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="javascript:void(0);" class="wa"><i class="fab fa-whatsapp"></i></a></li>
                                    <li><a href="javascript:void(0);" class="ib"><i class="fa fa-inbox"></i></a></li>
                                </ul>
                            </div>
                            <div class="right">
                                <a href="javascript:void(0);" class="coupon-button">Show Code <span>9X</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6">
                <div class="voucher-card">
                    <div class="top">
                        <div class="left">
                            <img src="{!! asset('assets/front/images/featured-vouchers/1.jpg') !!}" alt="featured-vouchers">
                            <div class="content">
                                <h3>Levis</h3>
                                <p>Receive Delivery For Free Off Any Order At Campus Gifts</p>
                            </div>
                        </div>
                        <div class="right">
                            <p class="views">Views <span>198</span></p>
                            <a href="javascript:void(0);" class="view-more">View More</a>
                        </div>
                    </div>
                    <div class="bottom">
                        <div class="meta">
                            <div class="separator"></div>
                            <div class="left">
                                <div class="date"><i class="fa fa-clock-o"></i> Sep-1-2018</div>
                                <ul class="share">
                                    <li><a href="javascript:void(0);" class="fb"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="javascript:void(0);" class="tw"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="javascript:void(0);" class="wa"><i class="fab fa-whatsapp"></i></a></li>
                                    <li><a href="javascript:void(0);" class="ib"><i class="fa fa-inbox"></i></a></li>
                                </ul>
                            </div>
                            <div class="right">
                                <a href="javascript:void(0);" class="coupon-button">Show Code <span>9X</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6">
                <div class="voucher-card">
                    <div class="top">
                        <div class="left">
                            <img src="{!! asset('assets/front/images/featured-vouchers/1.jpg') !!}" alt="featured-vouchers">
                            <div class="content">
                                <h3>Levis</h3>
                                <p>Receive Delivery For Free Off Any Order At Campus Gifts</p>
                            </div>
                        </div>
                        <div class="right">
                            <p class="views">Views <span>198</span></p>
                            <a href="javascript:void(0);" class="view-more">View More</a>
                        </div>
                    </div>
                    <div class="bottom">
                        <div class="meta">
                            <div class="separator"></div>
                            <div class="left">
                                <div class="date"><i class="fa fa-clock-o"></i> Sep-1-2018</div>
                                <ul class="share">
                                    <li><a href="javascript:void(0);" class="fb"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="javascript:void(0);" class="tw"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="javascript:void(0);" class="wa"><i class="fab fa-whatsapp"></i></a></li>
                                    <li><a href="javascript:void(0);" class="ib"><i class="fa fa-inbox"></i></a></li>
                                </ul>
                            </div>
                            <div class="right">
                                <a href="javascript:void(0);" class="coupon-button">Show Code <span>9X</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('front.partials._popularCategories')
@endsection
