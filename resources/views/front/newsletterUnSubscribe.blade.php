@extends('front.layouts.app')

@include('front.partials._head')

@section('css')
    <!-- <link rel="stylesheet" href="{!! asset('assets/front/css/content-pages.css') !!}"> -->
@endsection

@section('content')
    
    @include('front.partials._innerSearch')
    
    @if(isset($alert_success))
        <div class="alert alert-success alert-show" role="alert">
            <p class="text-success">If your email exist in our database you will receive an email. Please check your inbox and spam folder for unsubscribe link</p>
        </div>
    @elseif(isset($alert_error))
        <div class="alert alert-danger alert-show" role="alert">
            <p class="text-danger">Please specify a valid email address</p>
        </div>
    @endif
    
    <section class="content-page mt-25 mb-25">
        <div class="container">
            @include('front.partials._sideBanners')
            <div class="row">
                <div class="col-12">
                    <div class="content-wrap p-5">
                        <h1 class="page-title">Unsubscribe</h1>
                        <p></p>
                        <form class="pb-4" id="newsletterUnSub" action="{{ route('newsletter.unsubscribeSubmit') }}" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-9 form-fields pr-0">
                                   <input type="email" name="email" class="form-control border" placeholder="Enter Your Email" required="">
                                </div>
                                <div class="col-3">
                                    <button type="submit" class="v-btn yellow h-100 w-100">Unsubscribe</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>


@include('front.partials._popularCategories')
@endsection

@section('js')
  <script type="text/javascript">
      $(document).ready(function() {
        hideAlert()
      });
  </script>
@endsection