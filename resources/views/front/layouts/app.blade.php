<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <meta name="p:domain_verify" content="voucher"/>
@yield('head')

@yield('og-tag')


<!-- Favicon -->
    <link rel="icon" href="{!! asset('assets/front/images/icon/favicon-'. config('app.site') .'.png') !!}" type="image/x-icon" />

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Roboto+Slab:300,400,700">

    <link rel="stylesheet" href="{!! asset('assets/front/css/main-.css') !!}" media="all">
    <link rel="stylesheet" href="{!! asset('assets/front/css/custom.css') !!}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    @yield('css')

</head>

<body class="">

<!--==================
    Preloader
===================-->

<!-- <div class="preloader">
    <div class="outer-circle">
        <div class="inner-circle"></div>
    </div>
</div> -->

<div class="main-wrapper">
    <!--==================
        Header
    ===================-->

    <header>
        <div class="container">
            <nav class="navbar navbar-expand-lg px-0">
                <a class="navbar-brand" href="#">
                    <img src="https://www.voucherpro.co.uk/uploads/logo-1569390591.svg" width="250" height="35" alt="#">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#voucherProNav" aria-controls="voucherProNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="voucherProNav">
                    <ul class="navbar-nav ml-auto">
{{--                        <li class="nav-item">--}}
{{--                            <a class="nav-link " href="">Travel Deals</a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item">--}}
{{--                            <a class="nav-link " href="">Fashion Deals</a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item">--}}
{{--                            <a class="nav-link " href="">Lockdown Deals</a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item">--}}
{{--                            <a class="nav-link " href="">Furniture Deals</a>--}}
{{--                        </li>--}}

                   <li class="nav-item">
	                        <a class="nav-link" href="{!! route('page', 'daily-treadning') !!}">Daily Trendings</a>
	                    </li>
	                    <li class="nav-item">
	                        <a class="nav-link" href="{!! route('page', 'daily-treadning') !!}">Travel Deals</a>
	                    </li>
	                    <li class="nav-item">
	                        <a class="nav-link" href="{!! route('category') !!}">Categories</a>
	                    </li>
	                    <li class="nav-item">
	                        <a class="nav-link" href="{!! route('store') !!}">All Stores</a>
	                    </li>
	                    <li class="nav-item">
	                        <a class="nav-link" href="{!! route('contact') !!}">Contact Us</a>
	                    </li>
                        <li class="nav-item associate">

                            <a class="nav-link" href="#"><img src="" alt="voucher"></a>

                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>

@yield('content')

<!--==================
			Footer
	===================-->

    <section class="newsletter">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="newsletter-container">
                        <div class="row">
                            <div class="col-12 col-sm-10 mx-sm-auto">
                                <div class="newsletter-wrap">
                                    <img src="{!! asset('assets/front/images/newsletter-envelope.png') !!}" width="234" height="155" class="img-fluid" alt="Newsletter" />
                                    <div class="form-area">
                                        <h5>Get voucher codes and online deals delivered straight to your inbox</h5>
                                        <form id="newsletterForm" action="{{ route('newsletter.subscribe') }}">
                                            <input type="email" class="input-email" name="email" placeholder="Please write your Email to subscribe">
                                            <button type="submit" class="send-newsletter"><i class="far fa-envelope"></i></button>
                                            <label class="disclaimer" for="newsletter-disclaimer">
                                                <input type="checkbox" id="newsletter-disclaimer" name="disclaimer">
                                                <span class="box"></span>
                                                <span class="text">Please click here if you would like to receive information about our services, products and exclusive offers through email. You can <a href="{{ route('newsletter.unsubscribe') }}" class="text-blue">Unsubscribe</a> at any time. Read our <a href="{!! route('page', 'privacy-policy') !!}" target="_blank" class="text-purple">Privacy Policy</a></span>
                                            </label>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="alert alert-success" role="alert">
        <p class="text-success"></p>
    </div>
    <div class="alert alert-danger" role="alert">
        <p class="text-danger"></p>
    </div>

    <input type="hidden" id="searchUrl" value="{!! route('search.stores') !!}">

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="footer-top">
                        <div class="row text-center text-sm-left">
                            <div class="col-12 col-sm-6 col-lg-3">
                                <h6 class="footer-heading">Browse Site</h6>
                                <ul class="footer-links">
                                    <li><a href="{!! route('store') !!}">All Stores</a></li>
                                    <li><a href="{!! route('category') !!}">Top Categories</a></li>
                                    <li><a href="{!! route('blogs') !!}">Blog</a></li>
                                    <li><a href="{!! route('contact') !!}">Contact Us</a></li>
                                    <li><a href="{!! route('voucher.request') !!}">Submit A Voucher</a></li>
                                    <li><a href="{!! url('sitemap.xml') !!}">Sitemap</a></li>
                                    @if(config('app.site') == 'voucherpro')
                                        <li><a href="{!! route('extension') !!}" target="_blank">
                                                <img style="max-width:170px; margin-bottom:10px;" class="img-fluid" src="{!! asset(uploadsDir().'chrome-extension-btn.png') !!}" title="VoucherPro Extension" alt="VoucherPro Extension" />
                                            </a></li>
                                        <li><a href="https://play.google.com/store/apps/details?id=com.application.voucherpro&hl=en" target="_blank"><img style="max-width:170px; margin-bottom:10px;" class="img-fluid" src="{!! asset(uploadsDir().'gplaydownload.png') !!}" title="VoucherPro App" alt="VoucherPro App" /></a></li>
                                        {{--                                    <li><a href="https://apps.apple.com/pk/app/voucher-pro/id1262423009" target="_blank"><img style="max-width:170px; margin-bottom:20px;" class="img-fluid" src="{!! asset(uploadsDir().'ios-download.png') !!}" title="VoucherPro App" alt="VoucherPro App" /></a></li>--}}
                                        <li><a href="#" target="_blank"><img style="max-width:170px; margin-bottom:20px;" class="img-fluid" src="{!! asset(uploadsDir().'ios-download.png') !!}" title="VoucherPro App" alt="VoucherPro App" /></a></li>
                                    @endif

                                    @if(config('app.site') == '247couponcodes')
                                        <li><a href="https://www.247couponcodes.com/" target="_blank">Promo Codes</a></li>
                                    @endif
                                </ul>

                            <!-- <h6 class="footer-heading">Download our Mobile App</h6>
	                            <ul class="list-inline app-links">
	                                <li class="list-inline-item">
	                                    <a href="javascript:void(0);"><span class="app google-play"></span></a>
	                                </li>
	                                <li class="list-inline-item">
	                                    <a href="javascript:void(0);"><span class="app app-store"></span></a>
	                                </li>
	                            </ul>
	                            <div>
	                                <a href="amazon-associates.php">
	                                    <amp-img src="{!! asset('assets/front/images/amazon-big.png') !!}" class="img-fluid" alt="Amazon Associate"></amp-img>
	                                </a>
	                            </div> -->
                            </div>

                            <div class="col-12 col-sm-6 col-lg-3">
                                <h6 class="footer-heading">Special Events</h6>
                                <ul class="footer-links">

                                    <li><a href="#">voucher</a></li>

                                </ul>
                            </div>

                            <div class="col-12 col-sm-6 col-lg-2">
                                <h6 class="footer-heading">Information</h6>
                                <ul class="footer-links">
                                    <li><a href="">About Us</a></li>
                                    <li><a href="">Cookies Policy</a></li>
                                    <li><a href="">Privacy Policy</a></li>
                                    <li><a href="">Terms & Conditions</a></li>
                                </ul>
                                <h6 class="footer-heading">Popular Stores</h6>
                                <ul class="footer-links">
                                    <li><a href="#">voucher</a></li>
                                </ul>
                            </div>

                            <div class="col-12 col-sm-6 col-lg-4">
                                <h6 class="footer-heading">Write to Us</h6>
                                <p class="footer-text">We welcome all comments and feedback on our vouchers, discount codes and hot deals portal</p>
                                <form method="post" id="footerContactForm" action="{{ route('process-contact') }}">
                                    <div class="row">
                                        <div class="col-12 footer-form-fields">
                                            <input class="form-control" name="name" value="{!! old('name') !!}" type="text" placeholder="Your name" maxlength="128" required>
                                        </div>
                                        <div class="col-12 footer-form-fields">
                                            <input class="form-control" name="email" value="{!! old('email') !!}" type="email" placeholder="Email" maxlength="128" required>
                                        </div>
                                        <div class="col-12 footer-form-fields">
                                            <textarea class="form-control" placeholder="Message" name="description" maxlength="65000" required>{!! old('description') !!}</textarea>
                                        </div>
                                        <div class="col-12 footer-form-fields">
                                            <label class="disclaimer" for="footer-disclaimer">
                                                <input type="checkbox" name="terms" id="footer-disclaimer">
                                                <span class="box"></span>
                                                <span class="text">I have read and agree to the <a href="{!! route('page', 'terms-conditions') !!}" target="_blank">Terms and Conditions</a> and <a href="{!! route('page', 'privacy-policy') !!}" target="_blank">Privacy Policy</a></span>
                                            </label>
                                        </div>
                                        <div class="col-12 footer-form-fields">
                                            <button type="submit">Submit</button>
                                        </div>
                                    </div>
                                </form>
                                <p class="text-white">Socialise with us!</p>
                                <ul class="list-social list-inline">
                                    <li class="list-inline-item"><a href="#" class="fb" target="_blank"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                                    <li class="list-inline-item"><a href="#" class="tw" target="_blank"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                                    <li class="list-inline-item"><a href="#" class="go" target="_blank"><i class="fab fa-google" aria-hidden="true"></i></a></li>
                                    <li class="list-inline-item"><a href="#" class="pi" target="_blank"><i class="fab fa-pinterest-p" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="footer-bottom">
                        <p class="copyright">
                            voucher
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

</div>
<a target="_blank" href="" id="openWin"></a>
<!--==================
        JS Files
===================-->


<script src="{!! asset('assets/front/js/all.min.js') !!}"></script>
<script src="{!! asset('assets/front/js/script.js') !!}"></script>

@yield('js')

{{--	<script>--}}
{{--        //redirection from social newtwork facebook--}}
{{--        $(function(){--}}
{{--            var name = "{!! request('fbclid') ?: '' !!}";--}}
{{--            if (name != '') {--}}
{{--                var store = "{!! $offerDetail['affiliate_url'] ? $offerDetail['affiliate_url'] : $offerDetail['store_affiliate_url']  !!}";--}}
{{--                if (store) {--}}
{{--                    var popUp = window.open(store, '_blank');--}}
{{--                    if (popUp == null || typeof(popUp)=='undefined') {--}}
{{--                        alert('Please allow pop-up from your browser then click OK.');--}}
{{--                        location.reload(true);--}}
{{--                    }--}}
{{--                    else {--}}
{{--                        popUp.focus();--}}
{{--                    }--}}
{{--                }--}}
{{--            }--}}
{{--        });--}}

{{--	</script>--}}
@if(config('app.site') == 'voucherpro')
    <script type="text/javascript">
        var vglnk = {key: 'e65a99ac565605320eca9c0f3ca94d40'};
        (function(d, t) {
            var s = d.createElement(t);
            s.type = 'text/javascript';
            s.async = true;
            s.src = '//cdn.viglink.com/api/vglnk.js';
            var r = d.getElementsByTagName(t)[0];
            r.parentNode.insertBefore(s, r);
        }(document, 'script'));
    </script>
@else
    <script type="text/javascript">
        var vglnk = {key: '1a65855990600e0b38ee0789bf2339a2'};
        (function(d, t) {
            var s = d.createElement(t);
            s.type = 'text/javascript';
            s.async = true;
            s.src = '//cdn.viglink.com/api/vglnk.js';
            var r = d.getElementsByTagName(t)[0];
            r.parentNode.insertBefore(s, r);
        }(document, 'script'));
    </script>
@endif
</body>
</html>
