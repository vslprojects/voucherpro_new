@extends('front.layouts.app')

@include('front.partials._head')

@section('css')
    <!-- <link rel="stylesheet" href="{!! asset('assets/front/css/submit-voucher.css') !!}"> -->
@endsection

@section('content')

@include('front.partials._innerSearch')

<div class="container">
    @include('front.partials._sideBanners')
</div>

<section class="submit-voucher">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="heading py-0">
                        <h2>Submit a Voucher</h2>
                        <p>Submit your voucher and promotional codes with other smart shoppers to help them save money at popular online stores.i It’s quick, easy, and best of all, helpful!i</p>
                    </div>

                    <form id="submitVoucherForm" method="post" action="{{ route('voucher.request.submit') }}">
                        <div class="row">
                            <div class="col-12 col-lg-6 form-fields mb-20">
                                <input type="text" class="form-control" name="name" placeholder="Your Name">
                            </div>
                            <div class="col-12 col-lg-6 form-fields mb-20">
                                <input type="email" class="form-control" name="email" placeholder="Your Email">
                            </div>
                            <div class="col-12 col-lg-6 form-fields mb-20">
                                <input type="text" class="form-control" name="store" placeholder="Store Name">
                            </div>
                            <div class="col-12 col-lg-6 form-fields mb-20">
                                <input type="text" class="form-control" name="description" placeholder="Voucher Description">
                            </div>
                            <div class="col-12 col-lg-6 mb-20 relative">
                                <div class="switch-field dependency">
                                    <input type="radio" id="printable_voucher" name="voucher_type" value="pv" checked/>
                                    <label for="printable_voucher">I have a printable Voucher</label>
                                    <input type="radio" id="promo_code" name="voucher_type" value="pc" />
                                    <label for="promo_code">I have a promo code</label>
                                </div>
                                <a href="javascript:void(0);" class="help" data-toggle="tooltip" data-placement="top" title="Promo codes typically are added at checkout when shopping online. Printable Vouchers can be printed out and given to a cashier at a store.">?</a>
                            </div>
                            <div class="col-12 col-lg-6 form-fields mb-20">
                                <input type="text" class="form-control dependent" name="voucher" placeholder="Printable Voucher">
                            </div>
                            <div class="col-12 col-lg-6 form-fields mb-20">
                                <input type="text" onfocus="(this.type='date')" onfocusout="(this.type='text')" name="expire_date" class="form-control" placeholder="Voucher Expiration">
                            </div>
                            <div class="col-12 col-lg-6 mb-20">
                                <label class="disclaimer" for="disclaimer-voucher-submission">
                                    <input type="checkbox" name="terms" id="disclaimer-voucher-submission">
                                    <span class="box"></span>
                                    <span class="text">I have read and agree to the <a href="{!! route('page', 'terms-conditions') !!}" class="text-purple" target="_blank">Terms and Conditions</a> and <a href="{!! route('page', 'privacy-policy') !!}" class="text-purple" target="_blank">Privacy Policy</a></span>
                                </label>
                            </div>
                            <div class="col-12">
                                <button class="v-btn yellow fz-18 w-auto h-auto px-5 py-3">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

@include('front.partials._popularCategories')
@endsection