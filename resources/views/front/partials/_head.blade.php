@section('head')

    <title>Voucher Pro</title>
    <meta name="description" content="vouhcer-pro">
    <meta name="keywords" content="voucher-pro">

    <meta property=og:title content="voucher-pro"/>
    <meta property=og:site_name content="voucher-pro"/>
    <meta property=og:url content="voucher-pro"/>
    <meta property=og:description content="voucher-pro"/>


    <link rel="manifest" href="/manifest.webmanifest"/>
    <link rel="canonical" href="{!! trim(url()->current()) !!}" />

    <meta name="p:domain_verify" content="voucher"/>
    <meta property=og:type content=voucher/>
    <meta name="twitter:site" content="voucher" />
    <meta name="google-site-verification" content="voucher" />

{{--    <meta property=og:image content="https://www.voucherpro.co.uk/uploads/ogg_logo.jpg"/> --}}
    <meta property=og:type content="DiscountCode"/>
    <meta name="twitter:site" content="@voucherpro" />
    <meta name="google-site-verification" content="voucher" />

    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="voucher" />
    <meta name="robots" content="Index, follow">


{{--    <script type="application/ld+json">--}}
{{--        {--}}
{{--          "@context": "http://schema.org",--}}
{{--          "@type": "Store",--}}
{{--          "name": "247couponcodes",--}}
{{--          "description": "Get the best Deals & Discounts from USA and Canada",--}}
{{--          "image": "{!! uploadsUrl($siteSettings->logo) !!}"--}}
{{--        }--}}
{{--    </script>--}}

    <script type="application/ld+json">
        {
          "@context": "http://schema.org",
          "@type": "Store",
          "name": "Voucher Pro",
          "description": "UK's No1. place to find the unbelievable Deals & Discounts.",
          "image": ""
        }
    </script>

@endsection
