@extends('front.layouts.app')
@include('front.partials._head')
@section('content')
@include('front.partials._innerSearch')
<section class="home-banner mb-20">
    <div class="owl-carousel blog-carousels">
         @if(isset($sliders))
            @foreach($sliders as $slider)
            <div class="item">
                <div class="banner-wrap">
                    <div class="banner-img">
                        <a href="{!! $slider->click_url !!}">
                            <img src="{!! asset(uploadsDir().$slider->image) !!}" alt="{!! $slider->alt_text !!}">
                        </a>
                    </div>
                </div>
            </div>
          @endforeach
        @endif
        
    </div>
</section>
<section class="top-offer">
    <div class="container">
        <div class="row">
            @if(isset($posts))
                @foreach($posts as $post)
                    <div class="col-12 col-sm-6 col-xl-4">
                        <div class="offer-card">
                            <!-- <ul class="blog-tags"><li><a href="javascript:;">Money saving tips</a></li></ul> -->
                            <div class="banner" style="background:url('{!! uploadsDir().$post->mediafile !!}') no-repeat; background-position:center center; background-size:cover;">
                            </div>
                            <div class="about-offer pt-30">
                                <h4 class="store-name">{!! $post->title !!}</h4>
                                <p class="description p-0 m-0">{!! date('M d, Y', strtotime($post->created_at)) !!}</p>
                                <a class="coupon-button full mx-auto mb-30" href="{!! route('blog-detail', $post->slug) !!}">View Details</a>
                            </div>
                        </div>
                    </div>
               @endforeach
            @endif

        </div>
        <div class="row">
            @if(isset($posts) && count($posts) > 0)
            <span style="margin: 0 auto;">{{ $posts->links() }}</span>
            @endif
        </div>

    </div>
</section>

@endsection
@section('js')
<script>
$(".blog-carousels").owlCarousel({
    loop: true,
    margin:20,
    center: true,
    nav: true,
    autoplay: true,
    autoplayTimeout: 5000,
    items: 2,

});
</script>
@endsection
@section('css')
<style>
.top-offer .banner {
    min-height: 160px;
    border-radius: 6px 6px 0 0;
}
.top-offer .offer-card .store-name+p {
    min-height:30px
}

/* ,.blog-carousels.owl-carousel .owl-dots.disabled */
.blog-carousels.owl-carousel .owl-nav.disabled {
        display:block
}
.blog-tags {
    position: absolute;
    font-size: 14px;
    list-style: none;
    padding: 0;
    top: 17px;
    margin: 0;
}

.blog-tags a {
    color: #FFF;
    background: #FF9401;
    padding: 2px 10px;
    border-radius: 5px 0;
}
</style>
@endsection