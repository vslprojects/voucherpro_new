@extends('front.layouts.app')

@include('front.partials._head')

@section('css')
    <!-- <link rel="stylesheet" href="{!! asset('assets/front/css/trending.css') !!}"> -->
@endsection

@section('content')

@include('front.partials._innerSearch')

<section class="trending">
    <div class="container">
        @include('front.partials._sideBanners')
        
        <div class="row">
            
            <div class="col-12 col-lg-9">
                <div class="row">
                    @foreach($eventOffers as $row)

                    @php
                        $offer_type = strtolower($row->offer_type);
                        $clickUrl = $row->website;
                        $clickUrl .= ($offer_type == 'codes') ? '?copy=' : '?shopnow=';
                        $clickUrl .= $row->id;

                        $affiliate_url = (!empty($row->affiliate_url)) ? $row->affiliate_url : $row->store_affiliate_url;

                    @endphp

                    <div class="col-12">
                        <div class="voucher-card">
                            <div class="top">
                                <div class="left">
                                    <a href="{!! route('page', trim($row->website)) !!}">
                                        <img src="{!! asset('uploads/'.$row->logo) !!}" alt="{!! $row->store_name !!}"></a>
                                    <div class="content">
                                        {{-- <h3>{!! $row->store_name !!}</h3>--}}
                                        <h3>{!! Str::words(strip_tags($row->title), 20) !!}</h3>
                                        {{--<p>{!! Str::words(strip_tags($row->title), 20) !!}</p>--}}
                                    </div>
                                </div>
                                <div class="right">
                                    <p class="views">Views <span>{{ $row->views_count }}</span></p>
                                    <!-- <a href="javascript:void(0);" class="view-more">View More</a> -->
                                </div>
                            </div>
                            <div class="bottom">
                                <div class="meta">
                                    <div class="separator"></div>
                                    <div class="left">
                                        <ul class="share">
                                            <li>
                                                <a href="javascript:void(0);" class="fb fb-share" data-value="{!! route('page', $clickUrl) !!}" data-desc="{!! textToShare($row->title) !!}" 
                                                data-image="{!! asset('uploads/'.$row->logo) !!}"><i class="fab fa-facebook-f"></i></a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0);" class="tw tw-share" data-value="{!! route('page', $clickUrl) !!}" data-desc="{!! textToShare($row->title) !!}"><i class="fab fa-twitter"></i></a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0);" class="wa wa-share" data-value="{!! route('page', $clickUrl) !!}" data-desc="{!! textToShare($row->title) !!}"><i class="fab fa-whatsapp"></i></a>
                                            </li>
                                            <li>
                                                <a href="mailto:?subject={!! $row->title !!}&body=Please Check this Link {!! route('page', $clickUrl) !!}" class="ib"><i class="fa fa-inbox"></i></a>
                                            </li>
                                            <li><div class="date"><i class="fa fa-clock-o"></i> {{ offerDateFormat($row->expiry_date) }}</div></li>
                                        </ul>
                                    </div>
                                    <div class="right">

                                         <a data-type="{!! $offer_type !!}" data-id="{!! $row->id !!}" class="coupon-button @php echo $offer_type == 'codes' ? '' : 'full'; @endphp" href="javascript:;" data-url="{!! $affiliate_url !!}">Show @php echo $offer_type == 'codes' ? 'Code' : 'Deal'; @endphp
                                        <span>{{ hideChr($row->voucher_code, -2) }}</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @if(count($eventOffers) > 0)
                        <span style="margin: 0 auto;">{{ $eventOffers->links() }}</span>
                    @endif
                </div>
            </div>
            <div class="col-12 col-lg-3 mb-4 mb-lg-0">
                <div class="description-card">
                    @if(isset($eventPage[0]->image) && !empty($eventPage[0]->image))
                    <img src="{!! asset('uploads/'.$eventPage[0]->image) !!}" class="img-fluid d-block mx-auto mb-20" alt="{!! $eventPage[0]->name !!}">
                    @endif
                    <h1 class="mb-10 text-center text-lg-left"><span>{!! $eventPage[0]->name !!}</span></h1>
                    {!! html_entity_decode($eventPage[0]->description, ENT_QUOTES, 'UTF-8') !!}
                    <!-- <h3 class="mb-20 text-center text-lg-left"><img src="{!! asset('assets/front/images/offers.png') !!}" class="mr-10" alt="Offers"><span class>Similar</span> Offers</h3>
                    <ul class="offers-list">
                        <li><a href="javascript:void(0);">Rakuten TV</a></li>
                        <li><a href="javascript:void(0);">Hochanda</a></li>
                        <li><a href="javascript:void(0);">Rakuten TV</a></li>
                        <li><a href="javascript:void(0);">Hochanda</a></li>
                        <li><a href="javascript:void(0);">Rakuten TV</a></li>
                        <li><a href="javascript:void(0);">Hochanda</a></li>
                    </ul> -->
                </div>
            </div>
        </div>
    </div>
</section>
@include('front.partials._popularCategories')
@include('front.partials._modal')

@endsection

@section('js')

    <script type="text/javascript">
        $(".coupon-button").click(function(){$varName=$(this).data("id"),$store=$(this).data("url"),$type=$(this).data("type"),$url=window.location.href.split("?")[0],$typeNew="codes"==$type?"copy":"shopnow",window.open($url+"?"+$typeNew+"="+$varName),window.location="?r="+store});
        

        @if(isset($offerDetail['offer_type']) && strtolower($offerDetail['offer_type']) == 'codes')
            $('#codeModal').modal('show');
        @elseif(isset($offerDetail['offer_type']) && strtolower($offerDetail['offer_type']) == 'deals')
            $('#dealModal').modal('show');
        @endif

     
    </script>
@endsection

