@extends('front.layouts.app')

@include('front.partials._head')

@section('css')
    <!-- <link rel="stylesheet" href="{!! asset('assets/front/css/trending.css') !!}"> -->
@endsection

@section('content')

@include('front.partials._innerSearch')

<section class="trending">
    <div class="container">
        @include('front.partials._sideBanners')
        
        <div class="row">
            <div class="col-12 col-lg-3 mb-4 mb-lg-0">
                <div class="description-card">
                    <img src="{!! asset('assets/front/images/trending-logo.png') !!}" class="img-fluid d-block mx-auto mb-20" alt="Valentines">
                    <h1 class="mb-10 text-center text-lg-left"><span>Daily</span> Trending</h1>
                    <p class="text-gray-2 fz-14 description text-center text-lg-left">Lorem Ipsum is simply dumy text of the printing and type setting industry. Lorem Ipsum has been the industry stand dumy text ever since the print took a galley of type and  it make a book. has survived not only five but also the leap into electronic type setting, remain changed.</p>
                    <h3 class="mb-20 text-center text-lg-left"><img src="{!! asset('assets/front/images/offers.png') !!}" class="mr-10" alt="Offers"><span class>Similar</span> Offers</h3>
                    <ul class="offers-list">
                        <li><a href="javascript:void(0);">Rakuten TV</a></li>
                        <li><a href="javascript:void(0);">Hochanda</a></li>
                        <li><a href="javascript:void(0);">Rakuten TV</a></li>
                        <li><a href="javascript:void(0);">Hochanda</a></li>
                        <li><a href="javascript:void(0);">Rakuten TV</a></li>
                        <li><a href="javascript:void(0);">Hochanda</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-12 col-lg-9">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="voucher-card">
                            <div class="top">
                                <div class="left">
                                    <img src="{!! asset('assets/front/images/featured-vouchers/1.jpg') !!}" alt="featured-vouchers">
                                    <div class="content">
                                        <h3>Rakuten TV</h3>
                                        <p>Receive Delivery For Free Off Any Order At Campus Gifts</p>
                                    </div>
                                </div>
                                <div class="right">
                                    <p class="views">Views <span>198</span></p>
                                    <a href="javascript:void(0);" class="view-more">View More</a>
                                </div>
                            </div>
                            <div class="bottom">
                                <div class="meta">
                                    <div class="separator"></div>
                                    <div class="left">
                                        <div class="date"><i class="fa fa-clock-o"></i> Sep-1-2018</div>
                                        <ul class="share">
                                            <li><a href="javascript:void(0);" class="fb"><i class="fab fa-facebook-f"></i></a></li>
                                            <li><a href="javascript:void(0);" class="tw"><i class="fab fa-twitter"></i></a></li>
                                            <li><a href="javascript:void(0);" class="wa"><i class="fab fa-whatsapp"></i></a></li>
                                            <li><a href="javascript:void(0);" class="ib"><i class="fa fa-inbox"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="right">
                                        <a href="javascript:void(0);" class="coupon-button">Show Code <span>9X</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-12 col-md-6">
                        <div class="voucher-card">
                            <div class="top">
                                <div class="left">
                                    <img src="{!! asset('assets/front/images/featured-vouchers/1.jpg') !!}" alt="featured-vouchers">
                                    <div class="content">
                                        <h3>Rakuten TV</h3>
                                        <p>Receive Delivery For Free Off Any Order At Campus Gifts</p>
                                    </div>
                                </div>
                                <div class="right">
                                    <p class="views">Views <span>198</span></p>
                                    <a href="javascript:void(0);" class="view-more">View More</a>
                                </div>
                            </div>
                            <div class="bottom">
                                <div class="meta">
                                    <div class="separator"></div>
                                    <div class="left">
                                        <div class="date"><i class="fa fa-clock-o"></i> Sep-1-2018</div>
                                        <ul class="share">
                                            <li><a href="javascript:void(0);" class="fb"><i class="fab fa-facebook-f"></i></a></li>
                                            <li><a href="javascript:void(0);" class="tw"><i class="fab fa-twitter"></i></a></li>
                                            <li><a href="javascript:void(0);" class="wa"><i class="fab fa-whatsapp"></i></a></li>
                                            <li><a href="javascript:void(0);" class="ib"><i class="fa fa-inbox"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="right">
                                        <a href="javascript:void(0);" class="coupon-button">Show Code <span>9X</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-6">
                        <div class="voucher-card">
                            <div class="top">
                                <div class="left">
                                    <img src="{!! asset('assets/front/images/featured-vouchers/1.jpg') !!}" alt="featured-vouchers">
                                    <div class="content">
                                        <h3>Rakuten TV</h3>
                                        <p>Receive Delivery For Free Off Any Order At Campus Gifts</p>
                                    </div>
                                </div>
                                <div class="right">
                                    <p class="views">Views <span>198</span></p>
                                    <a href="javascript:void(0);" class="view-more">View More</a>
                                </div>
                            </div>
                            <div class="bottom">
                                <div class="meta">
                                    <div class="separator"></div>
                                    <div class="left">
                                        <div class="date"><i class="fa fa-clock-o"></i> Sep-1-2018</div>
                                        <ul class="share">
                                            <li><a href="javascript:void(0);" class="fb"><i class="fab fa-facebook-f"></i></a></li>
                                            <li><a href="javascript:void(0);" class="tw"><i class="fab fa-twitter"></i></a></li>
                                            <li><a href="javascript:void(0);" class="wa"><i class="fab fa-whatsapp"></i></a></li>
                                            <li><a href="javascript:void(0);" class="ib"><i class="fa fa-inbox"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="right">
                                        <a href="javascript:void(0);" class="coupon-button">Show Code <span>9X</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-6">
                        <div class="voucher-card">
                            <div class="top">
                                <div class="left">
                                    <img src="{!! asset('assets/front/images/featured-vouchers/1.jpg') !!}" alt="featured-vouchers">
                                    <div class="content">
                                        <h3>Rakuten TV</h3>
                                        <p>Receive Delivery For Free Off Any Order At Campus Gifts</p>
                                    </div>
                                </div>
                                <div class="right">
                                    <p class="views">Views <span>198</span></p>
                                    <a href="javascript:void(0);" class="view-more">View More</a>
                                </div>
                            </div>
                            <div class="bottom">
                                <div class="meta">
                                    <div class="separator"></div>
                                    <div class="left">
                                        <div class="date"><i class="fa fa-clock-o"></i> Sep-1-2018</div>
                                        <ul class="share">
                                            <li><a href="javascript:void(0);" class="fb"><i class="fab fa-facebook-f"></i></a></li>
                                            <li><a href="javascript:void(0);" class="tw"><i class="fab fa-twitter"></i></a></li>
                                            <li><a href="javascript:void(0);" class="wa"><i class="fab fa-whatsapp"></i></a></li>
                                            <li><a href="javascript:void(0);" class="ib"><i class="fa fa-inbox"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="right">
                                        <a href="javascript:void(0);" class="coupon-button">Show Code <span>9X</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-6">
                        <div class="voucher-card">
                            <div class="top">
                                <div class="left">
                                    <img src="{!! asset('assets/front/images/featured-vouchers/1.jpg') !!}" alt="featured-vouchers">
                                    <div class="content">
                                        <h3>Rakuten TV</h3>
                                        <p>Receive Delivery For Free Off Any Order At Campus Gifts</p>
                                    </div>
                                </div>
                                <div class="right">
                                    <p class="views">Views <span>198</span></p>
                                    <a href="javascript:void(0);" class="view-more">View More</a>
                                </div>
                            </div>
                            <div class="bottom">
                                <div class="meta">
                                    <div class="separator"></div>
                                    <div class="left">
                                        <div class="date"><i class="fa fa-clock-o"></i> Sep-1-2018</div>
                                        <ul class="share">
                                            <li><a href="javascript:void(0);" class="fb"><i class="fab fa-facebook-f"></i></a></li>
                                            <li><a href="javascript:void(0);" class="tw"><i class="fab fa-twitter"></i></a></li>
                                            <li><a href="javascript:void(0);" class="wa"><i class="fab fa-whatsapp"></i></a></li>
                                            <li><a href="javascript:void(0);" class="ib"><i class="fa fa-inbox"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="right">
                                        <a href="javascript:void(0);" class="coupon-button">Show Code <span>9X</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-6">
                        <div class="voucher-card">
                            <div class="top">
                                <div class="left">
                                    <img src="{!! asset('assets/front/images/featured-vouchers/1.jpg') !!}" alt="featured-vouchers">
                                    <div class="content">
                                        <h3>Rakuten TV</h3>
                                        <p>Receive Delivery For Free Off Any Order At Campus Gifts</p>
                                    </div>
                                </div>
                                <div class="right">
                                    <p class="views">Views <span>198</span></p>
                                    <a href="javascript:void(0);" class="view-more">View More</a>
                                </div>
                            </div>
                            <div class="bottom">
                                <div class="meta">
                                    <div class="separator"></div>
                                    <div class="left">
                                        <div class="date"><i class="fa fa-clock-o"></i> Sep-1-2018</div>
                                        <ul class="share">
                                            <li><a href="javascript:void(0);" class="fb"><i class="fab fa-facebook-f"></i></a></li>
                                            <li><a href="javascript:void(0);" class="tw"><i class="fab fa-twitter"></i></a></li>
                                            <li><a href="javascript:void(0);" class="wa"><i class="fab fa-whatsapp"></i></a></li>
                                            <li><a href="javascript:void(0);" class="ib"><i class="fa fa-inbox"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="right">
                                        <a href="javascript:void(0);" class="coupon-button">Show Code <span>9X</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-6">
                        <div class="voucher-card">
                            <div class="top">
                                <div class="left">
                                    <img src="{!! asset('assets/front/images/featured-vouchers/1.jpg') !!}" alt="featured-vouchers">
                                    <div class="content">
                                        <h3>Rakuten TV</h3>
                                        <p>Receive Delivery For Free Off Any Order At Campus Gifts</p>
                                    </div>
                                </div>
                                <div class="right">
                                    <p class="views">Views <span>198</span></p>
                                    <a href="javascript:void(0);" class="view-more">View More</a>
                                </div>
                            </div>
                            <div class="bottom">
                                <div class="meta">
                                    <div class="separator"></div>
                                    <div class="left">
                                        <div class="date"><i class="fa fa-clock-o"></i> Sep-1-2018</div>
                                        <ul class="share">
                                            <li><a href="javascript:void(0);" class="fb"><i class="fab fa-facebook-f"></i></a></li>
                                            <li><a href="javascript:void(0);" class="tw"><i class="fab fa-twitter"></i></a></li>
                                            <li><a href="javascript:void(0);" class="wa"><i class="fab fa-whatsapp"></i></a></li>
                                            <li><a href="javascript:void(0);" class="ib"><i class="fa fa-inbox"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="right">
                                        <a href="javascript:void(0);" class="coupon-button">Show Code <span>9X</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-6">
                        <div class="voucher-card">
                            <div class="top">
                                <div class="left">
                                    <img src="{!! asset('assets/front/images/featured-vouchers/1.jpg') !!}" alt="featured-vouchers">
                                    <div class="content">
                                        <h3>Rakuten TV</h3>
                                        <p>Receive Delivery For Free Off Any Order At Campus Gifts</p>
                                    </div>
                                </div>
                                <div class="right">
                                    <p class="views">Views <span>198</span></p>
                                    <a href="javascript:void(0);" class="view-more">View More</a>
                                </div>
                            </div>
                            <div class="bottom">
                                <div class="meta">
                                    <div class="separator"></div>
                                    <div class="left">
                                        <div class="date"><i class="fa fa-clock-o"></i> Sep-1-2018</div>
                                        <ul class="share">
                                            <li><a href="javascript:void(0);" class="fb"><i class="fab fa-facebook-f"></i></a></li>
                                            <li><a href="javascript:void(0);" class="tw"><i class="fab fa-twitter"></i></a></li>
                                            <li><a href="javascript:void(0);" class="wa"><i class="fab fa-whatsapp"></i></a></li>
                                            <li><a href="javascript:void(0);" class="ib"><i class="fa fa-inbox"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="right">
                                        <a href="javascript:void(0);" class="coupon-button">Show Code <span>9X</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('front.partials._popularCategories')
@endsection
