@extends('front.layouts.app')

@include('front.partials._head')

@section('css')
    <!-- <link rel="stylesheet" href="{!! asset('assets/front/css/categories.css') !!}"> -->
@endsection

@section('content')

@include('front.partials._innerSearch')

<div class="container">
    @include('front.partials._sideBanners')
</div>

<section class="category-wrap">
    <div class="container">
        <div class="heading py-0">
            <h2>Save at these shops</h2>
            <p>Find your store and grab a voucher</p>
        </div>

        <div class="row">
            <div class="col-12">
                @foreach($categories as $category)
                <div class="category-card">
                    <div class="top">
                        <div class="cat-name">
                            <img src="{!! asset(uploadsDir(). $category['image_large']) !!}" alt="{!! $category['name'] !!}">
                            <h1>
                                <a href="{!! route('category.details', $category['slug']) !!}" style="color: {!! $category['color'] !!}">{!! $category['name'] !!}</a>
                            </h1>
                        </div>
                        <a href="{!! route('category.details', $category['slug']) !!}" class="v-btn yellow">View All</a>
                    </div>
                    <div class="bottom">
                        @if(count($category['stores']) > 0)
                        <ul class="list-inline text-center text-md-left mb-0">
                            @foreach($category['stores'] as $store)
                            <li class="list-inline-item">
                                <a href="{!! route('page', trim($store->website)) !!}" class="tag">{{ $store->name }}</a>
                            </li>
                            @endforeach
                        </ul>
                        @endif
                    </div>
                </div>
                @endforeach

            </div>
        </div>

        <!-- <a href="javascript:void(0);" class="load-more-cards">Load More <i class="fa fa-repeat" aria-hidden="true"></i></a> -->
    </div>
</section>

@include('front.partials._popularCategories')
@endsection