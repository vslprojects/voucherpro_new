@extends('front.layouts.app')

@include('front.partials._head')

@section('css')
    <!-- <link rel="stylesheet" href="{!! asset('assets/front/css/trending.css') !!}"> -->
@endsection

@section('content')

@include('front.partials._innerSearch')

    <section class="category-listing">
        <div class="container">
            @include('front.partials._sideBanners')
            <div class="row">
                <div class="col-12 mb-20">
                    <h2><span>Result For</span> {{ $q }}</h2>
                </div>
                <div class="col-12">
                    <div class="row">
                        @foreach($stores as $store)
                        @php
                            $coverImg = (!empty($store['cover'])) ? uploadsDir(). $store['cover'] : defaultStoreCoverUrl();    
                        @endphp
                        <div class="col-12 col-sm-6 col-xl-3">
                            <div class="company-card">
                                <div class="banner"><img src="{!! asset($coverImg) !!}" class="img-fluid" alt="{!! $store['name'] !!}"></div>
                                <div class="about-offer">
                                    <div class="brand-logo"><img src="{!! asset(uploadsDir(). $store['logo']) !!}" width="81" height="72" alt="{!! $store['name'] !!}"></div>
                                    <h4 class="store-name">{!! $store['name'] !!}</h4>
                                    <p class="description">{!! Str::words(strip_tags($store['description']), 12) !!}</p>
                                    <div class="meta">
                                        <div class="separator"></div>
                                        <a href="{!! route('page', trim($store['website'])) !!}" class="v-btn yellow p-2 mx-auto">Visit Store</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>

@include('front.partials._popularCategories')
@endsection

@section('js')
@endsection
