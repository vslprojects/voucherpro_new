@extends('front.layouts.app')

@include('front.partials._head')

@section('css')
    <!-- <link rel="stylesheet" href="{!! asset('assets/front/css/content-pages.css') !!}"> -->
@endsection

@section('content')

@include('front.partials._innerSearch')

<section class="content-page mt-25 mb-25">
    <div class="container">
        @include('front.partials._sideBanners')
        <div class="row">
            <div class="col-12">
                <div class="content-wrap">
                    <h1 class="page-title">{!! $cmsPage->page_title !!}</h1>
                    {!! $cmsPage->content !!}
                </div>
            </div>
        </div>
    </div>
</section>
@include('front.partials._popularCategories')
@endsection