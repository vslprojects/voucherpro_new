@extends('front.layouts.app')

@include('front.partials._head')

@section('css')
    <!-- <link rel="stylesheet" href="{!! asset('assets/front/css/contact.css') !!}"> -->
@endsection

@section('content')

@include('front.partials._innerSearch')

<section class="contact-us">
    <div class="container">
    @include('front.partials._sideBanners')
        <section class="heading text-center">
            <h1 class="text-blue">Let's keep in touch</h1>
            <p>Something on your mind? Or any suggestion for {!! $siteSettings->website !!}? <span class="d-lg-block">We'd love to hear from you!</span></p>
        </section>
    </div>

    <section class="form-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-5">
                    <form class="mb-20" id="contactForm" action="{{ route('process-contact') }}">
                        <div class="row">
                            <div class="col-12 form-fields">
                                <input type="text" name="name" class="form-control" placeholder="Name*">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 form-fields">
                                <input type="email" name="email" class="form-control" placeholder="Email*">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 form-fields">
                                <input type="tel" name="phone" class="form-control" placeholder="Phone">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 form-fields">
                                <textarea class="form-control" name="description" placeholder="Message*"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <label class="disclaimer" for="contact-disclaimer">
                                    <input type="checkbox" name="terms" id="contact-disclaimer">
                                    <span class="box unchecked"></span>
                                    <span class="text">I have read and agree to the <a href="javascript:void(0);">Terms and Conditions</a> and <a href="javascript:void(0);">Privacy Policy</a></span>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <button type="submit">Send <i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                            </div>
                        </div>
                    </form>
                    <!-- <div class="alert alert-success" role="alert">
                        <p class="text-success">You form has been submitted successfully</p>
                    </div>
                    <div class="alert alert-danger" role="alert">
                        <p class="text-danger">Please fill the required fields</p>
                    </div> -->
                </div>
                <div class="col-12 col-lg-6 ml-auto">
                    <h2>Contact us</h2>
                    <h3>Tell us if you need any kind of informations</h3>
                    <p>Call us for imiditate support this number</p>
                    <a class="phone-number" href="callto:+442037500511">(+44) 203 750 0511</a>
                </div>
            </div>
        </div>
    </section>

    <div class="container">
        @include('front.partials._popularCategories')
    </div>  
</section>


@endsection