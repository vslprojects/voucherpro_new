@extends('front.layouts.app')

@include('front.partials._head')

@section('css')
    <!-- <link rel="stylesheet" type="text/css" href="{!! asset('assets/front/css/home.css') !!}"> -->
@endsection

@section('og-tag')

    <meta property=og:image content="{!! asset('assets/front/images/ogg-logo-247.png') !!}" />

    <meta property=og:image content="{!! asset('assets/front/images/ogg-logo-vp.jpg') !!}" />

@endsection

@section('content')

    @include('front.partials._headerSearch')
    <div class="home-companies">
        <div class="container">
           
            <ul class="brand-logos">
                <li>
                    <a href="#" target="_blank">
                        <img src="{!! asset('assets/front/images/store/alpinetrek-logo.jpg') !!}" class="img-fluid" alt="voucher">
                        <img src="{!! asset('assets/front/images/store/ann-summers-logo.gif') !!}" class="img-fluid" alt="voucher">
                        <img src="{!! asset('assets/front/images/store/appleyard-flowers-logo.png') !!}" class="img-fluid" alt="voucher">
                        <img src="{!! asset('assets/front/images/store/argos-logo.png') !!}" class="img-fluid" alt="voucher">
                        <img src="{!! asset('assets/front/images/store/bodybuilding-warehouse-logo.png') !!}" class="img-fluid" alt="voucher">
                        <img src="{!! asset('assets/front/images/store/boux-avenue-logo.png') !!}" class="img-fluid" alt="voucher">
                        <img src="{!! asset('assets/front/images/store/dell-logo.png') !!}" class="img-fluid" alt="voucher">
                        <img src="{!! asset('assets/front/images/store/f1-store-logo.jpg') !!}" class="img-fluid" alt="voucher">
                        <img src="{!! asset('assets/front/images/store/farmdrop-logo.jpg') !!}" class="img-fluid" alt="voucher">
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <section class="home-banner">
        <div class="container">
            <div class="row">
                <div class="col-12 col-xl-7">
                    <h2 class="section-heading"><span>Daily</span> Trending</h2>
                    <div class="owl-carousel home-carousel">
                        <div class="item">
                            <div class="banner-wrap">
                                <div class="banner-img">
                                    <a href="" target="_blank">
                                        <img src="{!! asset('assets/front/images/products/daily-trending.jpg') !!}" width="667" height="" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-xl-5">
                    <h2 class="section-heading"><span>Featured</span> Vouchers</h2>
                    <div class="row">
                        <div class="col-12">
                            <div class="featured-voucher-carousel">
                                <img class="slick-next slick-arrow" src="{!! asset('assets/front/images/prev.jpg') !!}" style="">
                                <img class="slick-next slick-arrow" src="{!! asset('assets/front/images/next.jpg') !!}" style="">
                                <div class="featured-voucher-card">
                                    <div class="top">
                                        <div class="left">
                                            <img src="" alt="">
                                            <div class="content">
                                                <h3>TabCat</h3>
                                                <p>product details here</p>
                                            </div>
                                        </div>
                                        <div class="right">
                                            <p class="views">Views <span>voucher</span></p>
                                            <a href="#" class="view-more">View More</a>
                                        </div>
                                    </div>
                                    <div class="bottom">
                                        <div class="meta">
                                            <div class="separator"></div>
                                            <div class="left">
                                                <div class="date"><i class="fa fa-clock-o"></i> </div>
                                                <ul class="share">
                                                    <li>
                                                        <a href="javascript:void(0);" class="fb fb-share" data-value="" data-desc=""
                                                           data-image=""><i class="fab fa-facebook-f"></i></a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);" class="tw tw-share" data-value="" data-desc=""><i class="fab fa-twitter"></i></a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);" class="wa wa-share" data-value="" data-desc=""><i class="fab fa-whatsapp"></i></a>
                                                    </li>
                                                    <li>
                                                        <a href="mailto:?subject=&body=Please Check this Link " class="ib"><i class="fa fa-inbox"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="right">
                                                <a data-id="" class="coupon-button" href="javascript:;" data-url="">
                                                    <span>voucher</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('front.partials._popularCategories')
    <style>
        .top-offer .banner{
            min-height:160px;
            border-radius: 6px 6px 0 0;
        }
        .offer-card .store-name{
            padding:0 10px !important;
        }
        .top-offer .offer-card .brand-logo{
            padding:5px !important;
        }
        .top-offer .row .col-12{
            padding-left:8px;
            padding-right:8px;
        }
    </style>
    <section class="top-offer">
        <div class="container">
            <h2 class="section-heading"><span>Top</span> Offers</h2>
            <div class="row">
                <div class="col-12 col-sm-6 col-xl-3">
                    <div class="offer-card">
                        <div class="banner" style="background: url({!! asset('assets/front/images/store-cover.png') !!}) no-repeat; background-position: center center; background-size: cover;">
                        </div>
                        <div class="about-offer">
                            <div class="brand-logo">
                                <a href="#" target="_blank">
                                    <img src="{!! asset('assets/front/images/products/1.jpg') !!}" width="81" height="72" alt="voucher"></a>
                            </div>
                            <h4 class="store-name">voucher</h4>
                            <p class="description p-0 m-0">voucher</p>
                            <div class="meta">
                                <div class="separator"></div>
                                <div class="top">
                                    <div class="date"><i class="fa fa-clock-o"></i>
                                        voucher
                                    </div>
                                    <ul class="share">
                                        <li>
                                            <a href="javascript:void(0);" class="fb fb-share" data-value="voucher" data-desc="voucher" data-image=""><i class="fab fa-facebook-f"></i></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);" class="tw tw-share" data-value="" data-desc=""><i class="fab fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);" class="wa wa-share"><i class="fab fa-whatsapp"></i></a>
                                        </li>
                                        <li>
                                            <a href="#Daily Trending" class="ib"><i class="fa fa-inbox"></i></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="bottom">
                                    <a data-type="" data-id="" class="coupon-button" href="javascript:;" data-url="">Show
                                        <span>vouhcer</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-xl-3">
                    <div class="offer-card">
                        <div class="banner" style="background: url({!! asset('assets/front/images/store-cover.png') !!}) no-repeat; background-position: center center; background-size: cover;">
                        </div>
                        <div class="about-offer">
                            <div class="brand-logo">
                                <a href="#" target="_blank">
                                    <img src="{!! asset('assets/front/images/products/1.jpg') !!}" width="81" height="72" alt="voucher"></a>
                            </div>
                            <h4 class="store-name">voucher</h4>
                            <p class="description p-0 m-0">voucher</p>
                            <div class="meta">
                                <div class="separator"></div>
                                <div class="top">
                                    <div class="date"><i class="fa fa-clock-o"></i>
                                        voucher
                                    </div>
                                    <ul class="share">
                                        <li>
                                            <a href="javascript:void(0);" class="fb fb-share" data-value="voucher" data-desc="voucher" data-image=""><i class="fab fa-facebook-f"></i></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);" class="tw tw-share" data-value="" data-desc=""><i class="fab fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);" class="wa wa-share"><i class="fab fa-whatsapp"></i></a>
                                        </li>
                                        <li>
                                            <a href="#Daily Trending" class="ib"><i class="fa fa-inbox"></i></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="bottom">
                                    <a data-type="" data-id="" class="coupon-button" href="javascript:;" data-url="">Show
                                        <span>vouhcer</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-xl-3">
                    <div class="offer-card">
                        <div class="banner" style="background: url({!! asset('assets/front/images/store-cover.png') !!}) no-repeat; background-position: center center; background-size: cover;">
                        </div>
                        <div class="about-offer">
                            <div class="brand-logo">
                                <a href="#" target="_blank">
                                    <img src="{!! asset('assets/front/images/products/1.jpg') !!}" width="81" height="72" alt="voucher"></a>
                            </div>
                            <h4 class="store-name">voucher</h4>
                            <p class="description p-0 m-0">voucher</p>
                            <div class="meta">
                                <div class="separator"></div>
                                <div class="top">
                                    <div class="date"><i class="fa fa-clock-o"></i>
                                        voucher
                                    </div>
                                    <ul class="share">
                                        <li>
                                            <a href="javascript:void(0);" class="fb fb-share" data-value="voucher" data-desc="voucher" data-image=""><i class="fab fa-facebook-f"></i></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);" class="tw tw-share" data-value="" data-desc=""><i class="fab fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);" class="wa wa-share"><i class="fab fa-whatsapp"></i></a>
                                        </li>
                                        <li>
                                            <a href="#Daily Trending" class="ib"><i class="fa fa-inbox"></i></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="bottom">
                                    <a data-type="" data-id="" class="coupon-button" href="javascript:;" data-url="">Show
                                        <span>vouhcer</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-xl-3">
                    <div class="offer-card">
                        <div class="banner" style="background: url({!! asset('assets/front/images/store-cover.png') !!}) no-repeat; background-position: center center; background-size: cover;">
                        </div>
                        <div class="about-offer">
                            <div class="brand-logo">
                                <a href="#" target="_blank">
                                    <img src="{!! asset('assets/front/images/products/1.jpg') !!}" width="81" height="72" alt="voucher"></a>
                            </div>
                            <h4 class="store-name">voucher</h4>
                            <p class="description p-0 m-0">voucher</p>
                            <div class="meta">
                                <div class="separator"></div>
                                <div class="top">
                                    <div class="date"><i class="fa fa-clock-o"></i>
                                        voucher
                                    </div>
                                    <ul class="share">
                                        <li>
                                            <a href="javascript:void(0);" class="fb fb-share" data-value="voucher" data-desc="voucher" data-image=""><i class="fab fa-facebook-f"></i></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);" class="tw tw-share" data-value="" data-desc=""><i class="fab fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);" class="wa wa-share"><i class="fab fa-whatsapp"></i></a>
                                        </li>
                                        <li>
                                            <a href="#Daily Trending" class="ib"><i class="fa fa-inbox"></i></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="bottom">
                                    <a data-type="" data-id="" class="coupon-button" href="javascript:;" data-url="">Show
                                        <span>vouhcer</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- <section class="best-selling">
    <div class="container">
        <h2 class="section-heading"><span>Best Selling</span> Amazon Products</h2>
        <div class="row">
            <div class="col-12">
                <div class="owl-carousel amazon-product-carousel">
                    <div class="item">
                        <div class="product-wrap">
                            <a href="javascript:void(0);">
                                <img src="{!! asset('assets/front/images/products/1.jpg') !!}" class="img-fluid" alt="Product">
                                <p class="product-description">Finish Dishwasher Tablets, All in 1 Max Lemon, 90-Count</p>
                                <p class="price"><span class="old">£25.00</span> £12.41</p>
                            </a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="product-wrap">
                            <a href="javascript:void(0);">
                                <img src="{!! asset('assets/front/images/products/2.jpg') !!}" class="img-fluid" alt="Product">
                                <p class="product-description">Finish Dishwasher Tablets, All in 1 Max Lemon, 90-Count</p>
                                <p class="price"><span class="old">£25.00</span> £12.41</p>
                            </a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="product-wrap">
                            <a href="javascript:void(0);">
                                <img src="{!! asset('assets/front/images/products/3.jpg') !!}" class="img-fluid" alt="Product">
                                <p class="product-description">Finish Dishwasher Tablets, All in 1 Max Lemon, 90-Count</p>
                                <p class="price"><span class="old">£25.00</span> £12.41</p>
                            </a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="product-wrap">
                            <a href="javascript:void(0);">
                                <img src="{!! asset('assets/front/images/products/4.jpg') !!}" class="img-fluid" alt="Product">
                                <p class="product-description">Finish Dishwasher Tablets, All in 1 Max Lemon, 90-Count</p>
                                <p class="price"><span class="old">£25.00</span> £12.41</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> -->
    @include('front.partials._modal')
@endSection

@section('js')

    {{--    <script type="text/javascript">--}}

    {{--        $(".coupon-button").click(function(){$varName=$(this).data("id"),$store=$(this).data("url"),$type=$(this).data("type"),$url=window.location.href.split("?")[0],$typeNew="codes"==$type?"copy":"shopnow",window.open($url+"?"+$typeNew+"="+$varName),window.location="?r="+store});--}}

    {{--        @if(isset($offerDetail['offer_type']) && strtolower($offerDetail['offer_type']) == 'codes')--}}
    {{--        $('#codeModal').modal('show');--}}
    {{--        @elseif(isset($offerDetail['offer_type']) && strtolower($offerDetail['offer_type']) == 'deals')--}}
    {{--        $('#dealModal').modal('show');--}}
    {{--        @endif--}}


    {{--    </script>--}}
@endsection
